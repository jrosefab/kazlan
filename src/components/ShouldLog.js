import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity} from 'react-native';

export const ShouldLog = (props) => {
    const { signup, login } = props;

    return (
    <View style={styles.centeredView}>
        <View style={styles.container}>
            <Text style={styles.title}>Vous devez vous connecter</Text>
            <View style={{ marginVertical : 10 }}>
                <TouchableOpacity onPress={login}>
                    <View style={styles.auth_btn}>
                        <Text style={styles.auth_text}>
                            J'ai déjà un compte
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ marginVertical : 10 }}>
                <TouchableOpacity onPress={signup}>
                    <View style={styles.auth_btn}>
                        <Text style={styles.auth_text}>
                            S'enregistrer
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    title : {
        color : '#fff',
        fontSize : 16,
        alignSelf :'center',
        marginBottom : 10
    },
    centeredView: {
        flex : 1,
        justifyContent :'center',
        marginBottom : 0,
    },
    container : {
        backgroundColor : "#151520",
        alignSelf : 'center',
        width : 350,
        margin : 5,
        padding : 20,
        borderRadius : 10,
    },
    auth_btn : {
        borderWidth : 1, 
        borderColor : '#fff',
        padding : 20,
        borderRadius : 60,
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
})