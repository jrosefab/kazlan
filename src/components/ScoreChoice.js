import React from "react";
import {View,  Text,  StyleSheet, TouchableOpacity} from "react-native";
import { Icon } from "native-base";

export const ScoreChoice = (props) => (
    <View>
        <View style={styles.matchs_header}>
            <Text style={styles.versus_text}>
                {props.matchs} Round n°{props.round}
            </Text>
        </View>
        <View style={styles.result_wrapper}>
            <TouchableOpacity 
                activeOpacity={props.isLooseAvailable ? 0.5 : 1} 
                onPress={props.onLoose}
            >
                <View style={[
                    !props.minify ? styles.result_item 
                    : styles.min_result_item,
                    { backgroundColor : "#FE628C",
                        opacity : props.isLooser ? 1 : 0.4
                    }
                ]}>
                    <Icon type="MaterialCommunityIcons" 
                        name="trophy-broken" 
                        style={!props.minify ? styles.result_icon : styles.min_result_icon}
                    />
                    {!props.minify &&
                        <Text numberOfLines={3} style={{ color : "#fff"}}>
                            J'ai perdu
                        </Text>
                    }              
                </View>
            </TouchableOpacity>
            <TouchableOpacity 
                activeOpacity={props.isWinAvailable ? 0.5 : 1} 
                onPress={props.onWin}
            >
                <View style={[
                    !props.minify ? styles.result_item 
                    : styles.min_result_item,
                    { backgroundColor : "#5BFFA2",
                        opacity : props.isWinner ? 1 : 0.4
                    }
                ]}>
                    <Icon type="MaterialCommunityIcons" 
                        name="trophy" 
                        style={!props.minify ? styles.result_icon : styles.min_result_icon}
                    />
                    {!props.minify &&
                        <Text numberOfLines={3} style={{ color : "#fff"}}>
                            J'ai gagné
                        </Text>
                    }
                </View>
            </TouchableOpacity>
        </View>
    </View>
)

const styles = StyleSheet.create({
    category : {
        marginVertical : 6,
    },
    matchs_header : {
        alignSelf : "center",
        paddingHorizontal : 30,
        borderRadius : 10,
        marginBottom : 10
    },
    versus_text:{
        color : '#fff', 
        alignSelf: 'center', 
        fontSize : 20, 
        fontWeight : "bold"
    },
    category_title : {
        fontSize : 20, 
        marginVertical : 10, 
        fontWeight : 'bold', 
        color : "#fff",
        alignSelf : "center"
    },
    min_category_title : {
        fontSize : 15, 
        marginTop : 5,
        fontWeight : 'bold', 
        color : "#fff",
        alignSelf : "center"
    },
    result_wrapper : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-around",
        marginVertical : 15
    },
    result_item : {
        justifyContent : "center",
        alignItems : "center",
        width : 115,
        height : 110,
        paddingHorizontal : 20,
        paddingVertical : 10,
        borderRadius : 20,
    },
    result_icon : {
        fontSize : 50,
        color : "#fff",
        textShadowOffset:{width:5, height:2},
        shadowColor:'#000000',
        shadowOpacity:0.7,
        marginVertical : 5,
    },
    min_result_item : {
        justifyContent : "center",
        alignItems : "center",
        width : 70,
        height : 70,
        paddingHorizontal : 0,
        paddingVertical : 5,
        borderRadius : 20,
    },
    min_result_icon : {
        fontSize : 30,
        color : "#fff",
        marginVertical : 5,
        textShadowOffset:{width:5, height:2},
        shadowColor:'#000000',
        shadowOpacity:0.7,
    }
})