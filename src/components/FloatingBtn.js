import React, { useState} from "react";
import { View, StyleSheet } from 'react-native';
import { FloatingAction } from "react-native-floating-action";
import { Icon } from "native-base";

const actions = [
    {
        position: 1,
        text: "Proposer une session",
        icon: <Icon style={{ color : 'white', fontSize : 20}} 
                    type="MaterialCommunityIcons" 
                    name="boxing-glove"
                />,
        color : "#9420c6",
        name : "new_session"
    },
    {
        position: 2,
        text: "Défier en money match",
        icon: <Icon style={{ color : 'white', fontSize : 20}} 
                    type="FontAwesome5" 
                    name="euro-sign"
                />,
        color : "#9420c6",
        name : "new_money_match"
    }
];

export const FloatingBtn = (props) => {
    const [ clicked, setClicked] = useState(false);

    return (
        <View style={[styles.floating, { width : clicked ? "60%" : 70, height : clicked ? "30%" : 70}]}>
            <FloatingAction
                onPressMain={(status) => setClicked(status)}
                distanceToEdge= {0}
                showBackground={false}
                actionsPaddingTopBottom={2}
                position="right"
                color="#9420c6"
                actions={actions}
                onPressItem={name => {
                    if (name === "new_money_match"){
                        props.navigation.navigate("new_session", { screen : `${name}` });
                    }else{
                        props.navigation.navigate(`${name}`);
                    }
                }}
            />
        </View>

    );
}
const styles = StyleSheet.create({
    floating : {
        position: "absolute",
        right : 30,
        bottom : 30,
        zIndex : 99
    },
});