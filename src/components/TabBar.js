import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Icon } from "native-base";

export const TabBar = ( props ) => {
    const  { color, label, type, name, page, navigation} = props
    return (
        <TouchableWithoutFeedback onPress={navigation}>
            <View style={[styles.button, { backgroundColor : color }]}>
                <Icon style={{ color : 'white', fontSize : 30}} type={type} name={name}/>
                <Text style={{ color : 'white'}}>{label}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    button: {
        paddingVertical :10,
        alignItems: 'center',
        justifyContent : 'center',
        alignContent : "center",
        width: "100%",
    },
});