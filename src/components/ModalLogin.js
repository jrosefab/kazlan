import React from 'react';
import { View, Modal, StyleSheet,
    TouchableWithoutFeedback, Text,TouchableOpacity} from 'react-native';

export const ModalLogin = (props) => {
    const { signup, onPress, login, 
            visible, onRequestClose } = props;

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={onRequestClose}
        >
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={styles.centeredView}>
                    <View style={styles.modalContainer}>
                        <Text style={styles.title}>Vous devez vous connecter</Text>
                        <View style={{ marginVertical : 10 }}>
                            <TouchableOpacity onPress={login}>
                                <View style={styles.auth_btn}>
                                    <Text style={styles.auth_text}>
                                        J'ai déjà un compte
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginVertical : 10 }}>
                            <TouchableOpacity onPress={signup}>
                                <View style={styles.auth_btn}>
                                    <Text style={styles.auth_text}>
                                        S'enregistrer
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
};

const styles = StyleSheet.create({
    title : {
        color : '#fff',
        fontSize : 16,
        alignSelf :'center',
        marginBottom : 10
    },
    centeredView: {
        flex : 1,
        justifyContent :'center',
        marginBottom : 0,
        backgroundColor : "rgba(0, 0, 0, 0.72)"
    },
    modalContainer : {
        backgroundColor : "#151520",
        alignSelf : 'center',
        width : 350,
        margin : 5,
        padding : 20,
        borderRadius : 10,
    },
    auth_btn : {
        borderWidth : 1, 
        borderColor : '#fff',
        padding : 20,
        borderRadius : 20,
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
})