import React from 'react';
import { View, Modal, StyleSheet,
    TouchableWithoutFeedback, Text,TouchableOpacity} from 'react-native';

export const ModalGame = (props) => {
    const { onPress, visible, onRequestClose } = props;

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={onRequestClose}
        >
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={styles.centeredView}>
                    <View style={styles.modalContainer}>
                        <Text style={styles.title}>Ajoutez un jeu</Text>
                        {props.children}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
};

const styles = StyleSheet.create({
    title : {
        color : '#fff',
        fontSize : 16,
        alignSelf :'center',
        marginBottom : 10
    },
    centeredView: {
        flex : 1,
        justifyContent :'center',
        marginBottom : 0,
        backgroundColor : "rgba(0, 0, 0, 0.72)"
    },
    modalContainer : {
        backgroundColor : "#151520",
        height : "auto",
        alignSelf : 'center',
        width : 350,
        margin : 5,
        padding : 20,
        borderRadius : 10,
    },
})