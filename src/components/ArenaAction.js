import React from "react";
import { Animated } from "react-native";
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import { Image, StyleSheet, View } from 'react-native';
import { StyleProvider, Container, 
        Header, Title, Icon,} from "native-base";
import Logo from '../assets/logo.png';

export const ArenaAction = (props) => {
    return (
        <TouchableOpacity onPress={() => navigation.navigate("arena_management")}>
            <View style={styles.btn_wrapper}>
                <Text style={styles.btn_text}>
                    {creatorInfos.id === userApp.user.uid ? "Gérer la session" : "Je m'inscris"}
                </Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    header : {
        backgroundColor : "#151520", 
        justifyContent : "space-between",
        height : 60
    },
    logo : {
        height: 50,
        width: 55,
        alignSelf : "center",
        padding : 15
    },
    icon : {
        color : "white",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    }
});