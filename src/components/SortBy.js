import React, { useRef, useState, useEffect } from "react";
import { Image,Text, StyleSheet, View,
    TouchableOpacity, Animated, TouchableWithoutFeedback } from 'react-native';
import { retrieveData } from '../services/AsyncStorage'
import { storeData } from '../services/AsyncStorage'
import { Header, Icon, Radio} from "native-base";
import Logo from '../assets/logo.png';

export const SortBy = (props) => {
    const { navigation } = props;
    const [expend, setExpend] = useState(false)
    const [filterChoice, setFilterChoice] = useState([])
    const [radio, setRadio] = useState("oldest")
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const handleParameter = (type) => {
        Animated.timing(
            fadeAnim, {
                toValue: 380,
                duration: 300
            },
        ).start();

        setTimeout(()=>{ 
            setExpend(true); 
        }, 250);
    };

    const submitChoice = () => {
        var index = filterChoice.findIndex(isExist => isExist === radio)
        if (index === 1){
            filterChoice.filter(itemToRemove => itemToRemove !== radio)
        }

        storeData.saveItem("filter", JSON.stringify(filterChoice) );
        navigation.replace("home")
    }

    const fadeOut = () => {
        Animated.timing(fadeAnim, {
        toValue: 60,
        duration: 300
        }).start();

        setExpend(false); 
    };

    const handleFilter = (choice) => {
        var index = filterChoice.findIndex(isExist => isExist === choice)
        if (index === -1){
            setFilterChoice(prevState => ([...prevState, choice]));
        }else{
            setFilterChoice(filterChoice.filter(itemToRemove => itemToRemove !== choice));
        }
    }

    useEffect(() => {
        retrieveData.retrieveItem("filter").then((filter) => {
            setFilterChoice(JSON.parse(filter))
        })

        console.log(navigation)
        
        if(expend){
            console.log(navigation)
        }
        Animated.timing(
            fadeAnim,
            {
                toValue: 60,
                duration: 0,
            }
        ).start();
    }, [fadeAnim])
    

    return (
        <TouchableWithoutFeedback onPress={fadeOut}>
            <Animated.View style={{height : fadeAnim }}>
                <View style={styles.parameters}>
                    <TouchableOpacity onPress={handleParameter}>
                        <Icon style={styles.icon_parameters} type="MaterialCommunityIcons" name="filter-variant"/>
                    </TouchableOpacity>
                </View>
                {expend &&
                <View style={[ styles.fadingContainer, {height : expend ? 300 : 0 }]}>
                    <View style={{ paddingHorizontal : 20, paddingVertical : 10 }}>
                        <Text style={styles.category}>Ordre d'affichage :</Text>
                        <View style={{ flexDirection : "column", marginVertical : 10,}}>
                            <TouchableOpacity 
                                style={{ flexDirection : "row", marginBottom : 5}}
                                onPress={() =>  setRadio("oldest")}
                            >
                                <Radio 
                                    color="#9420c6"
                                    selectedColor="#9420c6" 
                                    selected={radio === "oldest" ? true : false} 
                                />
                                <Text style={{ color : "white", marginLeft : 10}}>
                                    Du plus anciens au plus récent
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={{ flexDirection : "row", marginBottom : 5}}
                                onPress={() => setRadio("newest")}
                            >                                
                                <Radio 
                                    color="#9420c6"
                                    selectedColor="#9420c6" 
                                    selected={radio === "newest" ? true : false} 
                                />                                
                                <Text style={{ color : "white", marginLeft : 10}}>
                                    Du plus récent au plus ancien
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <Text style={[styles.category, {marginTop : 10}]}>
                            N'afficher que :
                        </Text>
                        <View style={{ flexDirection : "row", marginVertical : 10, flexWrap : "wrap" }}>

                            <TouchableOpacity 
                                onPress={() => handleFilter("sessions")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("sessions") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Sessions</Text>
                            </TouchableOpacity>
                            
                            <TouchableOpacity 
                                onPress={() => handleFilter("money_matchs")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("money_matchs") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Money Matchs</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => handleFilter("online")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("online") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Partie en ligne</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => handleFilter("offline")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("offline") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Partie locale</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.apply}
                        onPress={submitChoice}
                    >
                        <Text style={{ color : "#fff", fontWeight :"bold", marginRight : 5 }}>
                            Appliquer
                        </Text>
                        <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                    </TouchableOpacity>
                </View>
                }
            </Animated.View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    parameters: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection : "row",
        backgroundColor : "red"
    },
    logo : {
        height: 45,
        width: 50,
        alignSelf : "center",
        padding : 15
    },
    icon_parameters : {
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    category : {
        fontSize : 16,
        color : "white",
        fontWeight : "bold"
    },
    btn : {
        color : "white",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    fadingContainer: {
        zIndex: 99,
        position : "absolute",
        top : 50, 
        marginTop : 10,
        width : "100%",
        backgroundColor : "#151520", 
    },
    choice : {
        borderColor : "#9420c6",
        borderWidth : 1,
        justifyContent : "center",
        padding : 10,
        borderRadius : 60,
        margin : 3
    },
    fadingText: {
        fontSize: 28,
        textAlign: "center",
        margin: 10
    },
    apply : {
        flexDirection : "row",
        alignItems : "center",
        alignSelf : 'flex-end',
    },
});
