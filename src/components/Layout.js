import React, { useRef, useState, useEffect, useContext } from "react";
import { Text, StyleSheet, View,
    TouchableOpacity, Animated, TouchableWithoutFeedback } from 'react-native';
import { UserContext } from '../contexts/UserProvider';
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import { StyleProvider, Container, 
        Header, Radio, Icon,} from "native-base";
import { retrieveData, storeData } from '../services/AsyncStorage'

export const Layout = (props) => {
    const userApp = useContext(UserContext);
    const [expend, setExpend] = useState(false)
    const [filterChoice, setFilterChoice] = useState([])
    const [radio, setRadio] = useState("oldest")
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const handleParameter = (type) => {
        Animated.timing(
            fadeAnim, {
                toValue: 300,
                duration: 300
            },
        ).start();

        setTimeout(()=>{ 
            setExpend(true); 
        }, 250);
    };

    const truncateString = (str) => {
        return str.length > 4 ? str.substring(0, 5) + "..." : str;
    }

    const submitChoice = () => {
        var index = filterChoice.findIndex(isExist => isExist === radio)
        if (index === 1){
            filterChoice.filter(itemToRemove => itemToRemove !== radio)
        }

        storeData.saveItem("filter", JSON.stringify(filterChoice) );
        props.navigation.replace("home")
    }

    const fadeOut = () => {
        Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 300
        }).start();

        setExpend(false); 
    };

    const handleFilter = (choice) => {
        var index = filterChoice.findIndex(isExist => isExist === choice)
        if (index === -1){
            setFilterChoice(prevState => ([...prevState, choice]));
        }else{
            setFilterChoice(filterChoice.filter(itemToRemove => itemToRemove !== choice));
        }
    }

    useEffect(() => {
        retrieveData.retrieveItem("filter").then((filter) => {
            setFilterChoice(JSON.parse(filter))
        })

        if(expend){
            setExpend(props.active)  
        }
        Animated.timing(
            fadeAnim,
            {
                toValue: 0,
                duration: 0,
            }
        ).start();
    }, [fadeAnim])

    return (
    <StyleProvider style={getTheme(platform)}>
        <TouchableWithoutFeedback onPress={fadeOut}>
        <Container>
            {props.sortBy &&
                    <View style={styles.header}>
                        <View style={styles.communities_container}>
                            {props.communities && 
                                props.communities.map(com => (
                                    <View key={com.game} style={styles.parameters}>
                                        <Text style={{ color :"#fff", marginRight : 10}}>
                                            {truncateString(com.game)}
                                        </Text>
                                    </View>
                                ))
                            }  
                            
                            <TouchableOpacity onPress={props.onReset}>
                                <View style={styles.parameters}>
                                    <Icon style={{fontSize : 15, color : "#fff"}}
                                        type="Entypo" 
                                        name="cross"
                                    />
                                    <Text style={{color : "#fff"}}>Reset</Text>
                                </View>
                            </TouchableOpacity>   
                        </View>

                        <View>
                            <TouchableOpacity onPress={handleParameter}>
                                <View style={[styles.parameters,{marginLeft : 10}]}>
                                    <Icon style={styles.icon_parameters} type="MaterialCommunityIcons" name="filter-variant"/>
                                    <Text style={{ color :"#fff"}}>Trier par : </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
            }
                
            <Animated.View style={{height : fadeAnim }}>
            {expend &&
                <View style={styles.fadingContainer}>
                    <View style={{ paddingHorizontal : 20, paddingVertical : 10 }}>
                        <Text style={styles.category}>Ordre d'affichage :</Text>
                        <View style={{ flexDirection : "column", marginVertical : 10,}}>
                            <TouchableOpacity 
                                style={{ flexDirection : "row", marginBottom : 5}}
                                onPress={() =>  setRadio("oldest")}
                            >
                                <Radio 
                                    color="#9420c6"
                                    selectedColor="#9420c6" 
                                    selected={radio === "oldest" ? true : false} 
                                />
                                <Text style={{ color : "white", marginLeft : 10}}>
                                    Du plus anciens au plus récent
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={{ flexDirection : "row", marginBottom : 5}}
                                onPress={() => setRadio("newest")}
                            >                                
                                <Radio 
                                    color="#9420c6"
                                    selectedColor="#9420c6" 
                                    selected={radio === "newest" ? true : false} 
                                />                                
                                <Text style={{ color : "white", marginLeft : 10}}>
                                    Du plus récent au plus ancien
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <Text style={[styles.category, {marginTop : 10}]}>
                            N'afficher que :
                        </Text>
                        <View style={{ flexDirection : "row", marginVertical : 10, flexWrap : "wrap" }}>

                            <TouchableOpacity 
                                onPress={() => handleFilter("sessions")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("sessions") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Sessions</Text>
                            </TouchableOpacity>
                            
                            <TouchableOpacity 
                                onPress={() => handleFilter("money_matchs")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("money_matchs") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Money Matchs</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => handleFilter("online")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("online") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Partie en ligne</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => handleFilter("offline")}
                                style={[styles.choice, { 
                                    backgroundColor : filterChoice.includes("offline") ? 
                                    "#9420c6" : "transparent"}
                                ]}
                            >
                                <Text style={{ color : "white"}}>Partie locale</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.apply}
                        onPress={submitChoice}
                    >
                        <Text style={{ color : "#fff", fontWeight :"bold", marginRight : 5 }}>
                            Appliquer
                        </Text>
                        <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                    </TouchableOpacity>
                </View>
                }
            </Animated.View>
            {props.children}
        </Container>
        </TouchableWithoutFeedback>
    </StyleProvider>
);
}

const styles = StyleSheet.create({
    parameters: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection : "row",
        borderRadius : 60,
        borderWidth : 1,
        borderColor : "#fff",
        paddingHorizontal : 10,
        margin : 2,
        paddingVertical : 5
    },
    header : {
        flexWrap : "wrap",
        backgroundColor : '#9420c6',
        justifyContent : "space-between",
        flexDirection : "row",
        zIndex: 99,
        padding : 10,
        elevation : 0,
    },
    communities_container : {
        flexDirection : "row", 
        flexWrap : "wrap", 
        flex : 1, 
        borderRightColor : "white", 
        borderRightWidth : 0.2
    },
    logo : {
        height: 45,
        width: 50,
        alignSelf : "center",
        padding : 15
    },
    icon_parameters : {
        color : "white",
        alignSelf : "center",
        fontSize: 20,
        marginHorizontal : 4,
        justifyContent : "flex-end",
        elevation : 0,
    },
    category : {
        fontSize : 16,
        color : "white",
        fontWeight : "bold"
    },
    btn : {
        color : "white",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    fadingContainer: {
        zIndex: 99,
        backgroundColor : '#151520',
        paddingVertical : 10,
        width : "100%",
    },
    choice : {
        borderColor : "#9420c6",
        borderWidth : 1,
        justifyContent : "center",
        padding : 10,
        borderRadius : 60,
        margin : 3
    },
    fadingText: {
        fontSize: 28,
        textAlign: "center",
        margin: 10
    },
    apply : {
        flexDirection : "row",
        alignItems : "center",
        alignSelf : 'flex-end',
    },
});