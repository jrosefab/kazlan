import React from 'react';
import { ActivityIndicator, View } from 'react-native'
import { Layout } from '../components';

export const Loading = (props) => {
    return (  
        props.fullscreen ?  
            <Layout>
                <View style={{ flex: 1, justifyContent: 'center', alignContent :'center',padding : 20 }}>
                    <View style={{alignSelf : 'center'}}>
                        <ActivityIndicator size="large" color="#20c6bd"/>
                    </View>
                </View>
            </Layout>
        :
        <View style={{ flex: 1, justifyContent: 'center', alignContent :'center', marginVertical : 5}}>
            <View style={{alignSelf : 'center'}}>
                <ActivityIndicator size="small" color="#20c6bd"/>
            </View>
        </View>
    )
}