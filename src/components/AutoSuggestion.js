import React, {useContext, useState} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Alert} from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import {Input} from 'native-base';

const InputRender = (props) => (
    <Input style={styles.input_render} {...props}/>
)
export const AutoSuggestion = ({ onChangeText, data, defaultValue, value, onPress }) => {
    return (
        <View style={styles.container}>
            <Autocomplete 
                containerStyle={styles.autocompleteContainer}
                inputContainerStyle={{ borderWidth : 0}}
                renderTextInput={() => 
                    <InputRender
                        placeholder="Quel jeu voulez vous ajouter"
                        onChangeText={onChangeText}
                        value={value}
                    />
                }
                autoCapitalize="none"
                autoCorrect={false}
                listStyle={styles.list}
                data={data}
                defaultValue={defaultValue}
                keyExtractor={(item, i) => ( item )}
                placeholder="Quel jeu voulez vous ajouter"
                renderItem={({item}) => (
                    <View style={styles.item_wrapper}>
                        <TouchableOpacity onPress={() => onPress(item)}>
                            <Text style={styles.items}>{item}</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 80
    },
    input_render : {
        color : "#fff",
        borderBottomWidth : 1,
        borderColor : "#fff"
    },
    list : {
        backgroundColor : "#151520",
        borderWidth : 0,
        borderBottomLeftRadius : 20,
        borderBottomRightRadius : 20,
        width : "100%"
    },
    item_wrapper : {
        marginVertical : 3
    },
    items:{
        height : 30,
        color : "#fff",
        fontSize : 15,
        borderBottomWidth : 1,
        borderColor : "#fff"
    }
});