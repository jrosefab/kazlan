import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Slider from 'react-native-slide-to-unlock';
import { Icon } from 'native-base'

export const SwipeAction = (props) => {
    const [ isUnlocked, setIsUnlocked] = useState(false);
    
    return (
        <View style={styles.container}>
            {!props.isVerified?
                <Slider
                    onEndReached={props.onEndReached}
                    containerStyle={styles.slider}
                    sliderElement={
                        <Icon type="MaterialIcons" name="arrow-forward-ios" style={styles.icon}/>
                    }
                >
                    <Text style={styles.text}>Confirmer le bon déroulement</Text>
                </Slider>
            :
            <View style={styles.confirmed}>
                <Text style={styles.text}>Confirmé !</Text>
            </View>
            }
        </View>
        
    );
}

const styles = StyleSheet.create({
    container : {
        marginVertical : 10
    },
    slider : {
        margin: 8,
        backgroundColor: 'transparent',
        borderRadius: 60,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        width: '95%',
        borderColor : "#fff",
        borderWidth : 2
    },
    confirmed : {
        margin: 8,
        backgroundColor: '#20c6bd',
        height : 60,
        borderRadius: 60,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        width: '95%',
        borderColor : "#fff",
        borderWidth : 2
    },
    text: {
        fontWeight : "bold",
        color : "#fff",
    },
    icon : {
        color : "#fff",
        width: 50,
        margin: 4,
        padding : 10,
        borderRadius: 300,
        height: 50,
        backgroundColor: '#20c6bd',
    }
})