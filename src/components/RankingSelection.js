import React from "react";
import {  Image, Text, TouchableOpacity } from "react-native";

export const RankingSelection = ({ onPress, source, isSelected, name, style}) => (
    <TouchableOpacity onPress={onPress}>          
        <Image style={[
                style, 
                isSelected ? 
                { opacity : 1 } : { opacity : 0.3 }
            ]} 
            source={source}
        />
        <Text style={{ alignSelf : 'center', color : 'white'}}>
            {name}
        </Text>                       
    </TouchableOpacity>
)