
import React, {useContext, useState, useCallback} from "react";
import {View, Alert, Text, Image, 
        StyleSheet,Dimensions, TouchableOpacity} from "react-native";
import 'moment/locale/fr';
import firestore from '@react-native-firebase/firestore';
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import {findBestOf, findMotif} from '../helpers/switchHelper';
import { UserContext } from "../contexts/UserProvider";
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading, ScoreChoice, SwipeAction} from "../components";
import ImagePicker from 'react-native-image-picker';
import StarRating from 'react-native-star-rating';
import { Icon, Picker, Textarea, Content } from "native-base";
import Broken from '../assets/broken_glass.png';
import UserDefault from '../assets/default_picture.png';
import Versus from '../assets/versus.png'
import Star from '../assets/star.png'
import Crown from '../assets/crown.png'
import stripe from 'tipsi-stripe'
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

const UserOpponent = ({ avatar, name, isCurrentPlayer, winScore, looseScore }) => (
    <View>
        <Image style={styles.user_image} source={avatar ? { uri : avatar } : UserDefault }/>
        <Text style={styles.versus_text}>
            {name}
        </Text>
        <View style={{ display: 'flex', flexWrap : "wrap", flexDirection : "row", alignSelf : "center"}}>
            {isCurrentPlayer ?
                [...Array(winScore)].map((e, i) => 
                    <Image style={{ height : 20, width : 20}} key={i} source={Star}/>
                )
            :
                [...Array(looseScore)].map((e, i) => 
                    <Image style={{ height : 20, width : 20}} key={i} source={Star}/>
                )
            }
        </View>
    </View>
)

const ArrowSlider = ({activeRound, limit, player, opponent, onPrevious, onNext}) => (
    <View style={styles.arrow_wrapper}>
        {activeRound !== 1 && 
            <TouchableOpacity onPress={onPrevious}>
                <Icon style={styles.arrow} name="leftcircle" type="AntDesign"/>
                <Text style={{ color : "white", alignSelf : "center", paddingHorizontal : 20}}>
                    Round précédent
                </Text>
            </TouchableOpacity>
        }
        {opponent > 0 && player > 0  &&
            <TouchableOpacity onPress={onNext}>
            <Icon style={styles.arrow} name="rightcircle" type="AntDesign"/>
            <Text style={{ color : "white", alignSelf : "center", paddingHorizontal : 20}}>
                Round suivant
            </Text>
        </TouchableOpacity>
        }
    </View>
)

export const Rounds = (props) => (
    <View style={{ flex: 1, paddingHorizontal : 20 }}>
        <View style={styles.versus_wrapper}>
            <UserOpponent
                avatar={playData.creator.avatar && playData.creator.avatar}
                name={playData.creator.name}
                isCurrentPlayer={playData.creator.is_current_player}
                winScore={playData.win_score}
                looseScore={playData.loose_score}
            />
            <Image style={styles.versus} source={Versus}/>
            <UserOpponent
                avatar={playData.participant.avatar && playData.participant.avatar}
                name={playData.participant.name}
                isCurrentPlayer={playData.creator.is_current_player}
                winScore={playData.loose_score}
                looseScore={playData.win_score}
            />
        </View>
        <View style={styles.scoreboard}>
            {playData.matchs.map(bo => {
                if (bo.round === playData.active_round)
                return (
                    <View style={{flex : 1 }}key={bo.round}>
                        {<ScoreChoice 
                            matchs={findBestOf(playData.matchs.length)}
                            opponent={opponent && opponent.name}
                            round={bo.round} 
                            isWinner={bo[playData.current_player] === "winner" ? true : false}
                            isLooser={bo[playData.current_player] === "looser" ? true : false}
                            isLooseAvailable={bo[playData.current_player].length <= 0 ? true : false}
                            isWinAvailable={bo[playData.current_player].length <= 0 ? true : false}
                            onWin={bo[playData.current_player].length <= 0 ? () => 
                                handleScore(bo.round , 'winner') : () => { return } 
                            }                                        
                            onLoose={bo[playData.current_player].length <= 0 ? () => 
                                handleScore(bo.round , 'looser') : () => { return } 
                            }                                        

                        /> }
                        {bo[playData.current_player].length <= 0 && 
                            <View style={{ alignSelf : "center"}}>
                                <Text style={styles.versus_text}>
                                    Saisir mon résultat 
                                </Text>
                            </View>
                        }
                        {bo[playData.current_opponent].length <= 0 ?
                            <View style={{ alignSelf : "center", flexDirection : "column", display : "flex"}}>
                                <Text style={styles.versus_text}>
                                    En attente du résultat de&nbsp;
                                    {arenaApp.currentArena.creator === userApp.user.uid ?
                                        playData.participant.name :
                                        playData.creator.name
                                    }
                                </Text>
                                <AnimatedEllipsis 
                                    animationDelay={150}
                                    numberOfDots={5}
                                    style={{
                                        color: '#fff',
                                        fontSize: 100,
                                        letterSpacing: 0,
                                        alignSelf : 'center',
                                        lineHeight : 80,
                                    }}
                                />
                            </View>
                            :
                            <Text style={styles.versus_text}>
                                {arenaApp.currentArena.creator === userApp.user.uid ?
                                    playData.participant.name :
                                    playData.creator.name
                                } a {bo[playData.current_opponent] === "winner"? 
                                "gagné " : "perdu "}
                                ce round
                            </Text>
                        }
                        
                        <ArrowSlider
                            activeRound={playData.active_round}
                            limit={playData.round_limit}
                            opponent={bo[playData.current_opponent].length}
                            player={bo[playData.current_player].length}
                            onPrevious={() => setPlayData(prevState =>({ 
                                ...prevState, 
                                active_round : playData.active_round -1 
                            }))}
                            onNext={() => setPlayData(prevState =>({ 
                                ...prevState, 
                                active_round : 
                                playData.active_round !== playData.round_limit ? 
                                playData.active_round  +1 : 
                                playData.active_round
                            }))}
                        />
                    </View>
                )
            })}
        </View>
    </View>
)

const styles = StyleSheet.create({
    versus_wrapper : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-evenly",
        width : "100%",
        alignItems : "center",
        marginVertical : 20,
    },
    versus : {
        height : 110,
        width : 110,
    },
    versus_text:{
        color : '#fff', 
        marginVertical : 10, 
        alignSelf: 'center', 
        fontSize : 16, 
        fontWeight : "bold"
    },
    user_image : {
        height : 100,
        width : 100,
        borderRadius : 600
    },
    scoreboard : {
        marginVertical : 6,
        flex : 1,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },

    amount_bet : {
        position : "relative",
        alignSelf : "center",
        marginBottom : 20
    },
    money_bet_text :{
        position: "absolute",
        top : 20,
        right: 13,
        fontSize : 14,
        fontWeight : "bold",
        color : "#a16012",
        zIndex: 5,
    },
    money_bet_coin : {
        height : 60,
        width : 60,
    },
    result_wrapper : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-around",
        marginVertical : 15
    },
    result_item : {
        justifyContent : "center",
        alignItems : "center",
        paddingHorizontal : 25,
        paddingVertical : 10,
        borderRadius : 20,
    },
    result_icon : {
        fontSize : 60,
        color : "#fff",
        marginVertical : 5
    },
    error_wrapper : {
        justifyContent : "center",
        alignSelf : "center",
    },
    error_text : {
        color : "#fff",
    },
    input_wrapper : {
        marginHorizontal : 20,
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
    },
    arrow_wrapper : {
        display : "flex",
        flexDirection : "row",
        alignSelf : "center",
        bottom : 40,
        position : "absolute",
        elevation:3
    },
    arrow : {
        fontSize : 50,
        color : "#fff",
        margin : 10,
        alignSelf : "center",
        borderRadius : 300
    },
    camera : {
        alignSelf : 'center',
        marginVertical : 20
    },
    image : {
        borderRadius : 15,
        height : 80,
        width : 80,
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    result_wrapper :{
        alignSelf : "center",
        position : "relative",
    },
    result_user_image : {
        position : "absolute",
        top : 65,
        right: 59,
        height : 150,
        width : 150,
        borderRadius : 600
    },
    btn_wrapper : {
        alignSelf : "center",
        backgroundColor : "#20c6bd",
        width :"100%",
        borderColor : '#fff',
        borderWidth : 2,
        textAlign : "center",
        paddingVertical : 20,
        borderRadius : 60,
        marginVertical : 30
    }
})