import React from 'react';
import {  StyleSheet, TouchableWithoutFeedback, 
    View, Text, Image, Button} from 'react-native';
import { Icon } from 'native-base';
import Coin from '../assets/coin.png';

const truncateString = (str) => {
    return str.length > 100 ? str.substring(0, 101) + "..." : str;
}

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

const divideAmountBet = (time) => {
    if (time.toString().includes('.')){ 
        return `${time}0`
    }else{
        return `${time}.00`
    }
}

export const CardArena = (props) => (
    <TouchableWithoutFeedback onPress={props.getArena}>
        <View style={styles.container_card}>
            <View style={styles.header}>
                <View style={{ width : "60%"}}>
                    <Text style={{ fontSize : 20, color :'#fff'}}>{props.title}</Text>
                    <Text style={{ fontSize : 12, color :'#fff'}}>
                        {props.money_bet ? "Money Match" : "Session"}&nbsp;
                        {props.mode === "online" ? "en ligne" : "à domicile"}
                        &nbsp;|&nbsp;
                        {capitalize(props.created)}
                    </Text>
                </View>
                <View style={[styles.arena_type, { width : "40%"}]}>
                    {props.accepted_ranking.length > 0 ?
                    <View style={styles.ranking_container}>
                        {props.accepted_ranking}
                    </View>
                    :
                    <View style={styles.amount_bet}>
                        <Image source={Coin} style={styles.money_bet_coin}/>
                        <Text style={styles.money_bet_text}>{divideAmountBet(props.money_bet)}€</Text>
                    </View>
                    }
                </View>
            </View>

            <View style={{ position : "relative"}}>
                <Image 
                    style={styles.image_arena}
                    source={props.image}
                />
                <View style={styles.game_container}>
                    <Image style={{ height : 30, width :30, borderRadius : 7 }} source={{ uri : props.game_bg}}/>
                    <Text style={{ color : "#000", marginHorizontal : 10, fontWeight : "bold"}}>{props.game}</Text>
                </View>
            </View>

            <View style={{color : '#fff', marginTop : 30, marginBottom : 30, marginHorizontal : 20}}>
                <Text style={{color : '#fff'}}>
                    {truncateString(props.description ? props.description : "")}
                </Text>
            </View>
            <View style={{color : '#fff', marginHorizontal : 16, flexDirection : 'row', justifyContent:'space-around'}}>

            </View>
            <View style={styles.action_arena}>
                <View style={styles.assets}>
                    <Icon style={styles.icon_assets} type="MaterialIcons" name="people"/>
                    <Text style={{ fontSize : 14, color : "#fff"}}>{props.inscription_limit} max</Text>
                </View>
                <View style={styles.assets}>
                    <Icon style={styles.icon_assets} type="MaterialIcons" name="date-range"/>
                    <Text style={{ fontSize : 14, color : "#fff"}}>Le {props.schedule_date}</Text>
                </View>
                <View style={styles.assets}>
                    <Icon style={styles.icon_assets} type="MaterialIcons" name="hourglass-top"/>
                    <Text style={{ fontSize : 14, color : "#fff"}}>De {props.start_hour} à {props.end_hour}</Text>
                </View>
            </View>
        </View>
    </TouchableWithoutFeedback>
);


const styles = StyleSheet.create({
    container_card : {
        position: 'relative',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderColor : '#161925',
        shadowColor: '#000',
        marginBottom : 30,
        zIndex : 1
    },
    header : { 
        margin : 12, 
        justifyContent : 'space-between', 
        flexDirection : "row", 
        alignItems : "baseline"
    },
    action_arena : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : "space-evenly",
        marginHorizontal : 16,
        marginBottom : 10
    },
    assets : {
        display : 'flex',
        flexDirection : 'row',
        marginHorizontal : 10,
        paddingHorizontal : 10,
        paddingVertical : 10,
        alignItems : "center",
        fontSize : 10,
        borderRadius : 12,
        backgroundColor : '#151520',
        borderWidth : 0,
    },
    icon_assets : {
        color : "#fff",
        paddingHorizontal : 5, 
        color : "#fff", 
        fontSize : 20
    },
    image_arena : {
        width : '100%',
        height : 220,
    },
    arena_type : {
        flexDirection : "column",
        paddingVertical : 5,
        alignItems : "center",
        zIndex: 3,
    },
    game_container : {
        position: "absolute",
        bottom : -25,
        alignSelf : "center",
        justifyContent : "space-between",
        flexDirection : "row",
        backgroundColor : '#fff',
        padding : 10,
        borderRadius : 12,
        alignItems : "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    ranking_container :{
        justifyContent : "flex-end",
        flexDirection :'row',
        alignSelf : "flex-end",
        flexWrap : "wrap",
    },
    amount_bet : {
        position : "relative",
        alignSelf : "flex-end",
    },
    money_bet_text :{
        fontSize : 10,
        alignSelf : "center",
        color : "#fff",
        zIndex: 5,
    },
    money_bet_coin : {
        height : 30,
        width : 30,
    }
});