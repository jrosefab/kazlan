import React, { useState, useContext} from "react";
import { Image,TextInput, StyleSheet, 
        TouchableOpacity, View, Text} from 'react-native';
import {getGames} from '../api/gamesApi'
import { UserContext } from '../contexts/UserProvider';
import { Icon, Input } from "native-base";
import Logo from '../assets/logo.png';

export const HeaderCustom = (props) => {
    const { navigation } = props
    const userApp = useContext(UserContext);
    const [searchTerm, setSearchTerm] = useState({
        query : null,
        searchResults : []
    })

    const handleSearch = async query => {
        setSearchTerm(prevState => ({ ...prevState, query : query }));
        getGames(query).then(({data}) =>{
            setSearchTerm(prevState => ({ ...prevState, searchResults : data.results }));
        })
    };

    const choseGame = (game, image) =>{
        const gameObject = {
            game : game,
            image : image
        }
        userApp.setUser(prevState =>({
            ...prevState,
            communities : [gameObject]
        }))
        setSearchTerm({ query : game, searchResults : null });
        navigation.replace("home")
    }

    return (
        <>
            <View style={styles.header}>
                <Image source={Logo} style={styles.logo}/>
                <TextInput 
                    style={styles.input} 
                    placeholderTextColor="#737373"
                    placeholder="Exemple : Super Smash Bros. Ultimate" 
                    value={searchTerm.query}
                    onChangeText={(query) => handleSearch(query)}
                />
                <Icon style={styles.btn} type="Feather" name={"bell"}/>
            </View>
            {(searchTerm.query && searchTerm.query.length) > 0 &&
                <View style={styles.results}>
                    {searchTerm.searchResults && 
                        searchTerm.searchResults.map(item => (
                            <TouchableOpacity key={item.slug} onPress={() => choseGame(item.name, item.background_image)}>
                                <View style={styles.item}>
                                    <Image 
                                        source={{ uri : item.background_image}}  
                                        style={{ width : 50, height : 50, borderRadius : 10}}
                                    />
                                    <View style={{flexShrink : 1}}>
                                        <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                            {item.name}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity> 
                        ))
                    } 
                </View>
            }
        </>
    );
}

const styles = StyleSheet.create({
    header : {
        backgroundColor : "#151520", 
        justifyContent : "space-between",
        flexDirection : "row",
        alignItems : "center",
        zIndex: 99,
        padding : 5,
        paddingVertical : 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2},
        shadowRadius: 10,
        elevation: 3,
    },
    logo : {
        height: 45,
        width: 50,
        alignSelf : "center",
        padding : 15
    },
    btn : {
        color : "white",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    results : {
        backgroundColor : "#151520", 
        paddingVertical : 10
    },
    item : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        marginVertical : 3,
        marginHorizontal : 10
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
    },
});
