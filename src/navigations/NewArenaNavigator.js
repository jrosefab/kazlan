import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { HeaderCustom } from '../components/HeaderCustom';
import { NewSessionScreen, NewMoneyMatch } from '../screens';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';

const Stack = createStackNavigator();

const NewArenaNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='new_session'>
            <Stack.Screen
                name='new_session' 
                component={NewSessionScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    headerTitle : (props) => (
                        <HeaderCustom current="Créer une session" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack()} {...props}>
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='new_money_match' 
                component={NewMoneyMatch}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    headerTitle : (props) => (
                        <HeaderCustom current="Défier en money match" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack()} {...props}>
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
        </Stack.Navigator>
    )
};

export default NewArenaNavigator; 