import React, { useContext} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {ArenaContext} from "../contexts/ArenaProvider";
import {ChatContext} from "../contexts/ChatProvider";
import { HeaderCustom } from '../components/HeaderCustom';
import {ArenaScreen, ArenaManagementScreen, 
        RoundsScreen, ParticipationArenaScreen,
        ArenaCompletedScreen, ChatScreen} from '../screens';
import CardFormScreen from '../stripe/scenes/CardFormScreen';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';

const Stack = createStackNavigator();

const config = {
    animation: 'spring',
    config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const ArenaNavigator = (props) => {
    const arenaApp = useContext(ArenaContext);
    const chatApp = useContext(ChatContext);

    const {navigation} = props
    return(
        <Stack.Navigator>
            <Stack.Screen 
                name='arena' 
                component={ArenaScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    headerTitle : (props) => (
                        <HeaderCustom current={arenaApp.currentArena.title} {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack()} {...props}>
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='participation_arena' 
                component={ParticipationArenaScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current="Valider mon inscription" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='arena_management' 
                component={ArenaManagementScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current="Gestion de l'arène" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='arena_rounds' 
                component={RoundsScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current={arenaApp.currentArena.title} {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='arena_completed' 
                component={ArenaCompletedScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current="Détails de la session" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='arena_chat' 
                component={ChatScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current={chatApp.currentChat.title} {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
            <Stack.Screen 
                name='arena_payement' 
                component={CardFormScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    animationEnabled: false,
                    headerTitle : (props) => (
                        <HeaderCustom current={arenaApp.currentArena.title} {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack(null)} {...props} >
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
        </Stack.Navigator>
)};

export default ArenaNavigator; 