import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {LoginScreen, SignupScreen} from '../screens';

const Stack = createStackNavigator();

const customOptions =  {
    headerStyle : 
        { backgroundColor: '#fff', 
            elevation: 0,
            shadowOffset: { height: 0 }
        },
        animationEnabled: false,
        headerShown: false
}

const AuthNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name='log_in' 
                component={LoginScreen} 
                options={customOptions}
            />
            <Stack.Screen 
                name='sign_up' 
                component={SignupScreen}
                options={customOptions}
            />
        </Stack.Navigator>
    )
};

export default AuthNavigator; 