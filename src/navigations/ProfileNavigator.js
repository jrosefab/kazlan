import React, {useContext} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {ProfileScreen, ProfileSettingsScreen, 
    AddCommunityScreen} from '../screens/'

const Stack = createStackNavigator();

const customOptions =  {
    headerStyle : 
        { backgroundColor: '#fff', 
            elevation: 0,
            shadowOffset: { height: 0 }
        },
        animationEnabled: false,
        headerShown: false
}

const ProfileNavigator = () => {
    return (
        <Stack.Navigator initialRouteName={'profile'}>
            <Stack.Screen 
                name='profile' 
                component={ProfileScreen}
                options={customOptions}
            />
            <Stack.Screen 
                name='profile_settings' 
                component={ProfileSettingsScreen}
               // options={customOptions}
            />
            <Stack.Screen 
                name='add_community' 
                component={AddCommunityScreen}
                options={customOptions}
            />
        </Stack.Navigator>
    )
};

export default ProfileNavigator; 