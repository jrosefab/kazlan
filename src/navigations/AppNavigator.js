import 'react-native-gesture-handler';
import React, {useEffect, useContext} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';import {TabBar} from '../components/TabBar'
import { UserContext } from '../contexts/UserProvider';
//import { getUserData } from '../api/Authentification'
import { HeaderCustom } from '../components/HeaderCustom';
import { createStackNavigator } from "@react-navigation/stack";
import BottomTabNavigator from './BottomTabNavigator';
import ArenaNavigator from './ArenaNavigator';
import CustomerNavigator from './CustomerNavigator';
import NewArenaNavigator from './NewArenaNavigator';
import firestore from '@react-native-firebase/firestore';
import { retrieveData, storeData, removeData } from '../services/AsyncStorage'
import jwtDecode from 'jwt-decode';
import { CustomerScreen } from '../screens';



const Stack = createStackNavigator()

const config = {
    animation: 'spring',
    config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const customOptions =  {
    headerStyle : 
        { backgroundColor: '#151520', 
            elevation: 0,
            shadowOffset: { height: 0 }
        },
    animationEnabled: false,
    headerShown: false,
    transitionSpec: {
        open: config,
        close: config,
    },
}

const AppNavigator = (props) => {
    const userApp = useContext(UserContext);

    useEffect(() => {
        const newFilter = JSON.stringify([
            "oldest", 
            "sessions", 
            "money_matchs", 
            "online", 
            "offline"
        ])

        retrieveData.retrieveItem("filter").then(filter => {
            if(filter){
                console.log(filter)
            }else{
                storeData.saveItem("filter", newFilter)
            }
        })

        retrieveData.retrieveItem("token").then(jwt => {
            const token = userApp.user.token || jwt
            if (token){
                const decode = jwtDecode(token)
                firestore()
                    .collection('users') 
                    .doc(decode.user_id)
                    .get()
                    .then((doc) => {
                        userApp.setUser(prevState =>({
                            ...prevState,
                            isAuthenticated : true,
                            displayName     : decode.name,
                            photoURL        : decode.picture,
                            uid             : decode.user_id,
                            games           : doc.data().games,
                            token           : token,
                            communities     : doc.data().communities
                        }));
                    })
            }
        })
    }, [userApp.user.token])

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="home">
                <Stack.Screen
                    name="home"
                    component={BottomTabNavigator}
                    options={{
                        headerStyle : 
                        { backgroundColor: '#151520', 
                            elevation: 0,
                            shadowOffset: { height: 0 },
                            height : 60,
                        },
                        animationEnabled: false,
                        header : (props) => (
                            <HeaderCustom main enableButton={true} {...props} />
                        )
                    }}
                />
                <Stack.Screen 
                    name="new_session" 
                    component={NewArenaNavigator}
                    options={customOptions}
                />
                <Stack.Screen 
                    name="arena" 
                    component={ArenaNavigator}
                    options={customOptions}
                />
                <Stack.Screen 
                    name="customer" 
                    component={CustomerNavigator}
                    options={customOptions}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigator;