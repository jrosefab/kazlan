import 'react-native-gesture-handler';
import React, {useEffect, useContext} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {TabBar} from '../components/TabBar';
import { View, Text } from 'react-native';
import { UserContext } from '../contexts/UserProvider';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import AuthNavigator from './AuthNavigator';
import ProfileNavigator from './ProfileNavigator';
import { MoneyMatchScreen, SessionsScreen } from '../screens/';
import ArenaNavigator from './ArenaNavigator';

const  { Navigator, Screen}  = createMaterialTopTabNavigator();

const options = {   
    showIcon  : false, 
    showLabel : true,
    headerStyle: {
        elevation:0,
    },
    indicatorStyle : {
        height: '100%',
        elevation : 0,
        backgroundColor : "#151520", 
        borderBottomWidth : 6,
        borderBottomColor : "#9420c6",
    },
    labelStyle : {
        alignItems : 'center',
        width: 100,
        marginTop: 5,
        elevation:0,
        color : "#fff",
        fontSize : 16,
        textTransform : 'capitalize',
    },
    style : {
        height: 70,
        marginBottom : -1,
        elevation:0,
        zIndex: 99,
        backgroundColor : "#151520", 
        justifyContent : 'center',
    },
}

const MyArenasNavigator = (props) => {
    return (
        <Navigator tabBarOptions={options} 
            sceneContainerStyle={{ backgroundColor : "#151520"}} 
            tabBarPosition='top'
        >
            <Screen name="my_sessions" 
                component={SessionsScreen}
                showLabel={true}
                options = {() => ({ 
                    tabBarLabel : "Mes sessions",
                })}
            />
            <Screen name="my_money_match" 
                component={MoneyMatchScreen}
                options = {() => ({ 
                    tabBarLabel : "Money Match",
                })}
            />
        </Navigator>
    );
};

export default MyArenasNavigator;
