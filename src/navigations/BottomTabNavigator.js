import 'react-native-gesture-handler';
import React, { useContext} from 'react';
import {TabBar} from '../components/TabBar'
import { UserContext } from '../contexts/UserProvider';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AuthNavigator from './AuthNavigator';
import ProfileNavigator from './ProfileNavigator';
import MyArenasNavigator from './MyArenasNavigator';
import { HomeScreen, ChatListScreen } from '../screens/';

const  { Navigator, Screen}  = createBottomTabNavigator();

const options = {
    showLabel : false, 
    style : { 
        height : 65, 
        backgroundColor : "#151520", 
        borderWidth : 0 
    } 
}

const BottomTabNavigator = ({ navigation }) => {
    const userApp = useContext(UserContext);
    return (
        <Navigator tabBarOptions = {options}>
            <Screen name="home" 
                component={HomeScreen}
                options = {( { route }) => ({ 
                    tabBarIcon: ({ focused}) => {
                        let iconColor;
                        if(route.name === "home"){
                            iconColor = focused
                            ? '#20c6bd'
                            : '#151520';
                        }
                        return ( 
                            <TabBar 
                                navigation={() => navigation.replace('home')}
                                page="home"
                                label="Arènes" 
                                type="MaterialCommunityIcons" 
                                name={"boxing-glove"}
                                color={iconColor}
                            />
                        )
                    },
                })}
            />
            <Screen name="my_sessions" 
                component={MyArenasNavigator}
                options = {( { route }) => ({ 
                    tabBarIcon: ({ focused}) => {
                        let iconColor;
                        if(route.name === "my_sessions"){
                            iconColor = focused
                            ? '#20c6bd'
                            : '#151520';
                        }
                        return ( 
                            <TabBar 
                                navigation={() => navigation.navigate('my_sessions')}
                                page="my_sessions"
                                label="Mes arènes" 
                                type="FontAwesome" 
                                name={"pencil-square-o"}
                                color={iconColor}
                            />
                        )
                    },
                })}
            />
            <Screen name="chat_list" 
                component={ChatListScreen}
                options = {( { route }) => ({ 
                    tabBarIcon: ({ focused}) => {
                        let iconColor;
                        if(route.name === "chat_list"){
                            iconColor = focused
                            ? '#20c6bd'
                            : '#151520';
                        }
                        return ( 
                            <TabBar 
                                navigation={() => navigation.navigate('chat_list')}
                                page="chat_list"
                                label="Chat" 
                                type="MaterialCommunityIcons" 
                                name={"message-processing"}
                                color={iconColor}
                            />
                        )
                    },
                })}
            />
            <Screen name={!userApp.user.isAuthenticated ? "log_in" : "profile"} 
                component={!userApp.user.isAuthenticated ? AuthNavigator : ProfileNavigator}
                options = {( { route }) => ({ 
                    tabBarIcon: ({ focused}) => {
                        let iconColor;
                        if(route.name === "log_in" || route.name === "profile"){
                            iconColor = focused
                            ? '#20c6bd'
                            : '#151520';
                        }
                        return ( 
                            <TabBar 
                                navigation={() => navigation.navigate(!userApp.user.isAuthenticated ? "log_in" : "profile")}
                                label={!userApp.user.isAuthenticated ? "Connexion" : "Profil"} 
                                type="FontAwesome" 
                                name={"user-circle"}
                                color={iconColor}
                            />
                        )
                    },
                })}
            />
        </Navigator>
    );
};

export default BottomTabNavigator;