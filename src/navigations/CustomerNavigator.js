import React, { useContext} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {ArenaContext} from "../contexts/ArenaProvider";
import {UserContext} from "../contexts/UserProvider";
import { HeaderCustom } from '../components/HeaderCustom';
import {CustomerScreen} from '../screens';
import CardFormScreen from '../stripe/scenes/CardFormScreen';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';

const Stack = createStackNavigator();

const CustomerNavigator = (props) => {
    const {navigation} = props
    return(
        <Stack.Navigator>
            <Stack.Screen 
                name='customer' 
                component={CustomerScreen}
                options={{
                    headerStyle : 
                    { backgroundColor: '#151520', 
                        elevation: 0,
                        shadowOffset: { height: 0 }
                    },
                    headerTitle : (props) => (
                        <HeaderCustom current="Voir le profil" {...props} />
                    ),
                    headerLeft: (props) => (
                        <TouchableOpacity onPress={() => navigation.goBack()} {...props}>
                            <Icon 
                                name="chevron-left" 
                                type="Entypo" 
                                style={{ color : "#fff", marginLeft : 20 }}
                            />
                        </TouchableOpacity>
                    ),
                }}
            />
        </Stack.Navigator>
)};

export default CustomerNavigator; 