export const messagesNewSession = {
    title : "Vous devez renseigner un titre",
    mode : "Votre sessoin a-t'elle lieu en ligne ou en local",
    description : "N'oubliez pas de décrire votre évenement",
    game : "Veuillez rajouter au un jeu",
    requirement :"Vous devez ajouter une image",
    accepted_ranking :"Veuillez renseigner le niveau requis",
    inscription_limit : "Veuillez renseigner le maximum de personnes",
    region : "Veuillez renseigner votre région",
    zip_code : "Veuillez renseigner votre code postal",
    city : "Veuillez renseigner votre ville",
    date : "Votre évenement n'a pas de date indiqué",
    start_hour : "Il vous faut renseigner la date de début",
    end_hour : "Il vous faut renseigner la date de fin"
};