import React, {useContext, useState, useCallback} from 'react';
import { Title, Content, Icon } from 'native-base';

export const findStatus = (data) => {
    switch (data){
        case "waiting":
            return "En attente"
        case "accepted":
            return "Inscrit"
        case "banned":
            return "Inscription non retenue"
        case "canceled":
            return "Inscription annulée"
        case "in_progress":
            return "En attente d'inscriptions"
        case "started":
            return "L'arène est complète"
        case "litigation":
            return "Litige en cours"
        case "finished":
            return "L'arène est terminée"
    }
}

export const findIconStatus = (data) => {
    switch (data){
        case "waiting":
            return (
                <Icon type="Feather" 
                    style={{fontSize : 20, marginRight : 5,color : "#F4FF87"}} 
                    name="loader" 
                />
            )
        case "accepted":
            return (
                <Icon type="Ionicons" 
                    style={{fontSize : 20, marginRight : 5,color : "#5BFFA2"}} 
                    name="checkmark-circle-outline" 
                />
            )
        case "banned":
            return (
                <Icon type="MaterialIcons" 
                    style={{fontSize : 20, marginRight : 5,color : "#FE628C"}} 
                    name="block" 
                />
            )
        case "canceled":
            return (
                <Icon type="Entypo" 
                    style={{fontSize : 20, marginRight : 5,color : "#6e6e6e"}} 
                    name="cross" 
                />
            )
        case "in_progress":
            return (
                <Icon type="MaterialCommunityIcons" 
                    style={{fontSize : 20, marginRight : 5,color : "#F4FF87"}} 
                    name="account-search-outline" 
                />
            )
        case "started":
            return (
                <Icon type="MaterialCommunityIcons" 
                    style={{fontSize : 20, marginRight : 5,color : "#5BFFA2"}} 
                    name="account-check" 
                />
            )
        case "litigation":
            return (
                <Icon type="Foundation" 
                    style={{fontSize : 20, marginRight : 5,color : "#FE628C"}} 
                    name="alert" 
                />
            )
        case "finished":
            return (
                <Icon type="MaterialCommunityIcons" 
                    style={{fontSize : 20, marginRight : 5,color : "gray"}} 
                    name="timer-off" 
                />
            )
    }
}

export const findColorStatus = (data) => {
    switch (data){
        case "waiting":
            return "#F4FF87"
        case "accepted":
            return "#5BFFA2"
        case "banned":
            return "#FE628C"
        case "canceled":
            return "#6e6e6e"
        case "in_progress":
            return "#F4FF87"
        case "started":
            return "#5BFFA2"
        case "litigation":
            return "#FE628C"
        case "finished":
            return "gray"
    }
}

export const findBestOf = (data) => {
    switch (data){
        case 3:
            return "2 matchs gagnant :"
        case 5:
            return "3 matchs gagnant :"
        case 7:
            return "4 match gagnant :"
        case 9:
            return "5 match gagnat :"
    }
}

export const findMotif = (data) => {
    switch (data){
        case "not_played":
            return "Votre adversaire n'a pas joué"
        case "round_mistake":
            return "Vous ou votre adversaire s'est trompé sur un round"
        case "not_viable":
            return "La connexion ou le lieu n'est pas suffisant"
        case "using_mod":
            return "Utilisation de mod ou d'outils ne permettant de jouer dans des conditions similaire"
    }
}