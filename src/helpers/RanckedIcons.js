import Novice from '../assets/novice.png';
import Amateur from '../assets/amateur.png';
import Medium from '../assets/medium.png';
import Expert from '../assets/expert.png';
import Pro from '../assets/pro.png';
import Elite from '../assets/elite.png';

export const iconArray = [
    {
        name : "novice",
        icon : Novice
    },
    {
        name : "amateur",
        icon : Amateur
    },
    {
        name : "medium",
        icon : Medium
    },
    {
        name : "expert",
        icon : Expert
    },
    {
        name : "pro",
        icon : Pro
    },
    {
        name : "elite",
        icon : Elite
    },
]

export const findIcon = (data) => {
    switch (data){
        case "novice":
            return Novice
        case "amateur":
            return Amateur
        case "medium":
            return Medium
        case "expert":
            return Expert
        case "pro":
            return Pro
        case "elite":
            return Elite
    }
}