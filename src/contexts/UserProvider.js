import React, { createContext, useState } from "react";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
    const [user, setUser] = useState({
        isAuthenticated : false,
        displayName     : null,
        photoURL        : null,
        uid             : null,
        token           : null,
    });
    const [customer, setCustomer] = useState({
        id : null
    });
    return (
        <UserContext.Provider
            value={{
                user,
                customer,
                setCustomer,
                setUser
            }}
            >
            {children}
        </UserContext.Provider>
    );
};