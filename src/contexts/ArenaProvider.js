import React, { createContext, useState } from "react";

export const ArenaContext = createContext();

export const ArenaProvider = ({ children }) => {
    const [currentArena, setCurrentArena] = useState("");
    return (
        <ArenaContext.Provider
            value={{
                currentArena,
                setCurrentArena
            }}
            >
            {children}
        </ArenaContext.Provider>
    );
};