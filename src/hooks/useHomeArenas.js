const useOccasion = () => {
    const firebase = useContext(FirebaseContext)
    const [occasions, setOccasions] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
    const unsubscribe = firebase.db.collection('occasions')
        .onSnapshot(snapshot => {
            if (snapshot.size) {
            let occasionList = []
            snapshot.forEach(doc =>
                occasionList.push({ ...doc.data(), uid: doc.id }),
            )

            setOccasions(occasionList)
            setLoading(false)
            } else {
            setOccasions([])
            setLoading(false)
            }
        })

        return () => {
            unsubscribe()
        }
    }, [])

    return { occasions, loading }
}

const { occasions, loading } = useOccasion()