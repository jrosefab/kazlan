import AsyncStorage from '@react-native-community/async-storage';

export const storeData = {
    async saveItem(key, value) {
        try {
            await AsyncStorage.setItem(key, value);
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    }
};

export const retrieveData = {
    async retrieveItem(data){
        try {
            return await AsyncStorage.getItem(data)
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    }
};

export const removeData = {
    async removeItem(data){
        try {
            return await AsyncStorage.removeItem(data)
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    }
};
