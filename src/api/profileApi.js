import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { Alert} from 'react-native';

export const addNewGame = (ref, updateData) => {
    return new Promise((resolve, reject) => {
        ref.get()
        .then(({_data}) => {
            if (_data.games instanceof Array && _data.games.length > 0 ){
                for( var i = 0; i < _data.games.length; i++){
                    if(_data.games[i].game === updateData.game){
                        return reject(Alert.alert( "Erreur", "Vous avez déjà renseigné ce jeu"))
                    }
                }
                return resolve(ref.update({
                    games : firestore.FieldValue.arrayUnion(updateData) 
                })) 
            }else{
                return resolve(ref.update({
                    games : [updateData]
                }))
            }
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const updateGame = (ref, updateData) => {
    return new Promise((resolve, reject) => {
        const newArray = []
        ref.get()
        .then(({_data}) => {
            _data.games.forEach((game) =>{
                if (game.game === updateData.game){
                    game.rank = updateData.rank
                }
                newArray.push(game);
            });
            return resolve(ref.update({
                games : newArray
            }))
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const deleteGame = (ref, updateData) => {
    return new Promise((resolve, reject) => {
        return resolve(ref.update({
            games : firestore.FieldValue.arrayRemove(updateData) 
        }))
        .catch(error => {
            reject(error)
        })
    })
}

export const uploadProfilePicture = (uri, currentUser) => {
    const filename = uri.substring(uri.lastIndexOf('/') + 1);
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
    const task = storage().ref('profiles/' + currentUser + '/' + filename).putFile(uploadUri);
    
    return new Promise((resolve, reject) => {
        task.then(() => {
            resolve(storage().ref('profiles/' + currentUser + '/' + filename).getDownloadURL())
        })
        .catch((error) => {
            reject(error);
        });
    })
}

export const deleteProfilePicture = (uri, currentUser) => {
    const filename = uri.substring(uri.lastIndexOf('/') + 1);
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
    const task = storage().refFromURL(uri)

    return new Promise((resolve, reject) => {
        resolve(storage().ref(task.fullPath).delete())
        .catch((error) => {
            reject(error);
        });
    })
}