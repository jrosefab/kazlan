import firestore from '@react-native-firebase/firestore';
import { retrieveData } from '../services/AsyncStorage'
import 'moment/locale/fr';

export const addParticipation = (ref, participant) => {
    return new Promise((resolve, reject) => {
        const newArray = []
        ref.get()
        .then(({_data}) => {
            _data.games.forEach((game) =>{
                if (game.game === updateData.game){
                    game.rank = updateData.rank
                }
                newArray.push(game);
            });
            return resolve(ref.update({
                games : newArray
            }))
        })
        .catch(error => {
            reject(error)
        })
    })
}


export const getArena = (ref, participant) => {
    return new Promise((resolve, reject) => {
        const newArray = []
        ref.get()
        .then((doc) => {
            ref
            _data.games.forEach((game) =>{
                if (game.game === updateData.game){
                    game.rank = updateData.rank
                }
                newArray.push(game);
            });
            return resolve(ref.update({
                games : newArray
            }))
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const getArenaByArenasByGame = (communities, limit) => {
    return new Promise((resolve, reject) => {
        const promises = [];
        communities.forEach((game) => {
            promises.push(
                firestore()
                .collection("sessions")
                .where("game.game", "==", `${game.game}`)
                //.limit(limit)
                .get()
                .then(query => {
                    const sessionsArray = [];
                    query.forEach(doc=>{
                        sessionsArray.push(doc.data())
                    })
                    return sessionsArray
                })
            )

            promises.push(
                firestore()
                .collection("money_matchs")
                .where("game.game", "==", `${game.game}`)
                //.limit(limit)
                .get()
                .then(query => {
                    const moneyMatchsArray = [];
                    query.forEach(doc=>{
                        moneyMatchsArray.push(doc.data())
                    })
                    return moneyMatchsArray
                })
            )
        })
        Promise.all(promises).then(data => {
            const userListedArenas = []
            data = [...data]
            data.forEach(arenas => {
                arenas.map(arena => {
                    userListedArenas.push(arena)
                })
            })
            resolve(userListedArenas)
        })
        .catch(error => {
            reject(error)
        })
    })
}

export const  getArenaSortBy = (communityGame) => {
    return new Promise((resolve, reject) => {
    retrieveData.retrieveItem("filter")
    .then((filter) => {
        const sortByArray = []
        filter = JSON.parse(filter)
        if(filter){
            communityGame.forEach(com => {
                /* user has chose sessions */
                if(filter.includes("sessions")){
                    if(
                        com.mode === "online" && 
                        typeof(com.bet) === "undefined" && 
                        filter.includes("online")
                    ){
                        sortByArray.push(com)
                    }
                    if(
                        com.mode === "offline" && 
                        typeof(com.bet) === "undefined" && 
                        filter.includes("offline")
                    ){
                        sortByArray.push(com)
                    }else if(!filter.includes("offline") && !filter.includes("online")){
                        if(typeof(com.bet) === "undefined"){
                            sortByArray.push(com)
                        }
                    }
                }

                /* user has chose money_matchs */
                if(filter.includes("money_matchs")){
                    if(
                        com.mode === "online" && 
                        typeof(com.bet) !== "undefined" && 
                        filter.includes("online")
                    ){
                        sortByArray.push(com)
                    }
                    if(
                        com.mode === "offline" && 
                        typeof(com.bet) !== "undefined" && 
                        filter.includes("offline")
                    ){
                        sortByArray.push(com)
                    }else if(!filter.includes("offline") && !filter.includes("online")){
                        if(typeof(com.bet) !== "undefined"){
                            sortByArray.push(com)
                        }
                    }
                }

                /* user has chose only online */
                if(
                    filter.includes("online") && 
                    !filter.includes("sessions") && 
                    !filter.includes("money_matchs") 
                ){
                    if(com.mode === "online"){
                        sortByArray.push(com)
                    }
                }

                /* user has chose only offline */
                if(
                    filter.includes("offline") && 
                    !filter.includes("sessions") && 
                    !filter.includes("money_matchs") 
                ){
                    if(com.mode === "offline"){
                        sortByArray.push(com)
                    }
                }

                /* user has chose did not chose anything */
                if( 
                    !filter.includes("offline")&& 
                    !filter.includes("online")&&
                    !filter.includes("sessions")&&
                    !filter.includes("money_matchs")
                )
                    sortByArray.push(com)
            })

            if(filter.includes('oldest')){
                sortByArray.sort((a, b) => {
                    return new Date(b.created_date.toDate()) - new Date(a.created_date.toDate());
                });
                resolve(sortByArray)
            }else if(filter.includes('newest')){
                sortByArray.sort((a, b) => {
                    return new Date(a.created_date.toDate()) - new Date(b.created_date.toDate());
                });
                resolve(sortByArray)
            }
        }})
        .catch(error => {
            reject(error)
        })
    })        
}
