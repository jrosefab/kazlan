import axios from "axios";

export const getRegions = () => {
    const url = `https://geo.api.gouv.fr/departements`
    return new Promise((resolve, reject) => {
        axios
        .get(url)
        .then((res) => {
            if(res.status === 200 ){
                resolve(res);
            }
        })
        .catch((error) => {
            if (error.response) {
                    console.log('data', error.response.data);
                    console.log('status', error.response.status);
                    console.log('headers', error.response.headers);
                    reject(error.response)
                } else if (error.request) {
                    console.log('request', error.request);
                    reject(error.request)
                } else {
                    console.log('Error', error.message);
                    reject(error.message)
                };
            reject(error);
        });
    });
};

export const getCity = (zipCode) => {
    const url = `https://vicopo.selfbuild.fr/cherche/${zipCode}`
    return new Promise((resolve, reject) => {
        axios
        .get(url)
        .then((res) => {
            if(res.status === 200 ){
                resolve(res);
            }
        })
        .catch((error) => {
            if (error.response) {
                    console.log('data', error.response.data);
                    console.log('status', error.response.status);
                    console.log('headers', error.response.headers);
                    reject(error.response)
                } else if (error.request) {
                    console.log('request', error.request);
                    reject(error.request)
                } else {
                    console.log('Error', error.message);
                    reject(error.message)
                };
            reject(error);
        });
    });
};