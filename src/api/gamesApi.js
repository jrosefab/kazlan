import axios from "axios";

export const getGames = (query) => {
    const search = axios.create({
        baseURL: "https://api.rawg.io/api/"
    })

    const options = {
        params: {
            search: query,
            page_size: 6
        }
    };
    return new Promise((resolve, reject) => {
        search.get('games', options)
        .then((res) =>{
            if(res.status === 200 ){
                resolve(res);
            }
        })
        .catch((error) => {
            if (error.response) {
                    console.log('data', error.response.data);
                    console.log('status', error.response.status);
                    console.log('headers', error.response.headers);
                    reject(error.response)
                } else if (error.request) {
                    console.log('request', error.request);
                    reject(error.request)
                } else {
                    console.log('Error', error.message);
                    reject(error.message)
                };
            reject(error);
        });
    })
};