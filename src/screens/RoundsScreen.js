import React, {useContext, useState, useCallback} from "react";
import {View, Alert, Text, Image, 
        StyleSheet,Dimensions, 
        TouchableOpacity, 
        TouchableWithoutFeedback} from "react-native";
import 'moment/locale/fr';
import { useClipboard } from '@react-native-community/clipboard'
import firestore from '@react-native-firebase/firestore';
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import {findBestOf, findMotif} from '../helpers/switchHelper';
import { UserContext } from "../contexts/UserProvider";
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading, ScoreChoice, SwipeAction} from "../components";
import ImagePicker from 'react-native-image-picker';
import StarRating from 'react-native-star-rating';
import { Icon, Picker, Textarea, Content } from "native-base";
import Broken from '../assets/broken_glass.png';
import UserDefault from '../assets/default_picture.png';
import Versus from '../assets/versus.png'
import Star from '../assets/star.png'
import Crown from '../assets/crown.png'
import stripe from 'tipsi-stripe'

const UserOpponent = ({ avatar, name, isCurrentPlayer, winScore, looseScore }) => (
    <View>
        <Image style={styles.user_image} source={avatar ? { uri : avatar } : UserDefault }/>
        <Text style={styles.versus_text}>
            {name}
        </Text>
        <View style={{ display: 'flex', flexWrap : "wrap", flexDirection : "row", alignSelf : "center"}}>
            {isCurrentPlayer ?
                [...Array(winScore)].map((e, i) => 
                    <Image style={{ height : 20, width : 20}} key={i} source={Star}/>
                )
            :
                [...Array(looseScore)].map((e, i) => 
                    <Image style={{ height : 20, width : 20}} key={i} source={Star}/>
                )
            }
        </View>
    </View>
)

const ArrowSlider = ({activeRound, limit, player, opponent, onPrevious, onNext}) => (
    <View style={styles.arrow_wrapper}>
        {activeRound !== 1 && 
            <TouchableOpacity onPress={onPrevious}>
                <Icon style={styles.arrow} name="leftcircle" type="AntDesign"/>
                <Text style={{ color : "white", alignSelf : "center", paddingHorizontal : 20}}>
                    Round précédent
                </Text>
            </TouchableOpacity>
        }
        {opponent > 0 && player > 0  &&
            <TouchableOpacity onPress={onNext}>
            <Icon style={styles.arrow} name="rightcircle" type="AntDesign"/>
            <Text style={{ color : "white", alignSelf : "center", paddingHorizontal : 20}}>
                Round suivant
            </Text>
        </TouchableOpacity>
        }
    </View>
)

const StarRatingRender = ({ text, onSelectStar, stars}) => (
    <View style={{ marginVertical : 30}}>
        <Text style={{ color : "white", marginBottom:20, fontWeight :"bold", fontSize :16}}>
            {text}
        </Text>
        <View style={{ paddingHorizontal : 30}}>
            <StarRating
                fullStarColor='#f2bd1a'
                emptyStarColor='#f2bd1a'
                starSize={30}
                disabled={false}
                maxStars={5}
                rating={stars}
                selectedStar={onSelectStar}
            />
        </View>
    </View>
)

export const RoundsScreen = (props) => {
    const { navigation } = props;
    const arenaApp = useContext(ArenaContext);
    const userApp = useContext(UserContext);
    const [ loading, setLoading] = useState(false);
    const [isCopied, setCopied] = useClipboard("");
    const [reclamationSend, setReclamationSend] = useState(false);
    const [ payement, setPayement] = useState({ loading : false, token : ""});
    const [playData, setPlayData] = useState({
        matchs : [],
        active_round : 1,
        round_limit : 0,
        opponent :null,
        creator : null,
        win_score : 0,
        loose_score : 0,
        reclamation : "default",
        custom_reclamation : null
    })
    const [opponent, setOpponent] = useState()
    const [starCount, setStarCount] = useState({
        fairplay : 0,
        host_quality: 0,
        ponctuality : 0,
    })

    const arenaRef = firestore().collection('arenas').doc(arenaApp.currentArena.id)
    const isFocused = useIsFocused();

    const handleCardPayPress = async () => {
        try {
            setPayement({ loading: true, token: null })
            const token = await stripe.paymentRequestWithCardForm({
                smsAutofillDisabled: true,
                requiredBillingAddressFields: 'full',
                prefilledInformation: {
                    billingAddress: {
                        name: 'Gunilla Haugeh',
                        line1: 'Canary Place',
                        line2: '3',
                        city: 'Macon',
                        state: 'Georgia',
                        country: 'FR',
                        postalCode: '31217',
                        email: 'ghaugeh0@printfriendly.com',
                    },
                },
            })
            console.log(token)
            setPayement({ loading: false, token })
        } catch (error) {
            setPayement({ loading: false })
        }
    }

    const generateCode = () => {
        var code = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (var i = 0; i < 6; i++)
            code += possible.charAt(Math.floor(Math.random() * possible.length));
        return code;
    }

    const handleVerified = () => {
        setLoading(true)
        generateCode()
        arenaRef.update({
            step : "finished",
            code : generateCode()
        }).then(() =>{
            setLoading(false)
        })
    }

    const divideAmountBet = (time) => {
        if (time.toString().includes('.')){ 
            return `${time}0`
        }else{
            return `${time}.00`
        }
    }

    const handleReclamation = () => {
        if(playData.reclamation === "other" && !playData.custom_reclamation ){
            return Alert.alert( "Erreur", "Vous devez expliquer le motif de la réclamation") 
        }else{
            Alert.alert(
                'Attention',
                'Vous êtes sur le point de contester les résultats. Cette action bloquera toute inscription à d\'autres arènes tant que le litige ne sera pas traité', // <- this part is optional, you can pass an empty string
                [   {text: 'Annuler'},
                    {text: 'Continuer', onPress: () => { 
                        const reclamation = {
                            from_player : firestore().doc(`users/${userApp.user.uid}`),
                            description : playData.reclamation
                        }
                        setLoading(true)
                        arenaRef.get().then(async(doc) => {
                            arenaRef.update({ 
                                reclamations : firestore.FieldValue.arrayUnion(reclamation) , 
                                step : "litigation" 
                            })
                            .then(()=>{
                                setReclamationSend(true)
                                setLoading(false)
                            })
                        })
                    }}
                ],
                {cancelable: false},
            );
        }
    }

    const handleScore = (round, result) => {
        Alert.alert(
            'Attention',
            'Vous ou votre adversaire ne pourrez plus modifier votre résultat.', // <- this part is optional, you can pass an empty string
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: () => { 
                    setLoading(true)
                    const newArray = []
                    arenaRef.get().then(async(doc) => {
                        await doc.data().matchs.forEach(bo => {
                            if (bo.round === round){
                                bo[playData.current_player] = result === "winner" ? "winner" : "looser";
                                bo[playData.current_opponent] = result === "winner" ? "looser" : "winner"
                            }
                            newArray.push(bo)
                        })
                        arenaRef.update({
                            matchs : newArray,
                        }).then(() => setLoading(false))
                    })
                }}
            ],
            {cancelable: false},
        );
    }

    const checkWhoWin = (bestOf, winScore, looseScore, isCreator) => {
        if((bestOf === 3) && (winScore === 2 || looseScore === 2)){
            setPlayData(prevState =>({
                ...prevState,
                limit : true
            }))
            arenaRef.get().then(doc =>{
                if (!doc.data().winner){
                    setLoading(true)
                    winScore === 2 ? 
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${ 
                            isCreator ?
                            doc.data().creator.id :
                            doc.data().participants[0].user.id
                        }`),
                    })
                    :
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${
                            isCreator ?
                            doc.data().participants[0].user.id :
                            doc.data().creator.id
                        }`),
                    })
                }
                setLoading(false)
            })
        }

        else if((bestOf === 5)&& (winScore === 3 || looseScore === 3)){
            setPlayData(prevState =>({
                ...prevState,
                limit : true
            }))
            arenaRef.get().then(doc =>{
                if (!doc.data().winner){
                    setLoading(true)
                    winScore === 2 ? 
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${ 
                            isCreator ?
                            doc.data().creator.id :
                            doc.data().participants[0].user.id
                        }`),
                    })
                    :
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${
                            isCreator ?
                            doc.data().participants[0].user.id :
                            doc.data().creator.id
                        }`),
                    })
                }
                setLoading(false)
            })
        }

        else if((bestOf === 7) && (winScore === 4 || looseScore === 4)){
            setPlayData(prevState =>({
                ...prevState,
                limit : true
            }))
            arenaRef.get().then(doc =>{
                if (!doc.data().winner){
                    setLoading(true)
                    winScore === 2 ? 
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${ 
                            isCreator ?
                            doc.data().creator.id :
                            doc.data().participants[0].user.id
                        }`),
                    })
                    :
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${
                            isCreator ?
                            doc.data().participants[0].user.id :
                            doc.data().creator.id
                        }`),
                    })
                }
                setLoading(false)
            })
        }

        else if(bestOf === 9 && (winScore === 5 || looseScore === 5)){
            setPlayData(prevState =>({
                ...prevState,
                limit : true
            }))
            arenaRef.get().then(doc =>{
                if (!doc.data().winner){
                    setLoading(true)
                    winScore === 2 ? 
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${ 
                            isCreator ?
                            doc.data().creator.id :
                            doc.data().participants[0].user.id
                        }`),
                    })
                    :
                    arenaRef.update({
                        winner : 
                        firestore().doc(`users/${
                            isCreator ?
                            doc.data().participants[0].user.id :
                            doc.data().creator.id
                        }`),
                    })
                }
                setLoading(false)
            })
        }

        else{
            return setPlayData(prevState => ({ 
                ...prevState, 
                win_score : winScore,
                loose_score : looseScore
            }))
        }
    }

    useFocusEffect(
        useCallback(() => {
            const unsubscribe = firestore()
            .collection('arenas')
            .doc(arenaApp.currentArena.id)
            .onSnapshot(async(doc) => {
                const player = userApp.user.uid === doc.data().creator.id ? "player_1" : "player_2";
                const opponent = userApp.user.uid !== doc.data().creator.id ? "player_1" : "player_2";
                const isCreator = userApp.user.uid === doc.data().creator.id ? true : false
                const isParticipant = userApp.user.uid === doc.data().participants[0].user.id ? true : false

                await firestore()
                .collection('users')
                .doc(doc.data().creator.id)
                .get()
                .then(creator => {
                    arenaApp.setCurrentArena(previousValue => ({
                        ...previousValue,
                        title : doc.data().title,
                        type : doc.data().type,
                        creator : doc.data().creator.id,
                        bet : doc.data().bet,
                        winner : doc.data().winner,
                        step : doc.data().step,
                        reclamations : doc.data().reclamations,
                        code : doc.data().code
                    }))

                    doc.data().reclamations.map(recla => {
                        if(recla.from_player.id === userApp.user.uid){
                            setReclamationSend(true)
                        }
                    })

                    setPlayData(prevState => ({ 
                        ...prevState, 
                        current_player : player,
                        current_opponent : opponent,
                        creator : { 
                            id : doc.data().creator.id,
                            score :0,
                            avatar : creator.data().avatar,
                            name : creator.data().name,
                            is_current_player : isCreator ? true : false
                        },
                        matchs : doc.data().matchs,
                        round_limit : doc.data().matchs.length
                    }))
                })
                
                await firestore()
                .collection('users')
                .doc(doc.data().participants[0].user.id)
                .get()
                .then((user) => {
                    setPlayData(prevState => ({ 
                        ...prevState, 
                        participant : {
                            avatar : user.data().avatar, 
                            id : user.id, 
                            name : user.data().name,
                            score : 0,
                            is_current_player : isParticipant ? true : false
                        },
                    }))
                })

                const roundToWin = doc.data().matchs.length
                let winScore = 0;
                let looseScore = 0;

                const rounds = doc.data().matchs

                for (var i in rounds ){
                    const playerScore = rounds[i][player]
                    const opponentScore = rounds[i][opponent]
                    const isNotEmpty = (playerScore && opponentScore) && (playerScore.length > 0 && opponentScore.length > 0)

                    if(playerScore !== opponentScore && isNotEmpty){
                        playerScore === "winner" ?
                        winScore++ : looseScore ++
                    } 
                }
                checkWhoWin(roundToWin, winScore, looseScore, isCreator)
            })

            return () => { unsubscribe; console.log("complete score screen unmounted") }
        }, [navigation])
    );

    return (
        (loading || !isFocused || !playData || !arenaApp.currentArena.title) ?
            <Loading fullscreen/>
        :
        playData && isFocused &&
        <Layout>
            <Content contentContainerStyle={{ flexGrow: 1 }}>
            {playData.participant &&
            !playData.limit ? 
                <View style={{ flex: 1, paddingHorizontal : 20 }}>
                    <View style={styles.versus_wrapper}>
                        <UserOpponent
                            avatar={playData.creator.avatar && playData.creator.avatar}
                            name={playData.creator.name}
                            isCurrentPlayer={playData.creator.is_current_player}
                            winScore={playData.win_score}
                            looseScore={playData.loose_score}
                        />
                        <Image style={styles.versus} source={Versus}/>
                        <UserOpponent
                            avatar={playData.participant.avatar && playData.participant.avatar}
                            name={playData.participant.name}
                            isCurrentPlayer={playData.creator.is_current_player}
                            winScore={playData.loose_score}
                            looseScore={playData.win_score}
                        />
                    </View>
                    <View style={styles.scoreboard}>
                        {playData.matchs.map(bo => {
                            if (bo.round === playData.active_round)
                            return (
                                <View style={{flex : 1 }}key={bo.round}>
                                    {<ScoreChoice 
                                        matchs={findBestOf(playData.matchs.length)}
                                        opponent={opponent && opponent.name}
                                        round={bo.round} 
                                        isWinner={bo[playData.current_player] === "winner" ? true : false}
                                        isLooser={bo[playData.current_player] === "looser" ? true : false}
                                        isLooseAvailable={bo[playData.current_player].length <= 0 ? true : false}
                                        isWinAvailable={bo[playData.current_player].length <= 0 ? true : false}
                                        onWin={bo[playData.current_player].length <= 0 ? () => 
                                            handleScore(bo.round , 'winner') : () => { return } 
                                        }                                        
                                        onLoose={bo[playData.current_player].length <= 0 ? () => 
                                            handleScore(bo.round , 'looser') : () => { return } 
                                        }                                        

                                    /> }
                                    {bo[playData.current_player].length <= 0 && 
                                        <View style={{ alignSelf : "center"}}>
                                            <Text style={styles.versus_text}>
                                                Saisir mon résultat 
                                            </Text>
                                        </View>
                                    }
                                    {bo[playData.current_opponent].length <= 0 ?
                                        <View style={{ alignSelf : "center", flexDirection : "column", display : "flex"}}>
                                            <Text style={styles.versus_text}>
                                                En attente du résultat de&nbsp;
                                                {arenaApp.currentArena.creator === userApp.user.uid ?
                                                    playData.participant.name :
                                                    playData.creator.name
                                                }
                                            </Text>
                                            <AnimatedEllipsis 
                                                animationDelay={150}
                                                numberOfDots={5}
                                                style={{
                                                    color: '#fff',
                                                    fontSize: 100,
                                                    letterSpacing: 0,
                                                    alignSelf : 'center',
                                                    lineHeight : 80,
                                                }}
                                            />
                                        </View>
                                        :
                                        <Text style={styles.versus_text}>
                                            {arenaApp.currentArena.creator === userApp.user.uid ?
                                                playData.participant.name :
                                                playData.creator.name
                                            } a {bo[playData.current_opponent] === "winner"? 
                                            "gagné " : "perdu "}
                                            ce round
                                        </Text>
                                    }
                                    
                                    <ArrowSlider
                                        activeRound={playData.active_round}
                                        limit={playData.round_limit}
                                        opponent={bo[playData.current_opponent].length}
                                        player={bo[playData.current_player].length}
                                        onPrevious={() => setPlayData(prevState =>({ 
                                            ...prevState, 
                                            active_round : playData.active_round -1 
                                        }))}
                                        onNext={() => setPlayData(prevState =>({ 
                                            ...prevState, 
                                            active_round : 
                                            playData.active_round !== playData.round_limit ? 
                                            playData.active_round  +1 : 
                                            playData.active_round
                                        }))}
                                    />
                                </View>
                            )
                        })}
                    </View>
                </View>
                :
                arenaApp.currentArena.bet && 
                arenaApp.currentArena.winner &&
                playData.limit &&
                <View style={{ flexGrow: 1, paddingHorizontal : 20 }}>
                    <View>
                        { arenaApp.currentArena.winner.id === userApp.user.uid ?
                        <>
                            <View style={styles.result_wrapper}>
                                <Image style={{height : 280, width : 270}} source={Crown}/>
                                <Image 
                                    style={styles.result_user_image} 
                                    source={userApp.user.photoURL ? { uri : userApp.user.photoURL } : UserDefault }
                                />
                            </View>
                            <Text style={[styles.versus_text, { textAlign :"center"}]}>
                                Félicitation {userApp.user.displayName}{"\n"} 
                                Tu remporte ce Money Match de {divideAmountBet(arenaApp.currentArena.bet)} €
                            </Text>
                            {arenaApp.currentArena.step !== "finished" &&  arenaApp.currentArena.step !== "litigation"?
                                <View style={{ alignSelf : "center", flexDirection : "column", display : "flex"}}>
                                    <Text style={styles.versus_text}>
                                        En attente de la validation de&nbsp;
                                        {arenaApp.currentArena.creator === userApp.user.uid ?
                                            playData.participant.name :
                                            arenaApp.currentArena.name
                                        }
                                    </Text>
                                    <AnimatedEllipsis 
                                        animationDelay={150}
                                        numberOfDots={5}
                                        style={{
                                            color: '#fff',
                                            fontSize: 100,
                                            letterSpacing: 0,
                                            alignSelf : 'center',
                                            lineHeight : 80,
                                        }}
                                    />
                                </View>
                            :
                            arenaApp.currentArena.step === "litigation" ?
                            <>
                                <TouchableWithoutFeedback>
                                    <View style={[styles.btn_wrapper, {backgroundColor : "#9e1442"}]}>
                                        <Text style={{ color : "#fff", alignSelf : "center", fontWeight : "bold" }}>
                                        {arenaApp.currentArena.creator === userApp.user.uid ?
                                            playData.participant.name :
                                            arenaApp.currentArena.name
                                        } conteste le résultat
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style={styles.reclamation}>
                                    <Text style={{ color : "white",  marginBottom : 10}}>
                                        Si tu penses être victime d'un mauvais joueur, peux tu justifier la situation :
                                    </Text>
                                    {!reclamationSend ?
                                        <View style={{ display : "flex", flexDirection : "row", marginVertical : 10, }}>
                                            <View style={[styles.input,{ flex : 1, flexDirection : "row", alignItems :"center" }]}>
                                                <Textarea 
                                                    style={{ flex: 1, borderRadius : 15, color : "white"}} 
                                                    rowSpan={2} 
                                                    placeholderTextColor="#737373"
                                                    placeholder="Exemple : Rage quit" 
                                                    onChangeText={(text) => setPlayData(previousValue => ({ 
                                                        ...previousValue, reclamation : text }
                                                    ))}
                                                />
                                            </View>
                                            <TouchableOpacity style={{marginLeft : 10}} onPress={handleReclamation}>
                                                <Icon style={{
                                                    color : "white",
                                                    padding : 10, 
                                                    borderRadius : 100,
                                                    backgroundColor : 'rgba(48, 48, 59, 0.99)'}}
                                                    type="MaterialIcons" 
                                                    name="send"
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    :
                                        <View style={{ marginVertical : 10, flexDirection : "row", alignItems : "center", alignSelf : "center"}}>
                                            <Icon type="Ionicons" style={{color : "#5BFFA2"}} name="checkmark-circle-outline"/>
                                            <Text style={{color : "#5BFFA2"}}>Réclamation en cours de traitement</Text>
                                        </View>
                                    }
                                </View>
                                <Text style={{color:"#fff", alignSelf : "center", marginTop : 10}}>
                                    Un modérateur étudiera la situation. En attendant, tu ne peux ni participer, ni créer de money match
                                </Text>
                            </>
                            :
                            <View>
                                {isCopied === arenaApp.currentArena.code && 
                                    <Text style={styles.copy_success}>Code copié</Text> 
                                }
                                <TouchableOpacity 
                                    onPress={() => setCopied(`${arenaApp.currentArena.code}`)} 
                                    style={[styles.btn_wrapper, { marginBottom :5}]}>
                                    <Text style={{ color : "#fff", alignSelf : "center", fontWeight : "bold" }}>
                                        {arenaApp.currentArena.code}
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{color:"#fff", alignSelf : "center", marginTop : 10}}>
                                    Afin de récupérer les fonds, saisir ce code dans l'espace virement de ton profil
                                </Text>
                            </View>
                            }
                        </>
                        :
                        <>
                            <View style={styles.result_wrapper}>
                                <Image style={{height : 280, width : 270, zIndex : 3}} source={Broken}/>
                                <Image 
                                    style={styles.result_user_image} 
                                    source={userApp.user.photoURL ? { uri : userApp.user.photoURL } : UserDefault }
                                />
                            </View>
                            <Text style={[styles.versus_text, { textAlign :"center"}]}>
                                Dommage {userApp.user.displayName}{"\n"} 
                                Tu perd ce Money Match de {divideAmountBet(arenaApp.currentArena.bet)} €
                            </Text>
                            <SwipeAction 
                                isVerified={arenaApp.currentArena.step !== "finished" ? false : true}
                                onEndReached={handleVerified}
                            />
                            {arenaApp.currentArena.step !== "finished" && 
                                <View style={[styles.reclamation, {marginTop : 20}]}>
                                    {!reclamationSend ?
                                    <>
                                        <Text style={{ color : "white", alignSelf : "center", marginBottom : 10}}>
                                            Si tu contestes ce résultat, merci de saisir un motif
                                        </Text>
                                        <View style={styles.input}>
                                            <Picker
                                                mode="dialog"
                                                iosIcon={<Icon style={{ color : "white"}} type="MaterialIcons" name="arrow-drop-down" />}
                                                style={{ width: undefined, color : "#fff" }}
                                                placeholder="Région"
                                                placeholderStyle={{ color: "#fff" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={playData.reclamation}
                                                onValueChange={recla => setPlayData(previousValue => ({
                                                    ...previousValue, 
                                                    reclamation : recla,
                                                    custom_reclamation : null })
                                                )} 
                                            >
                                                <Picker.Item label="Choisir un motif" value="default"/>
                                                <Picker.Item label="L'adversaire a validé mais je n'ai pas joué"value="not_played"/>
                                                <Picker.Item label="L'adversaire s'est trompé dans un round"value="round_mistake"/>
                                                <Picker.Item label="La connexion ou le lieu n'est pas viable"value="not_viable"/>
                                                <Picker.Item label="Utilisation de mod" value="mod_user"/>
                                                <Picker.Item label="Autre" value="other"/>
                                            </Picker>
                                            {playData.reclamation === "other" &&
                                                <Textarea 
                                                    rowSpan={5} 
                                                    placeholderTextColor="#737373"
                                                    placeholder="Expliquer avec détail le litige" 
                                                    onChangeText={(text) => setPlayData(previousValue => ({ 
                                                        ...previousValue, custom_reclamation : text }
                                                    ))}
                                                />
                                            }
                                        </View>
                                        <TouchableOpacity 
                                            activeOpacity={playData.reclamation !== "default" ? 0.5: 1} 
                                            onPress={playData.reclamation !== "default" ? () => handleReclamation() : () => { return } }>
                                            <View style={[styles.btn_wrapper, {backgroundColor : "#9e1442", opacity : playData.reclamation !== "default" ? 1: 0.5 }]}>
                                                <Text style={{ color : "#fff", alignSelf : "center", fontWeight : "bold" }}>
                                                    Confirmer la réclamation
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </>
                                    :
                                        <View style={{ marginVertical : 10, flexDirection : "row", alignItems : "center", alignSelf : "center"}}>
                                            <Icon type="Ionicons" style={{color : "#5BFFA2"}} name="checkmark-circle-outline"/>
                                            <Text style={{color : "#5BFFA2"}}>Réclamation en cours de traitement</Text>
                                        </View>
                                    }
                                </View>
                            }
                        </>
                        }
                    </View>
                    <StarRatingRender 
                        text={`${playData.creator.is_current_player ? 
                            playData.participant.name :
                            playData.creator.name} a été courtois et fairplay :`}
                        stars={starCount.fairplay}
                        onSelectStar={
                            (rating) => setStarCount(prevState =>({
                            ...prevState,
                            fairplay : rating
                        }))}
                    />
                    <StarRatingRender 
                        text="La connexion ou le lieu est favorable :"
                        stars={starCount.host_quality}
                        onSelectStar={
                            (rating) => setStarCount(prevState =>({
                            ...prevState,
                            host_quality : rating
                        }))}
                    />
                    <StarRatingRender 
                        text="Le money match s'est déroulé dans les temps"
                        stars={starCount.ponctuality}
                        onSelectStar={
                            (rating) => setStarCount(prevState =>({
                            ...prevState,
                            ponctuality : rating
                        }))}
                    />

                    <Text style={styles.category_title}>
                        Récapitulatif :
                    </Text>
                    <View style={{ marginBottom :50}}>
                        {playData.matchs.map(bo => (
                            <View key={bo.round}>
                                <View style={{ borderBottomColor : "#2b2b31", borderBottomWidth : 1 }}>
                                    <ScoreChoice 
                                        minify
                                        opponent={opponent && opponent.name}
                                        round={bo.round} 
                                        isWinner={bo[playData.current_player] === "winner" ? true : false}
                                        isLooser={bo[playData.current_player] === "looser" ? true : false}
                                        isLooseAvailable={bo[playData.current_player].length <= 0 ? true : false}
                                        isWinAvailable={bo[playData.current_player].length <= 0 ? true : false}
                                        onWin={bo[playData.current_player].length <= 0 ? () => 
                                            handleScore(bo.round , 'winner') : () => { return } 
                                        }                                        
                                        onLoose={bo[playData.current_player].length <= 0 ? () => 
                                            handleScore(bo.round , 'looser') : () => { return } 
                                        }                                        
                                    />
                                </View>
                            </View>
                        ))}
                    </View>
                </View>
            }
            </Content>
        </Layout>
    );
}

const styles = StyleSheet.create({
    versus_wrapper : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-evenly",
        width : "100%",
        alignItems : "center",
        marginVertical : 20,
    },
    versus : {
        height : 110,
        width : 110,
    },
    versus_text:{
        color : '#fff', 
        marginVertical : 10, 
        alignSelf: 'center', 
        fontSize : 16, 
        fontWeight : "bold"
    },
    user_image : {
        height : 100,
        width : 100,
        borderRadius : 600
    },
    scoreboard : {
        marginVertical : 6,
        flex : 1,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },
    result_wrapper : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-around",
        marginVertical : 15
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
    },
    arrow_wrapper : {
        display : "flex",
        flexDirection : "row",
        alignSelf : "center",
        bottom : 40,
        position : "absolute",
        elevation:3
    },
    arrow : {
        fontSize : 50,
        color : "#fff",
        margin : 10,
        alignSelf : "center",
        borderRadius : 300
    },
    camera : {
        alignSelf : 'center',
        marginVertical : 20
    },
    image : {
        borderRadius : 15,
        height : 80,
        width : 80,
    },
    copy_success : {
        color : "white",
        alignSelf : "center",
        position : "absolute",
        paddingHorizontal : 10,
        paddingVertical : 2,
        backgroundColor : "#6e6e6e",
        borderRadius : 40
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    result_wrapper :{
        alignSelf : "center",
        position : "relative",
    },
    result_user_image : {
        position : "absolute",
        top : 65,
        right: 59,
        height : 150,
        width : 150,
        borderRadius : 600
    },
    btn_wrapper : {
        alignSelf : "center",
        backgroundColor : "#20c6bd",
        width :"100%",
        borderColor : '#fff',
        borderWidth : 2,
        textAlign : "center",
        paddingVertical : 20,
        borderRadius : 60,
        marginVertical : 30
    }
})