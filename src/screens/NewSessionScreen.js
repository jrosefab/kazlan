import React, { useEffect, useState, useContext } from "react";
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import Tags from "react-native-tags";
import {getGames} from '../api/gamesApi'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { UserContext } from "../contexts/UserProvider";
import { messagesNewSession } from "../helpers/errorForm";
import { View, Image, Text, Alert, 
        StyleSheet, TouchableOpacity,
        FlatList } from "react-native";
import ImagePicker from 'react-native-image-picker';
import { Layout, RankingSelection } from '../components';
import { getRegions, getCity } from '../api/geoFrance';
import Camera from '../assets/camera.jpg'
import {iconArray} from '../helpers/RanckedIcons';
import 'moment/locale/fr';
import { Form,Textarea, Item, 
        Label, Icon, Content, 
        Input, Picker, Button,
        DatePicker, Title, Radio } from "native-base";

const options = {
    title: 'Choisir une image',
    takePhotoButtonTitle : 'Prendre une photo',
    chooseFromLibraryButtonTitle :'Choisir dans la gallerie',
    mediaType : 'photo',
    cameraType: 'back',
    noData : true,
    allowsEditing : true,
    quality : 0.7,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export const NewSessionScreen = (props) => {
    const { handleSubmit, navigation } = props
    const userApp = useContext(UserContext);
    const [ ranking, setRanking ] = useState([])

    const [searchTerm, setSearchTerm] = useState({
        query : null,
        searchResults : []
    })

    const [hourPicker, setHourPicker] = useState({
        start_hour : false,
        end_hour : false
    });

    const [ location, setLocation] = useState({
        zip_code : "",
        regions : [],
        city : []
    })

    const [ image, setImage] = useState({
        image : null,
    })

    const [ arenaDatas, setArenaDatas] = useState({
        created_date : new Date(),
        creator : firestore().doc(`users/${userApp.user.uid}`),
        mode : "offline",
        title : null,
        description : null,
        game : null,
        requirement : [],
        participants : [],
        accepted_ranking : [],
        inscription_limit : 0,
        region : null,
        zip_code : null,
        city : null,
        address : null,
        schedule_date : null,
        step : "in_progress",
        start_hour : null,
        end_hour : null
    });

    const handleFormSubmit = async() => {
        for (const [arenaKey, arenaValue] of Object.entries(arenaDatas)) {
            if (!arenaValue){
                for (const [messageKey, messageValue] of Object.entries(messagesNewSession)) {
                    if(messageKey === arenaKey){
                        return Alert.alert( "Champ manquant", `${messageValue}`)
                    }
                };
            }
        };
        
        firestore().collection("sessions").add({...arenaDatas, accepted_ranking : ranking})
            .then(async(doc) => {
                if(image.image){
                    await uploadImage(doc.id).then((photoUrl) => {
                        firestore().collection("sessions").doc(doc.id).update({
                            image : photoUrl
                        })
                    })
                }
                
                firestore().collection("chats").add({
                    arena : firestore().doc(`sessions/${doc.id}`),
                    title : arenaDatas.title,
                    created_date : new Date(),
                    messages : [],
                    participants :[
                        firestore().doc(`users/${userApp.user.uid}`)
                    ] 
                }).then((chat) => {
                    firestore().collection("sessions").doc(doc.id).update({
                        chats : firestore().doc(`chats/${chat.id}`)
                    })

                    firestore().collection("users").doc(userApp.user.uid).update({
                        chats : firestore.FieldValue.arrayUnion(firestore().doc(`chats/${chat.id}`)),
                        sessions : firestore.FieldValue.arrayUnion({
                            arena : firestore().doc(`sessions/${doc.id}`),
                            role : "creator"
                        })
                    }).then(() =>{
                        navigation.navigate('home')
                    })
                })
            }).catch(error => console.log(error))
    }

    const uploadImage = async(currentArena) => {
        const uri = image.image.uri
        const filename = uri.substring(uri.lastIndexOf('/') + 1);
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref('sessions/' + currentArena + '/' + filename).putFile(uploadUri);

        return new Promise((resolve, reject) => {
            task.then(() => {
                resolve(storage().ref('sessions/' + currentArena + '/' + filename).getDownloadURL())
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    const handlePicture = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                return response.didCancel;
            } else if (response.error) {
                return response.error;
            } else {
                setImage({
                    image : { 
                        uri      : response.uri,
                        type     : response.type,
                        fileName :  response.fileName 
                    }
                });
            }
        });
    }

    const handleRankingChange = (rank) => {
        var index = ranking.findIndex(isExist => isExist === rank)
        if (index === -1){
            setRanking(prevState => ([...prevState, rank]));
        }else{
            setRanking(ranking.filter(itemToRemove => itemToRemove !== rank));
        }
    }
    const handleCity = (zipCode) => {
        var reg = /^\d+$/;
        setLocation(previousValue => ({...previousValue, zip_code : zipCode }))
        if (zipCode.length === 5 && reg.test(zipCode)){
            getCity(zipCode).then(res => {
                console.log(res)
                if (res.data.cities[0]){
                    setArenaDatas(previousValue => ({
                        ...previousValue,
                        zip_code : zipCode, 
                        city : res.data.cities[0].city 
                    }))
                    setLocation(previousValue => ({...previousValue, city : res.data.cities }))
                }
            })
        }
    }

    const makeTwoDigits = (time) => {
        const timeString = `${time}`;
        if (timeString.length === 2) return time
        return `0${time}`
    }

    const convertHour = (hour) => {
        let hours = hour.getHours();
        let minutes = hour.getMinutes();
        console.log(hours, minutes)
        return `${makeTwoDigits(hours)}:${makeTwoDigits(minutes)}`
    }

    const handleSearch = async query => {
        setSearchTerm(prevState => ({ ...prevState, query : query }));
        getGames(query).then(({data}) =>{
            setSearchTerm(prevState => ({ ...prevState, searchResults : data.results }));
        })
    };

    const choseGame = (game, image) =>{
        console.log(game, image)
        setArenaDatas(prevState => ({
            ...prevState,
            game : {
                game : game,
                image : image
            }
        }))
        setSearchTerm({ query : game, searchResults : null });
    }

    useEffect(() => {
        let isSubscribed = true
        getRegions().then(res => {
            if(isSubscribed){
                setLocation(previousValue => ({...previousValue, regions : res.data }))
            }
        })
        return () => isSubscribed = false
    }, []);
    
    return (
        <Layout title={                
            <Title style={{ textAlign : "center", alignSelf : "center"}}>
                Nouvelle arène
            </Title>
        }>  
        <Content>
            <Form style={{ marginVertical : 30, paddingHorizontal : 5}} 
                onSubmit={() => handleSubmit(handleFormSubmit)}
            >
                <View style={styles.camera}>
                    {arenaDatas.image &&
                        <TouchableOpacity 
                            style={styles.cancel}
                            onPress={() => setArenaDatas(previousValue => ({ ...previousValue, image : null }))}
                        >
                            <Icon style={{ color : '#bf3737'}}type="MaterialIcons" name="cancel"/>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity onPress={handlePicture}>
                        <View>
                            <Image style={styles.image} 
                                source={image.image ? { uri : image.image.uri } : Camera}
                            />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Nom de l'arène:</Label>
                    <Input 
                        style={styles.input} 
                        onChangeText={(text) => setArenaDatas(previousValue => ({ 
                            ...previousValue, title : text }
                        ))}
                    />
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Choisir un jeu :</Label>
                    <Input 
                        style={[styles.input, {marginBottom : 3}]} 
                        placeholderTextColor="#737373"
                        placeholder="Exemple : Super Smash Bros. Ultimate" 
                        value={searchTerm.query}
                        onChangeText={(query) => handleSearch(query)}
                    />
                    {searchTerm.searchResults !== null &&  
                    searchTerm.query && 
                    searchTerm.query.length > 0 &&
                        searchTerm.searchResults.map(item => (
                            <TouchableOpacity onPress={() => choseGame(item.name, item.background_image)}>
                            <View style={styles.item}>
                                <Image source={{ uri : item.background_image}}  style={{ width : 50, height : 50, borderRadius : 10}}/>
                                <View style={{flexShrink : 1}}>
                                    <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                        {item.name}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity> 
                    ))}
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Sélectionner le type de session :</Label>
                    <View style={styles.input}>
                        <Picker
                            mode="dialog"
                            iosIcon={<Icon style={{ color : "white"}} type="MaterialIcons" name="arrow-drop-down" />}
                            style={{ width: undefined, color : "#fff" }}
                            placeholder="Région"
                            placeholderStyle={{ color: "#fff" }}
                            placeholderIconColor="#007aff"
                            selectedValue={arenaDatas.mode}
                            onValueChange={mode => setArenaDatas(previousValue => ({...previousValue, mode : mode }))} 
                        >
                            <Picker.Item label="En local"value="offline"/>
                            <Picker.Item label="En ligne"value="online"/>
                        </Picker>
                    </View>
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Description :</Label>
                    <Textarea 
                        style={[styles.input, { borderRadius : 15}]} 
                        rowSpan={5} 
                        placeholderTextColor="#737373"
                        placeholder="Description" 
                        onChangeText={(text) => setArenaDatas(previousValue => ({ 
                            ...previousValue, description : text }
                        ))}
                    />
                </View>

                { arenaDatas.mode === "offline" && 
                    <View style={{ marginLeft : 15, marginRight : 20, marginVertical : 10}}>
                    <Label style={styles.label}>Besoins pour le déroulement (virgule pour ajouter) :</Label>
                        <Tags style={styles.tags}
                            initialText="1 manette PS4"
                            createTagOnString={[","]}
                            onChangeTags={tags => setArenaDatas(previousValue => ({ ...previousValue, requirement : tags}) )}
                            onTagPress={(index, tagLabel, event, deleted) =>
                                console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                            }
                            containerStyle={{ justifyContent: "center" }}
                            inputStyle={styles.input_tag}
                            renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                                <TouchableOpacity style={styles.tag} key={`${tag}-${index}`} onPress={onPress}>
                                    <Text style={{ color : '#fff', marginRight : 10 }}>x</Text>
                                    <Text style={{ color : '#fff' }}>{tag}</Text>
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                }

                <View style={{ marginHorizontal : 10, marginVertical : 20}}>
                    <Label style={styles.label}>Niveau accepté :</Label>
                    <View style={styles.rank_wrapper}>
                        {iconArray.map(r => (
                            <View key={r.name}>
                                <RankingSelection 
                                    style={styles.rank}
                                    onPress={() => handleRankingChange(r.name)}
                                    isSelected={ranking.includes(r.name)}                                    
                                    source={r.icon}
                                    name={r.name.charAt(0).toUpperCase() + r.name.slice(1)}
                                />
                            </View>
                        ))}
                    </View>
                </View>

                <View style={{ marginHorizontal : 25, }}>
                    <Text style={{ color: "#fff", marginTop:20 }}>
                        (Par mesure de sécurité, l'addresse uniquement sera communiqué uniquement à la validation d'inscription)
                    </Text>
                </View>
                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Sélectionner la région :</Label>
                    <View style={styles.input}>
                        <Picker
                            mode="dialog"
                            iosIcon={<Icon style={{ color : "white"}} type="MaterialIcons" name="arrow-drop-down" />}
                            style={{ width: undefined, color : "#fff" }}
                            placeholder="Région"
                            placeholderStyle={{ color: "#fff" }}
                            placeholderIconColor="#007aff"
                            selectedValue={arenaDatas.region}
                            onValueChange={dpt => setArenaDatas(previousValue => ({...previousValue, region : dpt }))} 
                        >
                            {location.regions && location.regions.map(r => (
                                <Picker.Item label={r.nom} value={r.nom} key={r.code} />
                            ))}
                        </Picker>
                    </View>
                </View>
                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Code Postale :</Label>
                    <Input keyboardType="numeric" 
                        style={styles.input} 
                        onChangeText={zipCode => handleCity(zipCode)}
                    />
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Ville :</Label>
                    <View style={styles.input}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon style={{ color : "white"}} type="MaterialIcons" name="arrow-drop-down" />}
                            style={{ width: undefined, color : "#fff" }}
                            placeholder="Ville"
                            placeholderStyle={{ color: "#fff" }}
                            placeholderIconColor="#007aff"
                            selectedValue={arenaDatas.city}
                            onValueChange={city => setArenaDatas(previousValue => ({...previousValue, city : city }))} 
                        >
                            {location.city && location.city.map(r => (
                                <Picker.Item label={r.city} value={r.city} key={r.code} />
                            ))}
                        </Picker>
                    </View>
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Adresse et détails d'accès :</Label>
                    <Textarea 
                        style={[styles.input, { borderRadius : 15}]} 
                        rowSpan={3} 
                        onChangeText={(text) => setArenaDatas(previousValue => ({ 
                            ...previousValue, address : text }
                        ))}
                    />
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>A quelle date :</Label>
                    <View style={styles.input}>
                        <DatePicker
                            defaultDate={new Date()}
                            maximumDate={new Date(2022, 12, 31)}
                            locale={"fr"}
                            timeZoneOffsetInMinutes={undefined}
                            modalTransparent={false}
                            animationType={"fade"}
                            androidMode={"default"}
                            placeHolderText="choisir une date"
                            textStyle={{ color: "#fff" }}
                            placeHolderTextStyle={{ color: "#d3d3d3" }}
                            onDateChange={date => setArenaDatas(previousValue => ({...previousValue, schedule_date : date }))} 
                            disabled={false}
                        /> 
                    </View>
                </View>

                <View style={{ marginVertical : 20, justifyContent : "space-around", display : 'flex', flexDirection : 'row'}}>
                    <View style={[styles.input_wrapper, { width : "45%"}]}>
                        <Label style={styles.label}>Débute à :</Label>
                        <View style={[styles.input, { marginHorizontal : 10}]}>
                            <TouchableOpacity onPress={() => setHourPicker({ start_hour : !hourPicker.start_hour })}>
                                <View style={[styles.input, { paddingVertical : 10}]}>
                                    <Text style={{ color : 'white', alignSelf : "center"}}>
                                        {arenaDatas.start_hour ? arenaDatas.start_hour : "Heure de début"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <DateTimePickerModal
                            isVisible={hourPicker.start_hour}
                            is24Hour={true}
                            mode="time"
                            onConfirm={hour => { 
                                setArenaDatas(previousValue => ({...previousValue, start_hour : convertHour(hour) }));
                                setHourPicker({ start_hour : false }) 
                            }}
                            onCancel={() => setHourPicker({ start_hour : false }) }
                        />   
                    </View>
                    <View style={[styles.input_wrapper, { width : "45%"}]}>
                        <Label style={styles.label}>Prends fin à :</Label>
                        <View style={[styles.input, { marginHorizontal : 10}]}>
                            <TouchableOpacity onPress={() => setHourPicker({ end_hour : !hourPicker.end_hour })}>
                                <View style={[styles.input, { paddingVertical : 10}]}>
                                    <Text style={{ color : 'white', alignSelf : "center"}}>
                                        {arenaDatas.end_hour ? arenaDatas.end_hour : "Heure de fin"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <DateTimePickerModal
                            isVisible={hourPicker.end_hour}
                            is24Hour={true}
                            mode="time"
                            onConfirm={hour => { 
                                setArenaDatas(previousValue => ({...previousValue, end_hour : convertHour(hour) }));
                                setHourPicker({ end_hour : false }) 
                            }}
                            onCancel={() => setHourPicker({ end_hour : false }) }
                        />   
                    </View>
                </View>

                <View style={styles.input_wrapper}>
                    <Label style={styles.label}>Maximum d'inscription :</Label>
                    <Input 
                        keyboardType="numeric" 
                        style={[styles.input, {textAlign : "center"}]} 
                        onChangeText={(max) => setArenaDatas(previousValue => ({ 
                            ...previousValue, inscription_limit : parseInt(max) }
                        ))}
                    />
                </View>

                <TouchableOpacity onPress={handleFormSubmit}>
                    <View style={styles.btn_wrapper}>
                        <Text style={styles.btn_text}>Publier</Text>
                    </View>
                </TouchableOpacity>

            </Form>
        </Content>
    </Layout>
    );
}

const styles = StyleSheet.create({
    container: {
        display : 'flex',
        alignSelf :'flex-start',
    },
    camera : {
        alignSelf : 'center',
        marginBottom : 30
    },
    item : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        marginVertical : 3
    },
    item_wrapper : {
        marginVertical : 5,
    },
    picker : {
        borderRadius : 20,
        height : 80,
        width : 80,
        backgroundColor : '#E4E4E4'
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    slider_wrapper: {
        flex: 1,
        marginHorizontal : 25,
        marginVertical : 20,
        alignItems: "stretch",
        justifyContent: "center"
    },
    cash_bag : {
        position : "relative",
        alignSelf : "center"
    },
    cash_bag_text : {
        position : "absolute",
        zIndex : 3,
        top : 48,
        right: 30,
        fontWeight : "bold",
        fontSize : 16,
        color : "white"
    },
    thumb : {
        width: 20,
        height: 20,
        backgroundColor : '#fff'
    },
    image : {
        borderRadius : 20,
        padding : 30,
        height : 130,
        width : 130,
    },
    label : {
        marginLeft : 10,
        marginBottom : 10,
        color : "#20c6bd"
    },
    input_wrapper : {
        marginHorizontal : 10,
        marginVertical : 10
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10
    },
    input_tag :{
        backgroundColor : 'rgba(48, 48, 59, 0.99)',
        borderRadius : 15,
        color : 'white'
    },
    tags : { 
        borderRadius : 300,
        paddingVertical : 10,
        marginHorizontal : 2
    },
    tag : {
        backgroundColor : '#9420c6',
        borderRadius : 15,
        display : 'flex',
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2
    },
    rank_wrapper : {
        display : 'flex', 
        alignSelf : "center",
        justifyContent : "center",
        flexDirection : 'row', 
        flexWrap : 'wrap',
        marginVertical : 10,
        marginHorizontal : 15,
    },
    rank : {
        marginVertical : 5,
        marginHorizontal : 10,
        padding : 30,
        height : 80,
        width : 80,
    },
    btn_wrapper : {
        backgroundColor : "#20c6bd", 
        borderWidth : 2,
        padding : 20,
        borderRadius : 60,
        marginVertical : 30,
        marginHorizontal : 20
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
});