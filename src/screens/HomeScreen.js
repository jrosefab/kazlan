import React, { useCallback, useState, 
                useContext,  } from "react";
import { View, Image, Text, FlatList,
        StyleSheet, RefreshControl } from "react-native";
import firestore from '@react-native-firebase/firestore';
import { UserContext } from '../contexts/UserProvider';
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import {ArenaContext} from "../contexts/ArenaProvider";
import { FloatingBtn,Loading, CardArena, Layout, SortBy } from "../components";
import {findIcon, iconArray} from '../helpers/RanckedIcons';
import { getArenaByArenasByGame, getArenaSortBy } from "../api/arenaApi";
import MagnifyGlass from '../assets/magnify_glass.png';
import NoImageAvailable from '../assets/default_image.png';
import moment from 'moment';
import 'moment/locale/fr';
import { firebase } from "@react-native-firebase/messaging";

export const HomeScreen = (props) => {
    const { navigation } = props;
    const userApp = useContext(UserContext);
    const arenaApp = useContext(ArenaContext);
    const [ loading, setLoading] = useState(false);
    const [ arenas, setArenas] = useState({
        limit        : 10,
        sessions     : [],
        money_matchs : [],
        list         : []
    });
    const [ hideHeader, setHideHeader] = useState(false);
    const [ refreshing, setRefreshing] = useState(false);
    const isFocused = useIsFocused();

    const useForceUpdate = () => {
        return () => 
            getArenaByArenasByGame(userApp.user.communities, arenas.limit)
            .then(arenas =>{
                getArenaSortBy(arenas).then(sortedArena => {
                    setArenas(prevState =>({
                        ...prevState,
                        list : sortedArena
                    }))
                }).catch(error =>{
                    return error
                })
            }).catch(error =>{
                return error
            })
    }

    const forceUpdate = useForceUpdate();
    
    const getCreatedDate = (data) => {
        return(
            moment(data.toDate()).startOf('hour').fromNow()
        )
    }
    
    const getScheduleDate = (data) => (
        moment(data).format("Do MMM YY")
    );

    const hideSortBy = (position) => {
        if(position > 200){
            setHideHeader(true)
        }else{
            setHideHeader(false)
        }
    }

    const onReset = () => {
        firestore()
        .collection('users') 
        .doc(userApp.user.uid)
        .get()
        .then((doc) => {
            userApp.setUser(prevState =>({
                ...prevState,
                communities     : doc.data().communities
            }));
            navigation.replace("home")
        })
    }

    const onRefresh = useCallback(async () => {
        setRefreshing(true);
        forceUpdate()
        setRefreshing(false)
    }, [refreshing]);
    
    useFocusEffect(
        useCallback(() => {
        setLoading(true)
        const unsubscribe = 
            getArenaByArenasByGame(userApp.user.communities, arenas.limit)
            .then(arenas =>{
                getArenaSortBy(arenas).then(sortedArena => {
                    setArenas(prevState =>({
                        ...prevState,
                        list : sortedArena
                    }))
                    setLoading(false)
                }).catch(error =>{
                    return error
                })
            }).catch(error =>{
                return error
            })

            return () => { unsubscribe; console.log("home unmounted") }
        }, [navigation, userApp.user.communities])
    );

    return (
        <Layout 
            navigation={navigation} 
            sortBy={!hideHeader ? true : false}
            communities={userApp.user.communities}
            onReset={onReset}
        >
            { isFocused && !loading ?
            <>
                {arenas.list.length <= 0 ?
                    <View style={{flex : 1, alignSelf : "center", marginVertical : 50}}>
                        <Image source={MagnifyGlass} style={[styles.notfound, { marginBottom : 30}]}/>
                        <Text style={{ alignSelf : "center", textAlign : "center", color : "#fff", fontSize : 25, fontWeight : "bold"}}>
                            Oups ! {"\n"}Aucuns évènement trouvé
                        </Text>
                        <View style={{ 
                            alignSelf : "center", 
                            marginTop : 10, 
                            marginHorizontal : 50, 
                            justifyContent : 'center'}}
                        >
                            <Text style={{ textAlign : "center", color : "#fff", fontSize : 16}}>
                                Il semblerait qu'aucun évènements n'a été trouvé, 
                                soit le premier à défier ton entourage !
                            </Text>
                        </View>
                    </View>
                    :
                    arenas.list.length > 0 &&
                    <FlatList 
                        contentContainerStyle = {{ position : "relative", zIndex: 1 }}
                        data={arenas.list}
                        horizontal={false}
                        numColumns={1}
                        onScroll={(e) => hideSortBy(e.nativeEvent.contentOffset.y)}
                        renderItem={({ item })=> 
                            <CardArena 
                                getArena={() =>{ 
                                    arenaApp.setCurrentArena({id : item.id}); 
                                    navigation.navigate('arena');
                                }}
                                game={item.game && item.game.game}
                                game_bg={item.game && item.game.image}
                                title={item.title}
                                offline={item.mode === "offline" ? true : false}
                                isMoneyMatch={item.type === "money_match" ? true : false}
                                image={item.image ? {uri :  item.image } : NoImageAvailable}
                                description={item.description}
                                created={getCreatedDate(item.created_date)}
                                schedule_date={getScheduleDate(item.schedule_date)}
                                inscription_limit={item.inscription_limit}
                                start_hour={item.start_hour}
                                end_hour={item.end_hour}
                                money_bet={item.bet}
                                mode={item.mode}
                                accepted_ranking={item.accepted_ranking.map((rank) => (
                                    <View key={rank} style={{alignItems : 'center'}}>
                                        <Image style={styles.ranked_image} source={findIcon(rank)}/>   
                                        <Text style={{ color : "#fff", fontSize : 10, textTransform : "capitalize"}}>
                                            {rank}
                                        </Text> 
                                    </View>
                                    )
                                )}
                                onValidation={() => {
                                    arena.setCurrentArena({id : item.id}); 
                                    navigation.navigate('participation_arena')}
                                }
                            />
                        }
                    keyExtractor={(item, index) => index.toString()}
                    refreshControl=
                    {<RefreshControl 
                        colors={["#20c6bd"]}
                        refreshing={refreshing} 
                        onRefresh={onRefresh} 
                    />}
                />
                }
            </> 
            :
                <Loading fullscreen />
            }
        <FloatingBtn navigation={navigation}/>
        </Layout>
    );
}
const styles = StyleSheet.create({
    notfound : {
        height: 150,
        width: 150,
        alignSelf : "center"
    },
    icon : {
        color : "white",
        backgroundColor : "#9420c6",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    icon_header : {
        color : "white",
        backgroundColor : "#9420c6",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
        justifyContent : "flex-end"
    },
    ranked_image :{
        height : 30,
        width : 30,
        marginVertical : 3,
        marginHorizontal : 6
    },
    logo : {
        height: 280,
        width: 300,
        alignSelf : "center",
        marginTop : 30
    },
});