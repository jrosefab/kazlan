import React, {useContext, useState} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Alert} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { Layout } from '../components';
import { Input, Title, Content, Item, Label, Icon } from 'native-base';
import UserDefault from '../assets/default_picture.png'
import auth from '@react-native-firebase/auth';
//import {UserContext} from "../context/UserProvider";

const options = {
    title: 'Choisir une image',
    takePhotoButtonTitle : 'Prendre une photo',
    chooseFromLibraryButtonTitle :'Choisir dans la gallerie',
    mediaType : 'photo',
    cameraType: 'back',
    noData : true,
    allowsEditing : true,
    quality : 0.7,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const messages = {
    username : "Vous devez renseigner un nom d'utilisateur",
    email : "Vous devez une adresse email",
    password : "Veuillez choisir un mot de passe",
};

export const SignupScreen = (props) => {
    const { onSubmit, navigation } = props
    const [image, setImage] = useState(null)
    const [user, setUser] = useState({
        username: null,
        email : null,
        password:null, 
    });

    const handlePicture = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                return response.didCancel;
            } else if (response.error) {
                return response.error;
            } else {
                setImage({ 
                    uri      : response.uri,
                    type     : response.type,
                    fileName :  response.fileName 
                });
            }
        });
    }

    const uploadImage = async(currentUser) => {
        const uri = image.uri
        const filename = uri.substring(uri.lastIndexOf('/') + 1);
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref('profiles/' + currentUser + '/' + filename).putFile(uploadUri);
        
        return new Promise((resolve, reject) => {
            task.then(() => {
                resolve(storage().ref('profiles/' + currentUser + '/' + filename).getDownloadURL())
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    const handleSignup = async() => {
        for (const [key, value] of Object.entries(user)) {
            if (!value){
                for (const [messageKey, messageValue] of Object.entries(messages)) {
                    if(messageKey === key)
                        return Alert.alert( "Champ manquant", `${messageValue}`)
                };
            }
        }  
        await auth().createUserWithEmailAndPassword(user.email, user.password)
            .then(async() => {
                const newUser = auth().currentUser;
                firestore().collection('users').doc(newUser.uid).set({
                    money_matchs: [],
                    sessions    : [],
                    feedbacks   : [],
                    chats       : [],
                    address     : "",
                    avatar      : "",
                    communities : [],
                    name        : user.username
                })
                if (image){
                    await uploadImage(newUser.uid).then((photoUrl) => {
                        firestore().collection('users').doc(newUser.uid).update({
                            avatar : photoUrl
                        })
                        return newUser.updateProfile({
                            displayName: user.username,
                            photoURL : photoUrl
                        })
                    })
                };
                return newUser.updateProfile({
                    displayName: user.username
                })
            })
            .catch(error => console.log(error))
    }

    return (
        <Layout title={                
            <Title style={{ textAlign : "center", alignSelf : "center"}}>
                Créer un compte
            </Title>
        }>
            <Content>
                <View style={styles.form}>
                    <Text style={{ color : '#fff',fontSize : 50, marginVertical : 20}}>
                        Inscription
                    </Text>
                    <View style={styles.camera}>
                        {image &&
                            <TouchableOpacity 
                                style={styles.cancel}
                                onPress={() => setImage(null)}
                            >
                                <Icon style={{ color : '#bf3737'}}type="MaterialIcons" name="cancel"/>
                            </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={handlePicture}>
                            <View>
                                <Image style={styles.image} 
                                    source={image ? { uri : image.uri } : UserDefault}
                                />

                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.input_wrapper}>
                        <Label style={styles.label}>Nom d'utilisateur :</Label>
                        <Input 
                            style={styles.input} 
                            onChangeText={(text) => setUser(previousValue => ({ 
                                ...previousValue, username : text }
                            ))}
                        />
                    </View>
                    
                    <View style={styles.input_wrapper}>
                        <Label style={styles.label}>Email :</Label>
                        <Input 
                            style={styles.input} 
                            onChangeText={(text) => setUser(previousValue => ({ 
                                ...previousValue, email : text }
                            ))}
                        />
                    </View>

                    <View style={styles.input_wrapper}>
                        <Label style={styles.label}>Mot de passe :</Label>
                        <Input 
                            style={styles.input} 
                            onChangeText={(text) => setUser(previousValue => ({ 
                                ...previousValue, password : text }
                            ))}
                        />
                    </View>

                    <TouchableOpacity onPress={() => { handleSignup() }}>
                        <View style={styles.auth_mode}>
                            <Text style={styles.auth_text}>Crée mon compte</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ marginVertical : 10, display : 'flex', flexDirection : 'row', alignSelf : 'center', alignItems: 'center'}}>
                        <Text style={{ color : "white" }}>
                            Vous avez déjà un compte | 
                        </Text>
                        <TouchableOpacity onPress={() => {navigation.navigate('log_in')}}>
                        <Text style={{ marginLeft : 5, color : "#20c6bd" }}>
                                Se connecter
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Content>
        </Layout>
    );
}

const styles = StyleSheet.create({
    form :{
        padding: 30,
        marginBottom : 20
    },
    camera : {
        alignSelf : 'center',
        marginBottom : 30
    },
    label : {
        marginLeft : 10,
        marginBottom : 10,
        color : "#20c6bd"
    },
    input_wrapper : {
        marginVertical : 10
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    image : {
        padding : 30,
        height : 120,
        width : 120,
        borderRadius : 500
    },
    auth_mode : {
        backgroundColor : 'transparent',
        borderColor : '#fff',
        borderWidth : 2,
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    }
})