import React, {useContext, useState, useCallback} from "react";
import moment from 'moment';
import 'moment/locale/fr';
import firestore from '@react-native-firebase/firestore';
import { UserContext } from "../contexts/UserProvider";
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading, ModalLogin} from "../components";
import {View, Text, Image, StyleSheet, Alert,TouchableOpacity} from "react-native";
import {findIcon, iconArray} from '../helpers/RanckedIcons';
import { Content, Icon } from "native-base";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import NoImageAvailable from '../assets/default_image.png';
import Coin from '../assets/coin.png';

export const ArenaScreen = (props) => {
    const arenaApp = useContext(ArenaContext);
    const userApp = useContext(UserContext);
    const [modal, setModal] = useState(false);
    const [arenaInfos, setArenaInfos] = useState(null)
    const [games, setGames] = useState(null)
    const [creatorInfos, setCreatorInfos] = useState(null)
    const { navigation } = props;
    const isFocused = useIsFocused();

    const fetchFromDoc = (collection, id) => { 
        return new Promise((resolve, reject) => {
            resolve(
                firestore()
                .collection(collection)
                .doc(id)
                .get()
            ).catch(error =>{
                return reject(error)
            })
        })
    }

    const handleSubscribe = () => {
        if(!userApp.user.uid){
            setModal(!modal)
        }else{
            navigation.navigate('participation_arena')
        }
    }
    
    const handleUnsubscribe = () => {
        const arenaRef = firestore().collection('arenas').doc(arenaApp.currentArena.id)
        const userRef = firestore().collection('users').doc(userApp.user.uid)

        Alert.alert( 
            "Attention", 
            "Vous êtes sur le point d'annuler votre inscription.",
            [{text: 'Annuler'},
            {text: 'Continuer', onPress: async() => {
                const newArray = [] 
                return arenaRef.get()
                .then(async(doc)=> {
                    await doc.data().participants.forEach(participant => {
                        if (participant.user.id === userApp.user.uid){
                            participant.status = "canceled"
                        }
                        newArray.push(participant)
                    })
                    arenaRef.update({participants : newArray })
                    userRef.get()
                    .then(async(user) => {
                        newArray.length = 0
                        await user.data().arenas.forEach(arena => {
                            if (arena.arena.id === arenaApp.currentArena.id){
                                arena.status = "canceled"
                            }
                            newArray.push(arena)
                        })
                        userRef.update({ arenas : newArray })
                    })
                })  
            }}],
            {cancelable: true},
        )
    }

    const getScheduleDate = (data) => (
        moment(data).format("Do MMM YY")
    );

    const divideAmountBet = (time) => {
        if (time.toString().includes('.')){ 
            return `${time}0`
        }else{
            return `${time}.00`
        }
    }

    useFocusEffect(
        useCallback(() => {
            console.log("arena screen is mounted")
            const unsubscribe = firestore()
                .collection('games')
                .get()
                .then(querySnapshot => {
                    const gamesArray = []
                    querySnapshot.forEach(function(doc) {
                        gamesArray.push(doc.data());
                    });
                    setGames(gamesArray)
                }); 

                firestore()
                .collection('arenas')
                .doc(arenaApp.currentArena.id)
                .get()
                .then((doc) => {
                    firestore()
                    .collection('arenas')
                    .doc(arenaApp.currentArena.id)
                    arenaApp.setCurrentArena(previousValue => ({
                        ...previousValue,
                        title : doc.data().title,
                        requirement : doc.data().requirement
                    }))
                    doc.data().creator.id
                    fetchFromDoc('users', doc.data().creator.id)
                    .then((creator) =>{
                        setArenaInfos(previousValue => ({ 
                            ...previousValue,
                            ...doc.data(),
                        }))
                        setCreatorInfos({ id : doc.data().creator.id, ...creator.data()})
                    })
                })
                .then(() => {
                    firestore()
                    .collection('arenas')
                    .doc(arenaApp.currentArena.id)
                    .onSnapshot((doc)=>{
                        let isRegisterd = false;
                        let status;
                        for (var i = 0; i < doc.data().participants.length; i++) {
                            if(doc.data().participants[i].user.id === userApp.user.uid ||
                                doc.data().creator.id === userApp.user.uid){
                                isRegisterd = true;
                                status = doc.data().participants[i].status
                            }
                        }
                        setArenaInfos(previousValue => ({ 
                            ...previousValue,
                            ...doc.data(),
                            isRegistered : isRegisterd,
                            status : status
                        }))
                    })
                }); 

            return () => { unsubscribe; console.log("arena screen is unmounted")} 
        }, [navigation])
    );

    return (
    (!arenaInfos && !creatorInfos) ?
        <Loading fullscreen/>
    :
    <Layout>
        <Content>
            {isFocused && 
            <>
                { arenaInfos && creatorInfos && 
                <>
                    <View style={{ padding : 0, margin : 0}}>
                        <Image style={styles.image} source={arenaInfos.image ? {uri :  arenaInfos.image } : NoImageAvailable}/>
                        <View style={{ position :"absolute", top : 10, alignSelf : "center", display : "flex", flexDirection:"row"}}>
                            <View style={styles.assets}>
                                <Icon style={styles.icon_assets} type="MaterialIcons" name="people"/>
                                <Text style={{ fontSize : 14, color : "#fff"}}>{arenaInfos.inscription_limit} max</Text>
                            </View>
                            <View style={styles.assets}>
                                <Icon style={styles.icon_assets} type="MaterialIcons" name="hourglass-top"/>
                                <Text style={{ fontSize : 14, color : "#fff"}}>
                                    De {arenaInfos.start_hour}  
                                    &nbsp;à {arenaInfos.end_hour}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.infos_arena_container}>
                        <View style={{flexDirection : 'row', alignItems :'center'}}>
                            <Image style={styles.avatar} source={{uri : creatorInfos.avatar}} />
                            <View>
                                <Text style={{ fontSize : 26, fontWeight : 'bold', color :'#fff' }}>
                                    {creatorInfos.name}
                                </Text>
                                <Text style={{ fontSize : 16, fontWeight : 'bold', color :'#fff'}}>
                                    Prévu le {getScheduleDate(arenaInfos.schedule_date)}
                                </Text>
                            </View>
                        </View>
                        
                        <View style={{ marginVertical : 20}}>
                            <Text style={{color : '#fff', fontSize : 18 }}>
                                {arenaInfos.description}
                            </Text>
                        </View>

                        <View style={styles.category}>
                            <Text style={styles.category_title}>
                                Jeu :
                            </Text>
                            <View style={styles.games} onPress={() => addCommunity(item.name, item.background_image)}>
                                <Image source={{ uri : arenaInfos.game.image}}  style={{ width : 50, height : 50, borderRadius : 10}}/>
                                <View style={{flexShrink : 1}}>
                                    <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                        {arenaInfos.game.game}
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.category}>
                            <Text style={styles.category_title}>
                            { arenaInfos.type === "session" ?
                                "Niveau accepté :" 
                            :   
                                "Somme mise en jeu :"
                            }
                            </Text>
                            <View style={styles.ranking_container}>
                                { arenaInfos.type === "session" ?
                                    arenaInfos.accepted_ranking.map((rank) => (
                                        <View key={rank} style={{alignItems : 'center'}}>
                                            <Image style={styles.ranked_image} source={findIcon(rank)}/>    
                                            <Text style={{ textTransform : 'capitalize', color : "#fff"}}>
                                                {rank}
                                            </Text> 
                                        </View>
                                    )) 
                                :   
                                <View style={styles.amount_bet}>
                                    <Image source={Coin} style={styles.money_bet_coin}/>
                                    <Text style={styles.money_bet_text}>{divideAmountBet(arenaInfos.bet)}€</Text>
                                </View>
                                }
                            </View>
                        </View>

                        { arenaInfos.type === "session" && 
                            <View style={styles.category}>
                                <Text style={styles.category_title}>
                                    Besoins pour le bon déroulement    
                                </Text>
                                <View style={{ flexDirection : 'row', marginVertical : 10, flexWrap: "wrap"}}>
                                    {arenaInfos.requirement && 
                                        arenaInfos.requirement.map(need => (
                                            <Text style={styles.tag} key={need}>
                                                {need}
                                            </Text> 
                                        ))
                                    }
                                </View>
                            </View>
                        } 

                        <View style={[styles.category, { marginBottom : 30}]}>
                            <Text style={styles.category_title}>
                                {arenaInfos.type === "session" ? "Lieu de la Session :" : "Hôte situé à :"}
                            </Text>
                            <View style={{ display : "flex", flexDirection : "row"}}>
                                <Icon style={{ color : "#fff", fontSize : 20}} type="Ionicons" name="location-sharp"/>
                                <View style={{ marginLeft : 10}}>
                                    <Text style={{ fontSize : 16, color : "#fff"}}>{arenaInfos.zip_code} {arenaInfos.city}</Text>
                                    <Text style={{ fontSize : 16, color : "#fff"}}>{arenaInfos.region}</Text>
                                    <Text style={{ fontSize : 16, color : "#fff", marginTop : 20}}>
                                        L'addresse et les détails d'accès seront 
                                        communiqué lors de la validation d'inscription
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.action_arena}>
                            {(arenaInfos.isRegistered || creatorInfos.id === userApp.user.uid )&& 
                            <>
                                {arenaInfos.step === "started" ? 
                                    <TouchableOpacity onPress={() => navigation.navigate(
                                        arenaInfos.type ==="money_match" ? "arena_rounds" : "arena_completed") }>
                                        <View style={styles.btn_wrapper}>
                                            <Text style={styles.btn_text}>
                                                Accéder à l'arène
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                :         
                                (arenaInfos.step === "in_progress" && creatorInfos.id !== userApp.user.uid) ?                   
                                <TouchableOpacity onPress={() => handleUnsubscribe() }>
                                    <View style={[styles.btn_wrapper, { backgroundColor : "#9e1442"}]}>
                                        <Text style={styles.btn_text}>
                                            Annuler mon inscription
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                :
                                arenaInfos.step === "deleted" ?                   
                                    <TouchableWithoutFeedback>
                                        <View style={[styles.btn_wrapper, { backgroundColor : "gray"}]}>
                                            <Text style={styles.btn_text}>
                                                Cette arène a été supprimé
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                :(creatorInfos.id === userApp.user.uid ) &&
                                    arenaInfos.step === "in_progress"?
                                        <TouchableOpacity onPress={() => navigation.navigate("arena_management")}>
                                            <View style={styles.btn_wrapper}>
                                                <Text style={styles.btn_text}>
                                                    {creatorInfos.id === userApp.user.uid ? "Gérer la session" : "Je m'inscris"}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    :
                                    arenaInfos.step === "finished" &&
                                        <TouchableOpacity onPress={() => navigation.navigate(arenaInfos.type ==="money_match" ? "arena_rounds" : "arena_completed")}>
                                            <View style={styles.btn_wrapper}>
                                                <Text style={styles.btn_text}>
                                                    {creatorInfos.id === userApp.user.uid ? "Gérer la session" : "Accédez à l'arène"}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                </>
                            }
                            {(!arenaInfos.isRegistered && creatorInfos.id !== userApp.user.uid) &&
                            <>
                                {arenaInfos.step === "deleted" ?                   
                                    <TouchableWithoutFeedback>
                                            <View style={[styles.btn_wrapper, {backgroundColor : "gray", opacity : 0.5 }]}>
                                            <Text style={styles.btn_text}>
                                                Cette arène a été supprimé
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                : 
                                arenaInfos.step === "in_progress" ? 
                                    <TouchableOpacity onPress={() => handleSubscribe()}>
                                        <View style={styles.btn_wrapper}>
                                            <Text style={styles.btn_text}>
                                                Je m'inscris
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                :   
                                    <TouchableWithoutFeedback>
                                        <View style={[styles.btn_wrapper, {backgroundColor : "gray", opacity : 0.5 }]}>
                                            <Text style={styles.btn_text}>
                                                Cette arène est complète
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                }
                            </>
                            }
                        </View>
                    </View>
                </>}
            </>}
            </Content>
            <ModalLogin 
                visible={modal}
                onPress={() => {
                    setModal(!modal);
                }}
                onRequestClose={!modal}
                login={()=>{ 
                    navigation.navigate('log_in');
                    setModal(false)}
                }
                signup={()=>{ 
                    navigation.navigate('log_in', { screen : 'sign_up' });
                    setModal(false)
                }}
            />
        </Layout>
    );
}

const styles = StyleSheet.create({
    image : {
        height : 280,
        width : '100%',
    },
    infos_arena_container : {
        padding : 20,
        backgroundColor : '#151520',
        height : '100%',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        transform : [{ translateY : -30}]
    },
    avatar : {
        height : 70,
        width : 70,
        marginRight : 20,
        borderRadius : 300
    },
    category : {
        marginVertical : 6,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },
    games : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        flex : 1,
        marginVertical : 3
    },
    games_list : {
        display : 'flex', 
        flexDirection : 'row', 
        flexWrap : 'wrap',
    },
    tag : {
        backgroundColor : '#9420c6',
        borderRadius : 15,
        display : 'flex',
        color : '#fff',
        flexDirection : 'row',
        alignContent : 'center',
        flexWrap : "wrap",
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2,
    },
    ranked_image :{
        height : 60,
        width : 60,
        marginHorizontal : 6
    },
    ranking_container : {
        display : "flex",
        flexDirection : "row",
        alignSelf : "center",
        flexWrap : "wrap"
    },
    amount_bet : {
        position : "relative",
        marginVertical : 10
    },
    money_bet_text :{
        position: "absolute",
        top : 28,
        right: 20,
        fontSize : 16,
        fontWeight : "bold",
        color : "#a16012",
        zIndex: 5,
    },
    money_bet_coin : {
        height : 80,
        width : 80,
    },
    assets : {
        display : 'flex',
        flexDirection : 'row',
        marginHorizontal : 2,
        paddingHorizontal : 10,
        paddingVertical : 10,
        alignItems : "center",
        fontSize : 10,
        borderRadius : 12,
        backgroundColor : '#151520',
    },
    icon_assets : {
        color : "#fff",
        paddingHorizontal : 5, 
        color : "#fff", 
        fontSize : 20
    },
    btn_wrapper : {
        backgroundColor : '#20c6bd',
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    }
})