import React, {useContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
import { StyleSheet, View, Alert, 
        Text, TouchableOpacity, Image } from 'react-native';
import { Layout } from '../components';
import { storeData } from '../services/AsyncStorage'
import { Input, Content, Title, Item, Label } from 'native-base';
import Logo from '../assets/logo.png';
import {UserContext} from "../contexts/UserProvider";

export const LoginScreen = (props) => {
    const { onSubmit, navigation } = props
    const userApp = useContext(UserContext);
    //const storageInfos = useContext(UserContext);
    const [user, setUser] = useState({
        email: null,
        password:null, 
    });

    const handleLogin = () =>{
        if (!user.email){
            return Alert.alert( "Champ manquant","Veuillez renseigner votre adresse email")
        }
        if (!user.password){
            return Alert.alert( "Champ manquant","Veuillez renseignez votre mot de passe")
        }else{     
            auth().signInWithEmailAndPassword(user.email, user.password)
            .then(() => {
                auth().onAuthStateChanged((connectedUser) => {
                    if (connectedUser) {
                        connectedUser.getIdToken().then((idToken) => { 
                            storeData.saveItem("token", idToken); 
                            userApp.setUser({
                                token : idToken
                            });
                            navigation.navigate('home')
                        });
                    };
                });
            })
            .catch(error => {
                switch (error.code){
                    case "auth/wrong-password":
                        Alert.alert( "Erreur", "Votre mot de passe est incorect")
                        break;
                    case "auth/user-not-found":
                        Alert.alert( "Erreur", "Aucun compte trouvé avec cette adresse email")
                        break;
                    case "auth/too-many-requests":
                        Alert.alert( "Erreur", "Trop de tentative, réessayer dans 5 minutes")
                        break;
                    default:
                        console.log(error.code);
                };
            });
        };
    };

    return (
        <Layout title={                
            <Title style={{ textAlign : "center", alignSelf : "center"}}>
                Se connecter
            </Title>
        }> 
            <Content>
                <Image source={Logo} style={styles.logo}/>
                <View style={styles.form}>
                    <Text style={{ color : '#fff',fontSize : 50, marginVertical : 20}}>
                        Se connecter
                    </Text>
                    <View style={styles.input_wrapper}>
                        <Label style={styles.label}>Email :</Label>
                        <Input 
                            style={styles.input} 
                            onChangeText={(text) => setUser(previousValue => ({ 
                                ...previousValue, email : text }
                            ))}
                        />
                    </View>

                    <View style={styles.input_wrapper}>
                        <Label style={styles.label}>Mot de passe :</Label>
                        <Input 
                            style={styles.input} 
                            onChangeText={(text) => setUser(previousValue => ({ 
                                ...previousValue, password : text }
                            ))}
                        />
                    </View>

                    <TouchableOpacity onPress={() => { handleLogin() }}>
                        <View style={styles.auth_mode}>
                            <Text style={styles.auth_text}>Se connecter</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ marginVertical : 10, display : 'flex', flexDirection : 'row', alignSelf : 'center', alignItems: 'center'}}>
                        <Text style={{ color : "white" }}>
                            Nouveau sur Kazlan ? | 
                        </Text>
                        <TouchableOpacity onPress={() => {navigation.navigate('sign_up')}}>
                            <Text style={{ marginLeft : 5, color : "#20c6bd" }}>
                                Créer mon compte
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Content>
        </Layout>
    );
}

const styles = StyleSheet.create({
    form :{
        paddingHorizontal : 30,
        marginBottom : 20
    },
    label : {
        marginLeft : 10,
        marginBottom : 10,
        color : "#20c6bd"
    },
    input_wrapper : {
        marginVertical : 10
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10
    },
    auth_mode : {
        backgroundColor : 'transparent',
        borderColor : '#fff',
        borderWidth : 2,
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
    logo : {
        height: 180,
        width: 185,
        alignSelf : "center",
        marginTop : 30
    },
})