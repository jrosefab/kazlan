import React, {useContext, useState} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Alert} from 'react-native';
import { UserContext } from '../contexts/UserProvider';
import ImagePicker from 'react-native-image-picker';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { Layout } from '../components';
import { Input, Content, Item, Label, Title } from 'native-base';
import auth from '@react-native-firebase/auth';


export const ProfileSettingsScreen = (props) => {
    const { navigation } = props;
    const currentUser = auth().currentUser;
    const userApp = useContext(UserContext);
    const [image, setImage] = useState(null)

    return (
        <Layout title={                
            <Title style={{ textAlign : "center", alignSelf : "center"}}>
                Bonjour {userApp.user.displayName}
            </Title>
        }>            
            <Content>
                <View style={styles.form}>
                    <View style={{ marginRight : 20, marginVertical : 20}}>
                        <Item>
                            <Label style={{ color : "white"}}>Nom d'utilisateur :</Label>
                            <Input 
                                style={styles.input} 
                                placeholder={currentUser.displayName}
                                placeholderTextColor="#737373"
                                onChangeText={(text) => setUser(previousValue => ({ 
                                    ...previousValue, username : text }
                                ))}
                            />
                        </Item>
                    </View>
                    <View style={{ marginRight : 20, marginVertical : 20}}>
                        <Item>
                            <Label style={{ color : "white"}}>Email :</Label>
                            <Input 
                                style={styles.input} 
                                placeholder={currentUser.email}
                                placeholderTextColor="#737373"
                                onChangeText={(text) => setUser(previousValue => ({ 
                                    ...previousValue, email : text }
                                ))}
                            />
                        </Item>
                    </View>
                    <TouchableOpacity onPress={() => { handleLogin() }}>
                        <View style={styles.auth_mode}>
                            <Text style={styles.auth_text}>Sauvegarder</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { handleLogin() }}>
                        <View style={styles.auth_mode}>
                            <Text style={styles.auth_text}>Réinitialiser mon mot de passe</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { handleLogin() }}>
                        <View style={[styles.auth_mode, {backgroundColor : '#c6203e'}]}>
                            <Text style={styles.auth_text}>Supprimer mon compte</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </Content>
        </Layout>
    );
};

const styles = StyleSheet.create({
    form :{
        padding: 30,
        marginBottom : 20
    },
    camera : {
        alignSelf : 'center',
    },
    input : {
        color : "#fff"
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    image : {
        padding : 30,
        height : 120,
        width : 120,
        borderRadius : 500
    },
    auth_mode : {
        backgroundColor : 'transparent',
        borderColor : '#fff',
        borderWidth : 2,
        padding : 20,
        borderRadius : 20,
        marginTop : 10
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    }
})