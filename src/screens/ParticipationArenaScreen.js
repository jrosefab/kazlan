import React, {useContext, useState, useEffect} from "react";
import moment from 'moment';
import 'moment/locale/fr';
import firestore from '@react-native-firebase/firestore';
import { UserContext } from "../contexts/UserProvider";
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading, ModalLogin} from "../components";
import {View, Text, Alert, StyleSheet, TouchableOpacity} from "react-native";
import {findIcon, iconArray} from '../helpers/RanckedIcons';
import { Label, Textarea, Content } from "native-base";

export const ParticipationArenaScreen = (props) => {
    const arenaApp = useContext(ArenaContext);
    const userApp = useContext(UserContext);
    const [ modal, setModal] = useState(false);
    const [arenaInfos, setArenaInfos] = useState(null)
    const [participationDetails, SetParticipationDetails] = useState({
        user : firestore().doc(`users/${userApp.user.uid}`),
        status : "waiting",
        motivation : null,
        ressource : null,
        
    })
    const { navigation } = props;

    const handleParticipation = () => {
        var arenaRef = firestore().collection('arenas').doc(arenaApp.currentArena.id)
        var userRef = firestore().collection('users').doc(userApp.user.uid)
        arenaRef.get().then((doc) => {
            const newParticipantArray = []
            let hasAlreadySign = false;
            const participants = doc.data().participants

            for(var i in participants){
                if (participants[i].user.id === userApp.user.uid) {
                    if(participants[i].status === "canceled"){
                        participants[i].status = "waiting"
                        hasAlreadySign = true
                    }else{
                        return Alert.alert( "Oups !", "Vous vous êtes déjà inscrit à cette session")
                    }
                }
            }

            if(!participationDetails.motivation || !participationDetails.ressource){
                Alert.alert(
                    'Attention',
                    'Un ou plusieurs champs n\'ont pas été remplit, êtes vous sûr(e) de continuer ?', // <- this part is optional, you can pass an empty string
                    [   {text: 'Annuler'},
                        {text: 'Continuer', onPress: () => { 
                            if(hasAlreadySign){
                                newParticipantArray.push(...participants)
                                arenaRef.update({ participants : participants}).then(() =>{
                                    userRef.get().then((doc => {
                                        newParticipantArray.length = 0;
                                        doc.data().arenas.map(arena => {
                                            if(arena.arena.id === arenaApp.currentArena.id){
                                                arena.status = "waiting";
                                            }
                                            newParticipantArray.push(arena)
                                        })
                                        navigation.replace('home')
                                        return userRef.update({ arenas : newParticipantArray })
                                    }))
                                })
                            }else{
                                return arenaRef.update({ participants : firestore.FieldValue.arrayUnion(participationDetails) })
                                .then(()=> {
                                    userRef.update({ arenas : firestore.FieldValue.arrayUnion({ 
                                            arena : firestore().doc(`arenas/${arenaApp.currentArena.id}`),
                                            status : "waiting",
                                            role : "participant" 
                                        }) 
                                    })
                                    navigation.replace('home')
                                })
                            }
                        }}
                    ],
                    {cancelable: false},
                );
            }else{
                return arenaRef.update({ participants : firestore.FieldValue.arrayUnion(participationDetails) })
                .then(()=> {
                    userRef.update({ arenas : firestore.FieldValue.arrayUnion({ 
                            arena : firestore().doc(`arenas/${arenaApp.currentArena.id}`),
                            status : "waiting",
                            role : "participant" 
                        }) 
                    })
                    Alert.alert( "Félicitation", "Vous êtes inscrit à la session, vous recrevrez une notification lorsque celle-ci sera validée !")
                    navigation.replace('home')
                })  
            }
        })
    }

    return (
    (!arenaApp.currentArena.title) ?
        <Loading fullscreen/>
    :
        <Layout>
            <Content>
                <View style={{ padding : 30 }}>
                    <Text style={{ color : '#fff',fontSize : 50, marginVertical : 20}}>
                        Plus qu'une étape...
                    </Text>
                    <View style={{}}>
                        <Label style={{ color : "white"}}>
                            Ton inscription sera validé sous réserve d'acceptation. {"\n"} {"\n"}
                            Afin de d'augmenter tes chances, tu peux dire quelques mots sur toi :
                        </Label>
                        <Textarea 
                            style={{ borderRadius : 10, marginTop : 20, color : "#fff"}} 
                            placeholderTextColor="#737373"
                            rowSpan={5} 
                            bordered 
                            placeholder="En quelques mots..." 
                            onChangeText={(text) => SetParticipationDetails(previousValue => ({
                                ...previousValue,  
                                motivation : text })
                            )}
                        />
                    </View>
                    
                    {arenaApp.currentArena.requirement.length > 0 && 
                        <View style={{ paddingTop : 20, borderTopColor : "white", borderTopWidth : 1, marginTop : 20}}>
                            <Label style={{ color : "white"}}>
                                Pensez tu pouvoir répondre aux besoins de la session ? 
                            </Label>
                            <View style={{ flexDirection : 'row', marginVertical : 10, flexWrap : "wrap"}}>
                                {arenaApp.currentArena.requirement.map(need => (
                                    <Text style={styles.tag} key={need}>
                                        {need}
                                    </Text> 
                                ))}
                            </View>
                            <Textarea 
                                style={{ borderRadius : 10, marginTop : 20, color : "#fff"}} 
                                placeholderTextColor="#737373"
                                rowSpan={5} 
                                bordered 
                                placeholder="Si tu souhaites emmener autre chose, tu peux le préciser (nouritures, câbles...)" 
                                onChangeText={(text) => SetParticipationDetails(previousValue => ({
                                    ...previousValue,  
                                    ressource : text })
                                )}
                            />
                        </View>
                    }
                    <View style={{ marginVertical : 30}}>
                        <TouchableOpacity onPress={handleParticipation}>
                            <View style={styles.btn_wrapper}>
                                <Text style={styles.btn_text}>Valider mon inscription</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Content>
        </Layout>
    );
}

const styles = StyleSheet.create({
    image : {
        height : 280,
        width : '100%',
    },
    infos_arena_container : {
        padding : 20,
        backgroundColor : '#151520',
        height : '100%',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        transform : [{ translateY : -30}]
    },
    avatar : {
        height : 70,
        width : 70,
        marginRight : 20,
        borderRadius : 300
    },
    category : {
        marginVertical : 6,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },
    games : {
        width : 75,
        height : 75,
        borderRadius : 15,
        marginVertical : 6,
        marginHorizontal : 6,
        padding: 24,
    },
    games_list : {
        display : 'flex', 
        flexDirection : 'row', 
        flexWrap : 'wrap',
    },
    tag : {
        backgroundColor : '#9420c6',
        borderRadius : 15,
        display : 'flex',
        color : '#fff',
        flexDirection : 'row',
        alignContent : 'center',
        flexWrap : "wrap",
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2
    },
    ranked_image :{
        height : 60,
        width : 60,
        marginHorizontal : 6
    },
    ranking_container : {
        display : "flex",
        flexDirection : "row",
        alignSelf : "center",
    },
    assets : {
        display : 'flex',
        flexDirection : 'row',
        marginHorizontal : 2,
        paddingHorizontal : 10,
        paddingVertical : 10,
        alignItems : "center",
        fontSize : 10,
        borderRadius : 12,
        backgroundColor : '#151520',
    },
    icon_assets : {
        color : "#fff",
        paddingHorizontal : 5, 
        color : "#fff", 
        fontSize : 20
    },
    btn_wrapper : {
        backgroundColor : '#20c6bd',
        borderColor : '#fff',
        borderWidth : 2,
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    }
})