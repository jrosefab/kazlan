import React, {useContext, useState, useEffect} from "react";
import moment from 'moment';
import 'moment/locale/fr';
import firestore from '@react-native-firebase/firestore';
import {findStatus, findColorStatus} from '../helpers/switchHelper';
import { UserContext } from "../contexts/UserProvider";
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading, ModalLogin} from "../components";
import {View, Alert, Text, Image, StyleSheet, TouchableOpacity} from "react-native";
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import { Icon, Textarea, Content } from "native-base";
import UserDefault from '../assets/default_picture.png'

export const ArenaManagementScreen = (props) => {
    const { navigation } = props;
    const arenaApp = useContext(ArenaContext);
    const userApp = useContext(UserContext);
    const [ loading, setLoading] = useState(false);
    const [participantsList, setParticipantsList] = useState([])
    const isFocused = useIsFocused();

    const arenaReference = firestore().collection('arenas').doc(arenaApp.currentArena.id)

    const fetchFromDoc = (collection, id) => { 
        return new Promise((resolve, reject) => {
            resolve(
                firestore()
                .collection(collection)
                .doc(id)
                .get()
            ).catch(error =>{
                return resolve(error)
            })
        })
    }

    const deleteArena = () => {
        Alert.alert(
            'Attention',
            'Êtes vous sûr(e) de vouloir bannir cette arène ?',
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: async() => { 
                    firestore()
                    .collection("arenas")
                    .doc(arenaApp.currentArena.id)
                    .update({
                        step : "deleted"
                    })
                    .then(() => {
                        firestore()
                        .collection("users")
                        .doc(userApp.user.uid)
                        .get()
                        .then(user => {
                            console.log(user)
                        })
                    });
                    navigation.goBack(null)
                }} 
            ],
            {cancelable: false},
        );
    }

    const handleBan = (participantId) => {
        Alert.alert(
            'Attention',
            'Êtes vous sûr(e) de vouloir bannir ce participant ?', // <- this part is optional, you can pass an empty string
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: async() => { 
                    const newArray = []
                    fetchFromDoc('arenas', arenaApp.currentArena.id)
                    .then(async(doc) => {
                        const newStateArray = []
                        for (var i in participantsList) {
                            if (participantsList[i].id == participantId) {
                                participantsList[i].status = "banned";
                            }
                        }
                        newStateArray.push(...participantsList)
                        setParticipantsList(newStateArray)
                        
                        await doc.data().participants.forEach(participant => {
                            if (participant.user.id === participantId){
                                participant.status = "banned"
                            }
                            newArray.push(participant)
                        })

                        firestore()
                            .collection('arenas')
                            .doc(arenaApp.currentArena.id)
                            .update({
                                participants : newArray
                            })

                        firestore()
                            .collection('users')
                            .doc(participantId)
                            .get()
                            .then(async(user) => {

                                firestore()
                                .collection('chats')
                                .get()
                                .then((query) => {
                                    query.forEach(doc => {
                                        if(doc.data().arena.id === arenaApp.currentArena.id){
                                            firestore().collection('chats').doc(doc.id).update({
                                                participants : firestore.FieldValue.arrayRemove(firestore().doc(`users/${participantId}`))
                                            })
                                            
                                            firestore().collection('users').doc(participantId).update({
                                                chats : firestore.FieldValue.arrayRemove(firestore().doc(`chats/${doc.id}`))
                                            })
                                        }
                                    })
                                })

                                newArray.length = 0
                                await user.data().arenas.forEach(arena => {
                                    if (arena.arena.id === arenaApp.currentArena.id){
                                        arena.status = "banned"
                                    }
                                    newArray.push(arena)
                                })

                                firestore()
                                    .collection('users')
                                    .doc(participantId).update({
                                        arenas : newArray
                                    })
                            })
                    })
                }} 
            ],
            {cancelable: false},
        );
    }

    const handleAccept = (participantId) => {
        Alert.alert(
            'Attention',
            'Êtes vous sûr(e) d\'accepter ce participant ?', // <- this part is optional, you can pass an empty string
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: async() => {
                    const newArray = []
                    const acceptedArray = []
                    fetchFromDoc('arenas', arenaApp.currentArena.id)
                    .then(async(doc) => {
                        doc.data().participants.map(participant => {
                            if (participant.status === "accepted"){
                                acceptedArray.push(participant)
                            }
                        })
                        const newStateArray = []
                        for (var i in participantsList) {
                            if (participantsList[i].id === participantId) {
                                participantsList[i].status = "accepted";
                            }
                        }
                        newStateArray.push(...participantsList)
                        setParticipantsList(newStateArray)

                        await doc.data().participants.forEach(participant => {
                            if (participant.user.id === participantId){
                                participant.status = "accepted"
                            }
                            newArray.push(participant)
                        })

                        firestore()
                        .collection('arenas')
                        .doc(arenaApp.currentArena.id)
                        .update({
                            participants : newArray,
                        })

                        firestore()
                        .collection('users')
                        .doc(participantId)
                        .get()
                        .then(async(user) => {
                            firestore()
                            .collection('chats')
                            .get()
                            .then((query) => {
                                query.forEach(doc => {
                                    if(doc.data().arena.id === arenaApp.currentArena.id){
                                        firestore().collection('chats').doc(doc.id).update({
                                            participants : firestore.FieldValue.arrayUnion(firestore().doc(`users/${participantId}`))
                                        })
                                        
                                        firestore().collection('users').doc(participantId).update({
                                            chats : firestore.FieldValue.arrayUnion(firestore().doc(`chats/${doc.id}`))
                                        })
                                    }
                                })
                            })

                            newArray.length = 0
                            await user.data().arenas.forEach(arena => {
                                if (arena.arena.id === arenaApp.currentArena.id){
                                    arena.status = "accepted"
                                }
                                newArray.push(arena)
                            })

                            const acceptedArray = []
                            doc.data().participants.map(participant => {
                                if (participant.status === "accepted"){
                                    acceptedArray.push(participant)
                                }
                            })

                            if(doc.data().inscription_limit === acceptedArray.length){
                                firestore()
                                .collection('arenas')
                                .doc(arenaApp.currentArena.id)
                                .update({
                                    step : "started"
                                }).then(() =>{
                                    return(                 
                                        Alert.alert(
                                            'Félicitation',
                                            'Votre arène à atteint la limite d\'inscription. Vous pouvez maintenant accéder au tableau de bord', // <- this part is optional, you can pass an empty string
                                            [   
                                                {text: 'Y Aller', onPress: async() => { 
                                                    navigation.replace('arena_completed')
                                                }}
                                            ],
                                            {cancelable: false},
                                        )
                                    );
                                })
                            }

                            firestore()
                                .collection('users')
                                .doc(participantId).update({
                                    arenas : newArray
                                })
                            })
                        }
                    )
                }} 
            ],
            {cancelable: false},
        );
    }

    useEffect(() => {
        let unsubscribe = false
        if (!unsubscribe){
            firestore()
            .collection('arenas')
            .doc(arenaApp.currentArena.id)
            .get()
            .then((doc) => {
                const promises = []
                doc.data().participants.forEach(participant => {
                    const participantRef = firestore().collection('users').doc(participant.user.id)
                    promises.push(participantRef.get()
                        .then((user) => {
                            console.log(user)
                            user = {
                                image : user.data().avatar, 
                                id : user.id, 
                                name : user.data().name,
                                motivation : participant.motivation,
                                ressource : participant.ressource,
                                status : participant.status
                            }
                            return user
                        })
                    )
                })
                Promise.all(promises).then(data => {
                    console.log(data)
                    return setParticipantsList(data)
                })
            })
        }
        setLoading(false)
        return () => { unsubscribe = true; console.log('gestion unmounted')}
    }, [navigation]);

    return (
        <Layout>
            <Content style={{ paddingHorizontal : 20 }}>
                {isFocused ? 
                    <>
                    <View style={{ display : "flex", flexDirection : "row", justifyContent : "space-evenly", marginTop : 20}}>
                        <TouchableOpacity onPress={deleteArena}>
                            <View style={styles.icon_wrapper}>
                                <Icon style={styles.icon_header}
                                    name="trash" 
                                    type="FontAwesome"
                                />
                            </View>
                            <Text style={{ color : "#fff", alignSelf : "center"}}>
                                Supprimer
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.icon_wrapper}>
                                <Icon style={styles.icon_header}
                                    name="pencil" 
                                    type="FontAwesome"
                                />
                            </View>
                            <Text style={{ color : "#fff", alignSelf : "center"}}>
                                Modifier
                            </Text>
                        </TouchableOpacity>
                    </View>
                    { participantsList.length <= 0 ?
                        <View style={{ alignSelf : "center", marginVertical : 30 }}>
                            <Text style={styles.category_title}>
                                Aucun inscrit pour le moment
                            </Text>
                        </View>
                    :
                    <>
                        <Text style={[styles.category_title, { marginTop : 30}]}>
                            {participantsList.length} demande d'inscription :
                        </Text>

                        {participantsList.map(p => (
                            <View key={p.id} style={styles.card_container}>
                                <View style={styles.card_header}>
                                    <Image style={styles.card_image} source={p.image ? { uri : p.image } : UserDefault }/>
                                    <View style={{ alignItems : "baseline"}}>
                                        <Text style={[styles.card_text, {fontWeight : "bold", fontSize : 20}]}>
                                            {p.name}
                                        </Text>
                                        <View style={styles.card_indicator}>
                                        {loading ?
                                            <Loading/>
                                        :
                                        <>
                                            <View style={[styles.card_dot, { backgroundColor : findColorStatus(p.status) }]}/>
                                            <Text style={{ color : findColorStatus(p.status)}}>{findStatus(p.status)}</Text>
                                        </>
                                        }
                                        </View>
                                    </View>
                                </View>
                                {p.motivation &&
                                    <Text style={[styles.card_text, {
                                        marginVertical : 10, 
                                        paddingHorizontal : 40 
                                    }]}>
                                        {p.motivation}
                                    </Text>
                                }
                                {p.ressource &&
                                    <Text style={[styles.card_text, {
                                        marginVertical : 10, 
                                        paddingHorizontal : 40 
                                    }]}>
                                        {p.ressource}
                                    </Text>
                                }
                                {p.status !== "canceled" &&
                                    <View style={styles.card_action}>
                                        <TouchableOpacity onPress={() => handleBan(p.id)}>
                                            <View style={[styles.card_btn, { backgroundColor : "#FE628C"}]}>
                                                <Text style={styles.card_btn_text}>Refuser</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => handleAccept(p.id)}>
                                            <View style={[styles.card_btn, { backgroundColor : "#5CDE94"}]}>
                                                <Text style={styles.card_btn_text}>Accepter</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> 
                                }
                            </View>
                        ))}
                    </>
                    }
                </>
                :
                    <Loading fullscreen/>
                }
            </Content>
        </Layout>
    );
}

const styles = StyleSheet.create({
    icon_wrapper : {
        alignItems: "center", 
        borderWidth : 2, 
        borderColor : "#fff",
        borderRadius : 10, 
        marginBottom : 5,
    },
    icon_header : {
        color : "#fff", 
        paddingHorizontal : 20,
        paddingVertical : 15,
        alignSelf : "center",
    },
    category : {
        marginVertical : 6,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },
    card_container : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        marginVertical : 10,
        borderRadius : 12,
        padding : 10,
    },
    card_header : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center"
    },
    card_image : {
        borderRadius : 100,
        height : 70,
        width : 70,
        marginRight : 15
    },
    card_text : {
        color : "#fff",
        fontSize : 15,
    },
    card_indicator : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center",
    },
    card_dot : {
        height : 15,
        marginRight : 10,
        backgroundColor : "white",
        width : 15,
        borderRadius : 200,
    },
    card_action : {
        display : "flex",
        flexDirection : "row",
        justifyContent : "space-around"
    },
    card_btn : {
        marginVertical : 20,
        paddingHorizontal : 20,
        paddingVertical : 10,
        borderRadius : 10
    },
    card_btn_text : {
        color : "#fff",
        textShadowColor: 'rgba(0, 0, 0, 0.4)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    }
})