import React, {useContext, useState, useCallback} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Alert,
        } from 'react-native';
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import StarRating from 'react-native-star-rating';
import { uploadProfilePicture,  deleteProfilePicture } from '../api/profileApi'
import { UserContext } from '../contexts/UserProvider';
import ImagePicker from 'react-native-image-picker';
import { getGames } from '../api/gamesApi' 
import { Layout, Loading, } from '../components';
import { removeData } from '../services/AsyncStorage'
import { Title, Content, Input, Icon } from 'native-base';
import UserDefault from '../assets/default_picture.png'

const options = {
    title: 'Choisir une image',
    takePhotoButtonTitle : 'Prendre une photo',
    chooseFromLibraryButtonTitle :'Choisir dans la gallerie',
    mediaType : 'photo',
    cameraType: 'back',
    noData : true,
    allowsEditing : true,
    quality : 0.7,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const StarRatingRender = ({ text, onSelectStar, stars}) => (
    <View style={{ flexDirection : "row"}}>
        <Text style={{ color : "white", width : "50%", marginBottom: 5, fontSize :14}}>
            {text}
        </Text>
        <View style={{ paddingHorizontal : 30}}>
            <StarRating
                fullStarColor='#f2bd1a'
                emptyStarColor='#f2bd1a'
                starStyle={{ marginHorizontal : 2}}
                starSize={18}
                maxStars={5}
                rating={stars}
                disabled
                selectedStar={onSelectStar}
            />
        </View>
    </View>
)

export const ProfileScreen = (props) => {
    const { navigation } = props
    const userApp = useContext(UserContext);
    const currentUser = auth().currentUser;
    const userRef = firestore().collection('users').doc(userApp.user.uid)
    const isFocused = useIsFocused();
    const [userInfos, setUserInfos] = useState(null)
    const [image, setImage] = useState({uri : null})
    const [searchTerm, setSearchTerm] = useState({
        query : null,
        searchResults : [],
        visible: false
    })

    const logout = () => {
        auth().signOut().then(()=> {
            removeData.removeItem("token");
            userApp.setUser({
                isAuthenticated : false,
                displayName     : null,
                photoURL        : null,
                uid             : null,
                token           : null
            }); 
            navigation.navigate("home")
        }).catch(error => {
            console.log(error)
        })
    }

    const handlePicture = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                return response.didCancel;
            } else if (response.error) {
                return response.error;
            } else {
                setImage({ 
                    uri      : response.uri,
                    type     : response.type,
                    fileName :  response.fileName 
                });
                Alert.alert(
                    'Attention',
                    'Votre image est chargé, souhaitez vous sauvegarder', // <- this part is optional, you can pass an empty string
                    [   {text: 'Annuler', onPress: () => { 
                            setImage({uri : currentUser.photoURL})}, style: 'cancel' 
                        },
                        {text: 'Accepter', onPress: async() => { 
                        await uploadProfilePicture(response.uri, userApp.user.uid)
                            .then((photoUrl)=> {
                                setImage({ uri : photoUrl})
                                return currentUser.updateProfile({ photoURL : photoUrl })
                            })
                        }}
                    ],
                    {cancelable: false},
                );
            }
        });
    }

    const deletePicture = () => {
        Alert.alert(
            'Attention',
            'Vous êtes sur le point de supprimer votre avatar', // <- this part is optional, you can pass an empty string
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: async() => { 
                    await deleteProfilePicture(image.uri, currentUser.uid).then(res => {
                        setImage({ uri : null})
                        return currentUser.updateProfile({ photoURL : null })
                    })
                }}
            ],
            {cancelable: false},
        );
    }

    const deleteCommunity = (game) => {
        userRef.onSnapshot(querySnapshot =>{
            querySnapshot.data().communities.forEach(com =>{
                if (com.game === game){
                    Alert.alert(
                        'Attention',
                        'Vous êtes sur le point de quitter cette communauté',
                        [   {text: 'Annuler'},
                            {text: 'Continuer', onPress: async() => { 
                                const newArray = []
                                newArray.push(...userInfos.communities)

                                for (var i = 0; i < newArray.length; i++) {
                                    console.log(newArray[i].game)
                                    if(newArray[i].game === game){
                                        newArray.splice(i, 1);
                                    }
                                }

                                setUserInfos(prevState =>({ 
                                    ...prevState, 
                                    communities : newArray
                                }))

                                userRef.update({ 
                                    communities : firestore.FieldValue.arrayRemove(com)
                                })
                            }}
                        ],
                        {cancelable: false},
                    );
                }
            })
        })
    }

    const handleSearch = async query => {
        setSearchTerm(prevState => ({ ...prevState, query : query }));

        getGames(query).then(({data}) =>{
            setSearchTerm(prevState => ({ ...prevState, searchResults : data.results }));
        })
    };

    const addCommunity = async (game, image) => {
        userRef.get().then(async doc => {
            await doc.data().communities.forEach(com => {
                if(com.game === game){
                    return Alert.alert('Erreur', 'Tu fait déjà partie de cette communautée')
                }
            });

            userRef.update({ 
                communities : firestore.FieldValue.arrayUnion({ game : game, image : image}) })
            .then(()=>{
                const newArray = userInfos.communities
                newArray.push({game, image})
                setUserInfos(prevState => ({ ...prevState, communities : newArray}))
                setSearchTerm({ query : null, searchResults : null })
            })
        })
    }

    const makeTwoDigit = (number) => {
        if (isNaN(number)){
            return '0.0'
        }
        if(number.includes('.')){
            return number
        }else{
            return `${number}.0`
        }
    }


    useFocusEffect(
        useCallback(() => {
            const userRef = firestore().collection('users')
            const arenaRef = firestore().collection('arenas')

            const unsubscribe = userRef
                .doc(currentUser.uid)
                .get()
                .then(doc => {
                    setImage({uri : currentUser.photoURL})

                    const feedbackArray = doc.data().feedbacks
                    var fairplay_counter = 0;
                    var host_quality_counter = 0;
                    var ponctuality_counter = 0;

                    feedbackArray.forEach(count => {
                        fairplay_counter += count.stars.fairplay
                        host_quality_counter += count.stars.host_quality
                        ponctuality_counter += count.stars.ponctuality
                    });
                    
                    setUserInfos({
                        communities : doc.data().communities,
                        avatar : doc.data().avatar,
                        name : doc.data().name,
                        fairplay : (fairplay_counter / feedbackArray.length).toString(),
                        host_quality: (host_quality_counter / feedbackArray.length).toString(),
                        ponctuality: (ponctuality_counter / feedbackArray.length).toString(),
                       // ...doc.data()
                    })

                    arenaRef.get().then((doc) =>{
                        var arenaList = 0;
                        var inscriptionList = 0;
                        var litigeList = 0;
                        doc.forEach(arena => {
                            if(arena.data().creator.id === userApp.user.uid){
                                arenaList ++
                                if(arena.data().step === "litigation"){
                                    litigeList++
                                }
                            }
                            arena.data().participants.forEach(participation =>{
                                if(participation.user.id === userApp.user.uid){
                                    inscriptionList ++
                                    if(arena.data().step === "litigation"){
                                        litigeList++
                                    }
                                }
                            })
                        })
                        setUserInfos(prevState => ({
                            ...prevState,
                            arenasCounter : arenaList,
                            inscriptionsCounter : inscriptionList,
                            litigesCounter : litigeList
                        }))
                    })

                    const promises = []
                        doc.data() && doc.data().feedbacks.forEach(feeds => {
                            promises.push(
                                userRef.doc(feeds.from.id).get()
                                .then((feed) => {
                                    var feedObject = {
                                            id : feed.id,
                                            avatar : feed.data().avatar,
                                            stars : feeds.stars,
                                            content : feeds.content,
                                            name : feed.data().name
                                        }
                                    return feedObject
                                })
                            )
                        })
                        Promise.all(promises).then(data => {
                            setUserInfos(prevState => ({
                                ...prevState,
                                feebdacks : data
                            }))
                        })
                })

            return () => { unsubscribe; console.log("my arena unmounted") }
        }, [navigation])
    )

    return (
        <Layout title={                
            <Title style={{ textAlign : "center", alignSelf : "center"}}>
                Mon profil
            </Title>
        }>   
        {isFocused && userInfos && userInfos.communities && userInfos.feebdacks ?
            <Content>
                <View style={styles.container}>
                <View style={styles.user_wrapper}>
                    <View style={styles.camera}>
                    {image.uri &&
                        <TouchableOpacity 
                            style={styles.cancel}
                            onPress={() => deletePicture()}
                        >
                            <Icon style={{ color : '#bf3737'}}type="MaterialIcons" name="cancel"/>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity onPress={handlePicture}>
                        <View>
                            <Image style={styles.image} 
                                source={image.uri ? { uri : image.uri } : UserDefault}
                            />
                        </View>
                    </TouchableOpacity>
                    </View>
                    <View style={{ position : "absolute", left : 30, top: 30}}>
                        <TouchableOpacity onPress={() => navigation.navigate("profile_settings")}>
                            <Icon style={{ color : "white"}} type="MaterialIcons" name="share"/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ position : "absolute", right : 30, top: 30}}>
                        <TouchableOpacity onPress={() => navigation.navigate("profile_settings")}>
                            <Icon style={{ color : "white"}} type="Ionicons" name="md-settings-sharp"/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginVertical : 20 }}>
                        <Text style={{ color : '#fff', marginBottom : 10, alignSelf: 'center', fontSize : 30, fontWeight : "bold"}}>
                            {userApp.user.displayName}
                        </Text>
                    </View>
                        <View style={styles.compteur}>
                            <View style={{ alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.fairplay)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14}}>
                                    Fairplay
                                </Text>
                            </View>
                            <View style={{alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.host_quality)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14, textAlign : "center"}}>
                                    Accueil ou{"\n"} réseaux
                                </Text>
                            </View>
                            <View style={{ alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.ponctuality)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14}}>
                                    Ponctualité
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.category}>
                        <View style={{ display :"flex",flexDirection :"row", marginBottom : 20}}>
                            <Icon style={{color : "#fff", marginRight : 15}} name="gamepad" type="FontAwesome5"/>
                            <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                                Mes communautés :
                            </Text>
                        </View>
                        <View style={{ marginBottom : 10}}>
                            {userInfos.communities.map(com => (
                                <View key={com.game} style={{ flexDirection : "row", alignItems : "center"}}>
                                    <View style={styles.games}>
                                        <Image source={{ uri : com.image}}  style={{ width : 50, height : 50, borderRadius : 10}}/>
                                        <View style={{flexShrink : 1}}>
                                            <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                                {com.game}
                                            </Text>
                                        </View>
                                    </View> 
                                    <TouchableOpacity onPress={() => deleteCommunity(com.game)}>
                                        <Icon style={{ color : '#bf3737', marginHorizontal : 5}}type="MaterialIcons" name="cancel"/>
                                    </TouchableOpacity>
                                </View>
                            ))}
                        </View>
                        {!searchTerm.visible ?
                        <TouchableOpacity onPress={() => setSearchTerm({visible : true})}>
                            <Icon style={styles.plus} type="MaterialCommunityIcons" name="plus"/>
                        </TouchableOpacity>
                        :
                        <>
                            <Input style={styles.input} 
                                placeholderTextColor="#737373"
                                placeholder="Exemple : Super Smash Bros. Ultimate" 
                                value={searchTerm.query}
                                onChangeText={(query) => handleSearch(query)}
                            />
                            {(searchTerm.query && searchTerm.query.length) > 0 &&
                            <>
                                {searchTerm.searchResults && 
                                    searchTerm.searchResults.map(item => (
                                        <TouchableOpacity key={item.slug} onPress={() => addCommunity(item.name, item.background_image)}>
                                            <View style={styles.item}>
                                                <Image 
                                                    source={{ uri : item.background_image}}  
                                                    style={{ width : 50, height : 50, borderRadius : 10}}
                                                />
                                                <View style={{flexShrink : 1}}>
                                                    <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                                        {item.name}
                                                    </Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity> 
                                    ))
                                } 
                            </>
                            }
                        </>
                        }
                    </View>

                    <View style={styles.category}>
                        <View style={{ display :"flex",flexDirection :"row", marginBottom : 20}}>
                            <Icon style={{color : "#fff", marginRight : 15}} name="feedback" type="MaterialIcons"/>
                            <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                                Avis :
                            </Text>
                        </View>
                        {userInfos.feebdacks.length > 0 ?
                            <View style={styles.feed_container}>
                                {userInfos.feebdacks.map(feed => (
                                    <View style={styles.feed_item} key={feed.id}>
                                        <TouchableOpacity 
                                            style={{ flexDirection : "row", alignItems : "center"}}
                                            onPress={() => {
                                            userApp.setCustomer({ id : feed.id }); 
                                            navigation.push('customer')
                                        }}>
                                            <Image style={styles.user_feed} source={feed.avatar ? { uri : feed.avatar } : UserDefault}/>
                                            <Text style={{ color : "#fff", marginLeft : 5}}>{feed.name}</Text>
                                        </TouchableOpacity>
                                        <View style={{ marginVertical : 10}}>
                                            <StarRatingRender 
                                                text="Fairplay :"
                                                stars={feed.stars.fairplay}
                                            />
                                            <StarRatingRender 
                                                text="Accueil ou résaux :"
                                                stars={feed.stars.host_quality}
                                            />
                                            <StarRatingRender 
                                                text="Ponctualité :"
                                                stars={feed.stars.fairplay}
                                            />
                                        </View>
                                        <View style={{ borderTopWidth : 0.5, borderColor : "#fff"}}>
                                            <Text selectable={true} style={{ color : "white", marginVertical:15, fontSize :14}}>
                                                {feed.content}
                                            </Text>
                                        </View>
                                    </View>
                                ))}
                            </View>
                            :
                            <Text style={{ color : "white", fontSize : 20}}> 
                                Aucun avis
                            </Text>
                        }
                    </View>

                    <View style={styles.category}>
                        <View style={{ display :"flex",flexDirection :"row", marginBottom : 20}}>
                            <Icon style={{color : "#fff", marginRight : 15}} name="graph" type="SimpleLineIcons"/>
                            <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                                Activité :
                            </Text>
                        </View>
                        <View style={styles.activity_container}>
                            <View style={[styles.activity, { backgroundColor : "#66d9ff"}]}>
                                <Text style={styles.activity_text}>
                                    {userInfos.arenasCounter}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Arènes crées
                                </Text>
                            </View>
                            <View style={[styles.activity, { backgroundColor : "#5BFFA2"}]}>
                            <Text style={styles.activity_text}>
                                    {userInfos.inscriptionsCounter}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Inscriptions
                                </Text>
                            </View>
                            <View style={[styles.activity, { backgroundColor : "#FE628C"}]}>
                                <Text style={styles.activity_text}>
                                    {userInfos.litigesCounter}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Litiges
                                </Text>
                            </View>
                        </View>
                    </View>

                    <TouchableOpacity onPress={logout}>
                        <View style={[styles.btn_wrapper, {backgroundColor : '#9e1442'}]}>
                            <Text style={styles.btn_text}>Se déconnecter</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </Content>
            : 
            <Loading fullscreen />
            }
        </Layout>
    );
};

const styles = StyleSheet.create({
    container :{
        padding: 30,
        marginBottom : 20
    },
    user_wrapper:{
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        paddingHorizontal : 20,
        paddingVertical :10,
        borderRadius : 20,
        marginBottom : 10
    },
    image : {
        padding : 30,
        height : 130,
        width : 130,
        borderRadius : 500
    },
    category : {
        marginTop : 6,
        marginVertical : 30,
    },
    camera : {
        alignSelf : 'center',
    },
    cancel : {
        position : 'absolute',
        top : 0,
        right : 0,
        zIndex: 99,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    compteur : {
        display : "flex",
        flexDirection : "row",
        justifyContent : 'space-around',
        marginBottom : 30
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        height : 50
    },
    games : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        flex : 1,
        marginVertical : 3
    },
    item : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        marginVertical : 3
    },
    item_wrapper : {
        marginVertical : 5,
    },
    plus : {
        color : "white",
        backgroundColor : "#9420c6",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
    },
    feed_container : {
        marginBottom : 10
    },
    feed_item : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 20,
        paddingHorizontal : 20,
        paddingVertical :10,
        marginVertical : 5
    },
    user_feed : {
        width : 50,
        height : 50,
        borderRadius : 200,
        marginVertical : 6,
        marginHorizontal : 6,
        padding: 24,
    },
    activity_container : {
        display : "flex",
        flexDirection : "row",
        justifyContent : 'space-around',
        marginBottom : 30,
        alignContent : "center",
    },
    activity:{
        width : "32%",
        borderRadius : 10,
        paddingHorizontal : 10,
        paddingVertical : 20,
        backgroundColor : "red",
        alignItems : "center",
    },
    activity_text : {
        color : "white",
        fontSize : 25,
        fontWeight : "bold"
    },
    btn_wrapper : {
        backgroundColor : 'transparent',
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    }
})