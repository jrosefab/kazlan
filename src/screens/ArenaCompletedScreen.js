import React, {useContext, useState, useCallback} from "react";
import { StyleSheet, Image, View, Text,
        TouchableOpacity, Alert, TouchableWithoutFeedback } from "react-native";
import moment from 'moment';
import 'moment/locale/fr';
import firestore from '@react-native-firebase/firestore';
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import { ChatContext } from "../contexts/ChatProvider";
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import { UserContext } from "../contexts/UserProvider";
import {ArenaContext} from "../contexts/ArenaProvider";
import {Layout, Loading } from "../components";
import { Content, Icon } from "native-base";
import UserDefault from '../assets/default_picture.png'
import {findIconStatus, findColorStatus, findStatus } from "../helpers/switchHelper";

export const ArenaCompletedScreen = (props) => {
    const { navigation } = props;
    const arenaApp = useContext(ArenaContext);
    const userApp = useContext(UserContext);
    const chatApp = useContext(ChatContext);

    const [participantsList, setParticipantsList] = useState([])

    const isFocused = useIsFocused();

    const utcDateToString = (momentInUTC) => {
        let s = moment.utc(momentInUTC).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        return s;
    };

    const addToCalendar = () => {
        console.log(utcDateToString(arenaApp.currentArena.schedule_date))
        const eventConfig = {
            title: "Inscription à une nouvelle session kazlan",
            startDate: utcDateToString(arenaApp.currentArena.schedule_date),
            notes: `Je me suis inscris à "${arenaApp.currentArena.title}" `
        };
        
        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
    }

    const handleUnsubscribe = () => {
        const arenaRef = firestore().collection('arenas').doc(arenaApp.currentArena.id)
        const userRef = firestore().collection('users').doc(userApp.user.uid)

        Alert.alert( 
            "Attention", 
            "Vous êtes sur le point d'annuler votre inscription.",
            [{text: 'Annuler'},
            {text: 'Continuer', onPress: async() => {
                const newArray = [] 
                return arenaRef.get()
                .then(async(doc)=> {
                    await doc.data().participants.forEach(participant => {
                        if (participant.user.id === userApp.user.uid){
                            participant.status = "canceled"
                        }
                        newArray.push(participant)
                    })
                    arenaRef.update({participants : newArray })
                    userRef.get()
                    .then(async(user) => {
                        newArray.length = 0
                        await user.data().arenas.forEach(arena => {
                            if (arena.arena.id === arenaApp.currentArena.id){
                                arena.status = "canceled"
                            }
                            newArray.push(arena)
                        })
                        userRef.update({ arenas : newArray })
                    })
                })  
            }}],
            {cancelable: true},
        )
    }

    const deleteArena = () => {
        Alert.alert(
            'Attention',
            'Êtes vous sûr(e) de vouloir clôturer cette arène ?',
            [   {text: 'Annuler'},
                {text: 'Continuer', onPress: async() => { 
                    firestore()
                    .collection("arenas")
                    .doc(arenaApp.currentArena.id)
                    .update({
                        step : "deleted"
                    })
                    .then(() => {
                        firestore()
                        .collection("users")
                        .doc(userApp.user.uid)
                        .get()
                        .then(user => {
                            console.log(user)
                        })
                    });
                    navigation.goBack(null)
                }} 
            ],
            {cancelable: false},
        );
    }

    useFocusEffect(
        useCallback(() => {
        
        const arenaRef = firestore().collection('arenas').doc(arenaApp.currentArena.id)
        const userRef = firestore().collection('users')
        const unsubscribe = arenaRef.get()
            .then(doc => {
                console.log(doc.data())
                const promises = []
                if(doc.data()){
                    arenaApp.setCurrentArena(prevState => ({
                        ...prevState,
                        title : doc.data().title,
                        creator : doc.data().creator,
                        type : doc.data().type,
                        chats : doc.data().chats.id,
                        schedule_date : doc.data().schedule_date,
                        requirement : doc.data().requirement,
                        inscription_limit : doc.data().inscription_limit,
                        region : doc.data().region,
                        zip_code : doc.data().zip_code,
                        city : doc.data().city,
                        address : doc.data().address,
                        step : doc.data().step,
                    }));

                    promises.push(
                        userRef.doc(doc.data().creator.id).get()
                        .then((creator) => {
                            var creatorObject = {
                                id : creator.id,
                                avatar : creator.data().avatar,
                                name : creator.data().name,
                                isCreator : true,
                            }
                            return creatorObject
                        })
                    )
                    
                    doc.data().participants.forEach(participant => {
                        if(participant.status === "accepted"){
                            promises.push(
                                userRef.doc(participant.user.id).get()
                                .then((p) => {
                                    var particpantObject = {
                                        id : p.id,
                                        avatar : p.data().avatar,
                                        name : p.data().name,
                                        motivation : participant.motivation,
                                        ressource : participant.ressource,
                                        isCreator : false
                                    }
                                    return particpantObject
                                })
                            )
                        }
                    })
                    Promise.all(promises).then(data => {
                        console.log(data)
                        setParticipantsList(data)
                    })
                }
            })

            return () => { unsubscribe; console.log("arenacomplete unmounted") }
        }, [navigation, arenaApp.currentArena.id])
    );


    return (
        <Layout>
            {isFocused && participantsList.length > 0 ?
                <Content contentContainerStyle={{ flexGrow: 1, paddingHorizontal : 20 }}>
                    <View style={{ flexDirection : "row", justifyContent : "space-evenly"}}>
                        <View style={[styles.indicator_wrapper, { borderColor : findColorStatus(arenaApp.currentArena.step) }]}>
                            <Text style={{ color : findColorStatus(arenaApp.currentArena.step)}}>
                                Capacitité : {arenaApp.currentArena.inscription_limit} max 
                            </Text>
                        </View>
                        <View style={[styles.indicator_wrapper, { borderColor : findColorStatus(arenaApp.currentArena.step) }]}>
                            {findIconStatus(arenaApp.currentArena.step)}
                            <Text style={{ color : findColorStatus(arenaApp.currentArena.step)}}>
                                {findStatus(arenaApp.currentArena.step)}
                            </Text>
                        </View>
                    </View>

                    <View style={{ alignItems : "baseline", flexDirection : "row", justifyContent : "center", marginVertical : 20}}>
                        <View style={{ width : "31%", justifyContent : "center"}}>
                            <TouchableOpacity onPress={addToCalendar}>
                                <View style={styles.icon_wrapper}>
                                    <Icon style={styles.icon_header}
                                        name="calendar" 
                                        type="FontAwesome"
                                    />
                                </View>
                            </TouchableOpacity>
                            <Text style={{ color : "#fff", textAlign : "center"}}>
                                Ajouter à {"\n"}mon agenda
                            </Text>
                        </View>
                        <View style={{ width : "31%", justifyContent : "center"}}>
                            <TouchableOpacity onPress={() => {
                                navigation.navigate("arena");
                            }}>
                                <View style={styles.icon_wrapper}>
                                    <Icon style={styles.icon_header}
                                        name="eye-sharp" 
                                        type="Ionicons"
                                    />
                                </View>
                            </TouchableOpacity>
                            <Text style={{ color : "#fff", textAlign : "center"}}>
                                Voir l'annonce
                            </Text>
                        </View>
                        <View style={{ width : "31%", justifyContent : "center"}}>
                            <TouchableOpacity onPress={() => {
                                chatApp.setCurrentChat({ id : arenaApp.currentArena.chats });
                                navigation.navigate("arena_chat");
                            }}>
                                <View style={styles.icon_wrapper}>
                                    <Icon style={styles.icon_header}
                                        name="message-processing" 
                                        type="MaterialCommunityIcons"
                                    />
                                </View>
                            </TouchableOpacity>
                            <Text style={{ color : "#fff", textAlign : "center"}}>
                                Discuter
                            </Text>
                        </View>
                    </View>
                    <View style={{ display :"flex",flexDirection :"row", marginVertical : 20}}>
                        <Icon style={{color : "#fff", marginRight : 15}}type="MaterialIcons" name="location-on"/>
                        <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                            Localisation :
                        </Text>
                    </View>
                    <View style={{ marginLeft : 10}}>
                        <Text selectable={true} style={{color : "#fff", fontSize : 16 }}>
                            {arenaApp.currentArena.address} {"\n"}
                            {`${arenaApp.currentArena.zip_code} ${arenaApp.currentArena.city}` }{"\n"}
                            {arenaApp.currentArena.region}
                        </Text>
                    </View>
                    <View style={{ display :"flex",flexDirection :"row", marginVertical : 20}}>
                        <Icon style={{color : "#fff", marginRight : 15}}type="Ionicons" name="md-people"/>
                        <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                            Membres de la session :
                        </Text>
                    </View>
                    { participantsList.map(participant => (
                        <TouchableWithoutFeedback 
                            key={participant.id}
                            onPress={() => {
                                userApp.setCustomer({ id : participant.id });
                                navigation.navigate("customer");
                        }}>
                            <View 
                                style={[
                                    styles.user_wrapper, 
                                    participant.isCreator && 
                                    { backgroundColor : "#7c7c7c"}
                                ]}
                            >
                                <Image source={participant.avatar ? { uri : participant.avatar} : UserDefault} style={styles.avatar}/>
                                <View style={{ flexShrink : 1}}>
                                    <Text style={{ color : "#fff", fontSize : 18, fontWeight : "bold"}}>
                                        {participant.isCreator && "Hôte : "}{participant.name}
                                    </Text>
                                    {participant.motivation &&
                                        <Text  selectable={true} style={{ color : "#fff"}}>
                                            {participant.motivation}
                                        </Text>
                                    }
                                    {participant.ressource &&
                                        <Text  selectable={true} style={{ color : "#fff"}}>
                                            {participant.ressource}
                                        </Text>
                                    }
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    ))}
                    { (arenaApp.currentArena.step === "started" && 
                        arenaApp.currentArena.type === "money_match") &&
                        <TouchableOpacity onPress={()=> navigation.navigate('arena_rounds')}>
                            <View style={[styles.btn_wrapper, {backgroundColor : '#20c6bd'}]}>
                                <Text style={styles.btn_text}>
                                    Accéder au money match
                                </Text>
                            </View>
                        </TouchableOpacity>
                    }
                    {arenaApp.currentArena.creator.id === userApp.user.uid &&             
                        <TouchableOpacity onPress={deleteArena}>
                            <View style={[styles.btn_wrapper, {backgroundColor : '#9e1442', marginBottom : 60}]}>
                                <Text style={styles.btn_text}>
                                    Clôturer la session
                                </Text>
                            </View>
                        </TouchableOpacity>
                    }
                    {arenaApp.currentArena.creator.id !== userApp.user.uid && 
                        <TouchableOpacity onPress={handleUnsubscribe}>
                            <View style={[styles.btn_wrapper, { backgroundColor : "#9e1442", marginBottom : 60}]}>
                                <Text style={styles.btn_text}>
                                    Annuler mon inscription
                                </Text>
                            </View>
                        </TouchableOpacity>
                    }
                    
                </Content>
            :
                <Loading fullscreen/>
            }
        </Layout>
    )
}

const styles = StyleSheet.create({
    icon_wrapper : {
        alignItems: "center", 
        marginBottom : 5,
    },
    icon_header : {
        color : "#fff", 
        borderRadius : 10, 

        borderWidth : 2, 
        borderColor : "#fff",
        paddingHorizontal : 20,
        paddingVertical : 15,
        alignSelf : "center",
    },
    user_wrapper : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        flexDirection : "row",
        position : "relative",
        borderRadius : 20,
        padding : 20,
        marginBottom : 10
    },
    avatar : {
        height : 40,
        width : 40,
        marginRight : 20,
        borderRadius : 100
    },
    get_user : {
        flexDirection : "row",
        alignItems : "center",
        position : "absolute",
        right : 0,
        bottom : 10
    },
    indicator_wrapper : {
        flexDirection : "row",
        alignSelf : "center",
        borderWidth : 1,
        padding: 10,
        borderRadius : 10,
        marginVertical : 10
    },
    btn_wrapper : {
        backgroundColor : 'transparent',
        borderWidth : 2,
        padding : 20,
        borderRadius : 60,
        marginTop : 10
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
    card_indicator : {
        position : "absolute",
        bottom : 10,
        right : 10,
        display : "flex",
        flexDirection : "row",
        alignItems : "center",
    },
    card_dot : {
        height : 15,
        marginLeft : 10,
        backgroundColor : "white",
        width : 15,
        borderRadius : 200,
    },
})