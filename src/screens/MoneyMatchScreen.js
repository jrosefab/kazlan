import React, {useContext, useState, useCallback} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image } from 'react-native';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import { ArenaContext } from "../contexts/ArenaProvider";
import { UserContext } from '../contexts/UserProvider';
import {findStatus, findIconStatus, findColorStatus} from '../helpers/switchHelper';
import { Layout,  Loading,  ShouldLog } from '../components';
import { Content, Icon } from 'native-base';
import moment from 'moment';
import 'moment/locale/fr';
import NoImageAvailable from '../assets/default_image.png';

export const MoneyMatchScreen = (props) => {
    const { navigation } = props
    const [ arenasList, setArenasList] = useState([]);
    const userApp = useContext(UserContext);
    const arenaApp = useContext(ArenaContext);
    const isFocused = useIsFocused();

    const getScheduleDate = (data) => (
        moment(data).format("Do MMM YY")
    );

    const getClotured = () => {
        console.log("object")
    }
        
    useFocusEffect(
        useCallback(() => {
            const unsubscribe = firestore()
                    .collection('users')
                    .doc(userApp.user.uid)
                    .onSnapshot((querySnapshot) => {
                        const promises = []
                        querySnapshot.data() && querySnapshot.data().money_matchs.forEach(match => {
                            const userArenaRef = firestore().collection('money_matchs').doc(match.arena.id)
                            promises.push(userArenaRef.get()
                                .then((arena) => {
                                    var moneyMatchObject = {
                                        created_date: arena.data().created_date,
                                        title : arena.data().title,
                                        status : match.role === "creator" ? null : match.status,
                                        image : arena.data().image,
                                        schedule_date : arena.data().schedule_date,
                                        inscription_limit : arena.data().inscription_limit,
                                        id : arena.id,
                                        start_hour : arena.data().start_hour,
                                        role : match.role,
                                        step : arena.data().step,
                                        type : arena.data().type,
                                        participants : arena.data().participants.length
                                    }
                                    return moneyMatchObject
                                })
                            )
                        })
                        Promise.all(promises).then(data => {
                            data.sort((a,b) =>{
                                return new Date(b.created_date) - new Date(a.created_date);
                            });
                            setArenasList(data)
                        })
                    })

            return () => { unsubscribe; console.log("sessions unmounted") }
        }, [navigation, userApp.user.uid])
    );

    return (
        <Layout>  
        {isFocused ?
            <>
            {!userApp.user.isAuthenticated ? 
                <ShouldLog 
                    login={() => navigation.navigate('log_in', { screen : 'log_in' })}
                    signup={() => navigation.navigate('log_in', { screen : 'sign_up' })}
                />
            :
            <Content style={{ paddingHorizontal : 20, marginTop : 20 }}>
                <View style={styles.category}>
                    <Text style={styles.category_title}>
                        Sessions crées
                    </Text>
                    {arenasList.map(arena => (
                        arena.role === "creator" && 
                        arena.step !== "deleted" &&
                        arena.step !== "finished" && 
                        <View style={styles.card_container} key={arena.id}>
                            <TouchableOpacity onPress={() => { 
                                arenaApp.setCurrentArena({id : arena.id});
                                navigation.navigate('arena')
                            }}>
                                <Image style={styles.card_image} source={arena.image ? { uri : arena.image } : NoImageAvailable} />
                            </TouchableOpacity>
                            <View style={{ marginLeft : 10, flex : 1 }}>
                                <TouchableOpacity onPress={() => { 
                                    arenaApp.setCurrentArena({id : arena.id});
                                    navigation.navigate('arena')
                                }}>
                                <View>
                                    <Text style={styles.card_text}>{arena.title}</Text>
                                    <Text style={styles.card_text}>Le {getScheduleDate(arena.schedule_date)}</Text>
                                </View>
                                </TouchableOpacity>
                                <View style={styles.indicator_wrapper}>
                                    <Icon style={{ color : "#fff", marginRight : 10, fontSize : 20 }} type="Ionicons" name="md-people"/>    
                                    <Text style={styles.card_text}>
                                        {arena.participants} inscriptions / {arena.inscription_limit} places
                                    </Text>
                                </View>
                                <View style={styles.indicator_wrapper}>
                                    {findIconStatus(arena.step)}
                                    <Text style={{ color : findColorStatus(arena.step)}}>{findStatus(arena.step)}</Text>
                                </View>
                                <TouchableOpacity style={styles.get_more}
                                    onPress={() => {
                                            arenaApp.setCurrentArena({id : arena.id});
                                            navigation.navigate("arena", 
                                            { screen : arena.step !== "in_progress" ? "arena_completed" : "arena_management"}
                                            )}
                                    }
                                >
                                    <Text style={{ color : "#fff", fontWeight :"bold", marginRight : 5 }}>
                                        {arena.step === "in_progress" ?
                                            "Liste d'inscrit"
                                        :
                                            "Détails de l'arène"
                                        }
                                    </Text>
                                    <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))}
                    <TouchableOpacity onPress={() => navigation.navigate('new_session')}>
                        <Icon style={styles.plus} type="MaterialCommunityIcons" name="plus"/>
                    </TouchableOpacity>
                </View>
                <View style={styles.category}>
                    <Text style={styles.category_title}>
                        Mes inscriptions : 
                    </Text>
                    {arenasList.map(arena => (
                        arena.role === "participant" && 
                        arena.step !== "deleted" &&
                        arena.step !== "finished" && 
                        <View style={styles.card_container} key={arena.id}>
                            <TouchableOpacity onPress={() => { 
                                arenaApp.setCurrentArena({id : arena.id});
                                navigation.navigate('arena')
                            }}>
                                <Image style={styles.card_image} source={arena.image ? { uri : arena.image } : NoImageAvailable} />
                            </TouchableOpacity>
                            <View style={{ marginLeft : 10, flex : 1 }}>
                                <TouchableOpacity onPress={() => { 
                                    arenaApp.setCurrentArena({id : arena.id});
                                    navigation.navigate('arena')
                                }}>
                                <View>
                                    <Text style={styles.card_text}>{arena.title}</Text>
                                    <Text style={styles.card_text}>
                                        Le {getScheduleDate(arena.schedule_date)} à {arena.start_hour}
                                    </Text>
                                </View>
                                </TouchableOpacity>
                                <View style={styles.indicator_wrapper}>
                                    {findIconStatus(arena.status)}
                                    <Text style={{ color : findColorStatus(arena.status)}}>{findStatus(arena.status)}</Text>
                                </View>
                                {arena.status === "accepted" &&
                                    <TouchableOpacity 
                                        style={styles.get_more}  
                                        onPress={() => {
                                            arenaApp.setCurrentArena({id : arena.id});
                                            navigation.navigate("arena", 
                                            { screen : 'arena_completed' });
                                        }}
                                    >
                                        <Text style={{ color : "#fff", fontWeight :"bold", marginRight : 5 }}>
                                            Détails de l'arène
                                        </Text>
                                        <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                    ))}
                </View>
                <View style={[styles.category, { marginBottom : 60 }]}>
                    <Text style={styles.category_title}>
                        Sessions clôturées                      
                    </Text>
                    {arenasList.map(arena => (
                        (arena.step === "deleted" ||
                        arena.step === "finished") &&
                        <View style={[styles.card_container, { opacity : 0.5}]} key={arena.id}>
                            <TouchableOpacity onPress={() => { 
                                arenaApp.setCurrentArena({id : arena.id});
                                navigation.navigate('arena')
                            }}>
                                <Image style={styles.card_image} source={arena.image ? { uri : arena.image } : NoImageAvailable} />
                            </TouchableOpacity>
                            <View style={{ marginLeft : 10, flex : 1 }}>
                                <TouchableOpacity onPress={() => { 
                                    arenaApp.setCurrentArena({id : arena.id});
                                    navigation.navigate('arena')
                                }}>
                                <View>
                                    <Text style={styles.card_text}>{arena.title}</Text>
                                    <Text style={styles.card_text}>Le {getScheduleDate(arena.schedule_date)}</Text>
                                </View>
                                </TouchableOpacity>
                                <View style={styles.indicator_wrapper}>
                                    {findIconStatus(arena.step)}
                                    <Text style={{ color : findColorStatus(arena.step)}}>{findStatus(arena.step)}</Text>
                                </View>
                                <TouchableOpacity style={styles.get_more}
                                    onPress={() => {
                                            arenaApp.setCurrentArena({id : arena.id});
                                            navigation.navigate("arena", 
                                            { screen : arena.step !== "in_progress" ? "arena_completed" : "arena_management"}
                                        )}
                                    }
                                >
                                    <Text style={{ color : "#fff", fontWeight :"bold", marginRight : 5 }}>
                                        Détails de l'arène
                                    </Text>
                                    <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))}
                </View>
            </Content>
                }
            </>
            :
            <Loading fullscreen />
        }
        </Layout>
    );
};

const styles = StyleSheet.create({
    header_tab :{
        display : "flex",
        flexDirection : "row",
        alignSelf : "center",
        borderWidth : 2,
        borderColor : "#9420c6",
        borderRadius : 10,
        marginVertical : 20,
    },
    plus : {
        color : "white",
        backgroundColor : "#9420c6",
        alignSelf : "center",
        padding : 10,
        borderRadius : 10,
        fontSize: 20,
        marginHorizontal : 2,
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
    text_header : {
        paddingHorizontal : 30,
        color : "#fff",
        paddingVertical : 17
    },
    category : {
        marginVertical : 6,
    },
    category_title : {
        fontSize : 20, 
        marginBottom : 10, 
        fontWeight : 'bold', 
        color : "#fff" 
    },
    card_container : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        marginVertical : 10,
        borderRadius : 12,
        padding : 10,
        display : "flex",
        flexDirection : "row",
        position : "relative"
    },
    card_image : {
        borderRadius : 12,
        height : 70,
        width : 70
    },
    card_text : {
        color : "#fff",
        fontWeight : "bold",
        fontSize : 15,
    },
    indicator_wrapper : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center",
        marginTop : 2,
    },
    indicator : {
        fontSize : 20,
        marginRight : 5,
    },
    card_action : {
        alignSelf : "center",
        backgroundColor : "#20c6bd",
        paddingHorizontal : 20,
        paddingVertical : 10,
        borderRadius : 10,
        marginVertical : 10
    },
    get_more : {
        marginTop :10,
        flexDirection : "row",
        alignItems : "center",
        alignSelf : 'flex-end',
    },
})