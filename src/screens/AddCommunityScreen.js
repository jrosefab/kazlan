import React, {useContext, useState} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, FlatList, TextInput, Alert} from 'react-native';
import { UserContext } from "../contexts/UserProvider";
import firestore from '@react-native-firebase/firestore';
import { getGames } from '../api/gamesApi' 
import Autocomplete from 'react-native-autocomplete-input';
import axios from "axios";
import {Layout} from '../components';
import {Content} from 'native-base';

export const AddCommunityScreen = (props) => {
    const { navigation } = props;
    const userApp = useContext(UserContext);
    const userRef = firestore().collection('users').doc(userApp.user.uid)
    const [searchTerm, setSearchTerm] = useState({
        query : null,
        searchResults : [],
        visible: false
    })

    const handleSearch = async query => {
        setSearchTerm(prevState => ({ ...prevState, query : query }));

        getGames(query).then(({data}) =>{
            setSearchTerm(prevState => ({ ...prevState, searchResults : data.results }));
        })
    };
    
    const addCommunity = async (game, image) => {
        userRef.get().then(async doc => {
            await doc.data().communities.forEach(com => {
                if(com.game === game){
                    return Alert.alert('Erreur', 'Tu fait déjà partie de cette communautée')
                }
            });

            userRef.update({ 
                communities : firestore.FieldValue.arrayUnion({ game : game, image : image}) })
            .then(()=>{
                navigation.goBack(null)
            })
        })
    }
    
    return (
        <Layout>
            <View style={styles.container}>
                <TextInput style={styles.input} 
                        placeholderTextColor="#fff"
                        placeholder="Rejoindre une communauté" 
                        value={searchTerm.query}
                        onChangeText={(query) => handleSearch(query)}
                />
                <View style={styles.item_wrapper}>
                    <FlatList
                        data={searchTerm && searchTerm.searchResults &&
                            searchTerm.searchResults.length <= 0 ? [] 
                            : searchTerm.searchResults
                        }
                        renderItem={({item}) =>(
                            <TouchableOpacity onPress={() => addCommunity(item.name, item.background_image)}>
                                <View style={styles.item}>
                                    <Image source={{ uri : item.background_image}}  style={{ width : 50, height : 50, borderRadius : 10}}/>
                                    <View style={{flexShrink : 1}}>
                                        <Text style={{ color : "#fff", fontWeight :"bold", marginLeft : 10}}>
                                            {item.name}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity> 
                        )}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>
        </Layout>
    );
};

const styles = StyleSheet.create({
    container: {
        padding : 20,
    },
    input : {
        color : '#fff',
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        height : 50
    },
    item : {
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 10,
        padding: 10,
        marginVertical : 3
    },
    item_wrapper : {
        marginVertical : 5,
    },
});