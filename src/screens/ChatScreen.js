import React, {useContext, useState, useCallback} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Platform } from 'react-native';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import { useClipboard } from '@react-native-community/clipboard'
import firestore from '@react-native-firebase/firestore';
import { ChatContext } from "../contexts/ChatProvider";
import { UserContext } from '../contexts/UserProvider';
import { GiftedChat, InputToolbar, Send, Bubble } from 'react-native-gifted-chat';
import { Layout,  Loading,  ShouldLog } from '../components';
import { Input, Content, Icon } from 'native-base';
import moment from 'moment';
import 'moment/locale/fr';
import NoImageAvailable from '../assets/default_image.png';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


const customBubble = (props) => {
    return (
        <Bubble {...props}
            onPress={() => console.log('ok')}
            textStyle={{
                right: {color: '#fff'},
                left: {color: '#fff'}
            }}
            wrapperStyle={{
                left: {
                    backgroundColor: '#9420c6', 
                    padding : 5,
                },
                right : {
                    backgroundColor: '#20c6bd',
                    padding : 5 
                },
            }}
        />
    );
}

const customtInputToolbar = props => {
    return (
        <InputToolbar
            {...props}
            containerStyle={{
                backgroundColor : '#151520',
                color : "#fff",
                padding : 10,
                alignItems : "center",
                borderTopColor : "transparent",
            }}
        />
    );
}

const customSend = (props) => (
        <Send {...props}        
            containerStyle={{
                height: "100%",
                width : 60,
                justifyContent: 'center',
                alignItems: 'center',
        }}>
            <Icon style={styles.send}
                type="MaterialIcons" 
                name="send"
            />
        </Send>
);

export const ChatScreen = (props) => {
    const { navigation } = props
    const userApp = useContext(UserContext);
    const chatApp = useContext(ChatContext);
    const [chatData, setChatData] = useState(null)
    const [isCopied, setCopied] = useClipboard("");
    const isFocused = useIsFocused();

    const getScheduleDate = (data) => (
        moment(data).format("Do MMM YY")
    );

    const handleNewMessage = (message) => {
        console.log(message[0])
        firestore()
        .collection("chats")
        .doc(chatApp.currentChat.id)
        .update({
            messages : firestore.FieldValue.arrayUnion(message[0])
        })
    }

    const CustomText = ({ text }) => {
        return (
            <TouchableOpacity 
                delayLongPress={500} 
                onLongPress={() => setCopied(text)}
            >
                <View style={{ borderColor : "red", borderWidth : 2 }}>
                    <Text style={{ flexWrap : "wrap"}}>{text}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    useFocusEffect(
        useCallback(() => {
            const unsubscribe = firestore()
                .collection('chats')
                .doc(chatApp.currentChat.id)
                .onSnapshot(async(querySnapshot) => {
                    const newArray = []

                    chatApp.setCurrentChat(previousValue => ({
                        ...previousValue,
                        title : querySnapshot.data().title,
                        participant : querySnapshot.data().participants.length
                    }))
                        
                    querySnapshot.data() && querySnapshot.data().messages.forEach((msg) => {
                        msg.createdAt = msg.createdAt.toDate()
                        newArray.push(msg)
                    })
                    
                    newArray.sort((a, b) =>{
                        return new Date(b.createdAt) - new Date(a.createdAt)
                    })
                    setChatData(prevState =>({...prevState, 
                        messages : newArray
                    }))
                })
                

            return () => { unsubscribe; console.log("my arena unmounted") }
        }, [navigation])
    );

    return (
        <Layout>  
            {isFocused ?
                <>
                    {!userApp.user.isAuthenticated ? 
                        <ShouldLog 
                            login={() => navigation.navigate('log_in', { screen : 'log_in' })}
                            signup={() => navigation.navigate('log_in', { screen : 'sign_up' })}
                        />
                    : 
                    !chatData || !chatData.messages ?
                        <Loading fullscreen/> 
                    :
                    <>
                        <View>
                            <Text style={{ color : "#fff", alignSelf : "center", fontWeight : "bold", fontSize : 20}}>
                                {chatApp.currentChat.participant} participants
                            </Text>
                        </View>
                        <GiftedChat
                            value={Platform.OS === 'ios' ? this.props.text : null}
                            renderUsernameOnMessage={true}
                            listViewProps={styles.chat_container}
                            textInputStyle={styles.input}
                            textInputProps={{
                                autoCorrect : false,
                                placeholder : "Nouveau message...",
                            }}
                            id={1}
                            showUserAvatar={true}
                            dateFormat="DD/MM/YYYY"
                            timeFormat="HH:mm"
                            messages={chatData.messages}
                            onLongPress={(data) => console.log(data)}
                            onSend={handleNewMessage}
                            renderInputToolbar={props => customtInputToolbar(props)}
                            renderSend={props => customSend(props)}
                            renderBubble={props => customBubble(props)}
                            user={{
                                _id: userApp.user.uid,
                                name : userApp.user.displayName,
                                avatar : userApp.user.photoURL ? userApp.user.photoURL : null
                            }}
                        />
                    </>
                    }
                </>
            :
                <Loading fullscreen/> 
            }
        </Layout>
    );
};

const styles = StyleSheet.create({
    chat_container: {
        marginBottom : 20
    },
    card_container : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        marginVertical : 10,
        borderRadius : 12,
        padding : 10,
        display : "flex",
        flexDirection : "row",
    },
    send : {
        color : "white",
        alignItems : "center",
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 60,
        width : "100%",
        padding : 10,
        borderRadius : 100,
    },
    input: {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 20,
        alignItems : "center",
        color : "#fff",
    },
    card_arena : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center"
    },
    card_image : {
        borderRadius : 12,
        height : 70,
        width : 70
    },
    card_text : {
        color : "#fff",
        fontWeight : "bold",
        fontSize : 15,
        marginLeft : 15
    },
    options :{
        backgroundColor : "red",
        paddingVertical : 15,
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
    }
})