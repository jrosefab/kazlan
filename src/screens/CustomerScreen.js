import React, {useContext, useState, useCallback } from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image, Alert} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import StarRating from 'react-native-star-rating';
import { useFocusEffect, useIsFocused  } from '@react-navigation/native';
import { UserContext } from '../contexts/UserProvider';
import ImagePicker from 'react-native-image-picker';
import {findIcon, iconArray} from '../helpers/RanckedIcons';
import { AutoSuggestion, Layout, 
        ModalCustom, RankingSelection,
        Loading } from '../components';
import { removeData } from '../services/AsyncStorage'
import { Title, Content, Textarea, Label, Icon } from 'native-base';
import UserDefault from '../assets/default_picture.png'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const StarRatingRender = ({ text, onSelectStar, stars}) => (
    <View style={{ flexDirection : "row"}}>
        <Text style={{ color : "white", width : "50%", marginBottom: 5, fontSize :14}}>
            {text}
        </Text>
        <View style={{ paddingHorizontal : 30}}>
            <StarRating
                fullStarColor='#f2bd1a'
                emptyStarColor='#f2bd1a'
                starStyle={{ marginHorizontal : 2}}
                starSize={18}
                maxStars={5}
                rating={stars}
                disabled
                selectedStar={onSelectStar}
            />
        </View>
    </View>
)

const SetStarsRender = ({ text, onSelectStar, stars}) => (
    <View style={{ marginVertical : 10}}>
        <Text style={{ color : "white", marginBottom:20, fontWeight :"bold", fontSize :16}}>
            {text}
        </Text>
        <View style={{ paddingHorizontal : 30}}>
            <StarRating
                fullStarColor='#f2bd1a'
                emptyStarColor='#f2bd1a'
                starSize={30}
                disabled={false}
                maxStars={5}
                rating={stars}
                selectedStar={onSelectStar}
            />
        </View>
    </View>
)

export const CustomerScreen = (props) => {
    const { navigation } = props
    const userApp = useContext(UserContext);
    const isFocused = useIsFocused();
    const [userInfos, setUserInfos] = useState(null)
    const [starCount, setStarCount] = useState({
        created_date : new Date(),
        fairplay : 0,
        host_quality: 0,
        ponctuality : 0,
        content : ""
    })
    const [modal, setModal] = useState({
        visible : false,
    })
    const userRef = firestore().collection('users')
    const arenaRef = firestore().collection('arenas')

    const handleFeedback = () => {
        if(starCount.content.length <= 0){
            return Alert.alert( "Erreur", "Reseigner quelques mots")
        }
        else{
            const feedback = {
                created_date : starCount.created_date,
                content : starCount.content,
                from : firestore().doc(`users/${userApp.user.uid}`),
                stars : {
                    fairplay : starCount.fairplay,
                    host_quality : starCount.host_quality,
                    ponctuality : starCount.ponctuality
                }
            }
            userRef.doc(userApp.customer.id).update({
                feedbacks : firestore.FieldValue.arrayUnion(feedback)
            })
        }
        console.log(starCount)
    }

    const makeTwoDigit = (number) => {
        if (isNaN(number)){
            return '0.0'
        }
        if(number.includes('.')){
            return number
        }else{
            return `${number}.0`
        }
    }

    useFocusEffect(
        useCallback(() => {
            const unsubscribe = firestore()
                .collection('users')
                .doc(userApp.customer.id)
                .get()
                .then(doc => {
                    const feedbackArray = doc.data().feedbacks
                    var fairplay_counter = 0;
                    var host_quality_counter = 0;
                    var ponctuality_counter = 0;

                    feedbackArray.forEach(count => {
                        fairplay_counter += count.stars.fairplay
                        host_quality_counter += count.stars.host_quality
                        ponctuality_counter += count.stars.ponctuality
                    });
                    
                    setUserInfos({
                        avatar : doc.data().avatar,
                        name : doc.data().name,
                        fairplay : (fairplay_counter / feedbackArray.length).toString(),
                        host_quality: (host_quality_counter / feedbackArray.length).toString(),
                        ponctuality: (ponctuality_counter / feedbackArray.length).toString(),
                    })

                    arenaRef.get().then((doc) =>{
                        var arenaList = 0;
                        var inscriptionList = 0;
                        var litigeList = 0;
                        doc.forEach(arena => {
                            if(arena.data().creator.id === userApp.customer.id){
                                arenaList ++
                                if(arena.data().step === "litigation"){
                                    litigeList++
                                }
                            }
                            arena.data().participants.forEach(participation =>{
                                if(participation.user.id === userApp.customer.id){
                                    inscriptionList ++
                                    if(arena.data().step === "litigation"){
                                        litigeList++
                                    }
                                }
                            })
                        })
                        setUserInfos(prevState => ({
                            ...prevState,
                            arenas : arenaList,
                            inscriptions : inscriptionList,
                            litiges : litigeList
                        }))
                    })

                    const promises = []
                    doc.data() && doc.data().feedbacks.forEach(feeds => {
                        promises.push(
                            userRef.doc(feeds.from.id).get()
                            .then((feed) => {
                                var feedObject = {
                                        id : feed.id,
                                        avatar : feed.data().avatar,
                                        stars : feeds.stars,
                                        content : feeds.content,
                                        name : feed.data().name
                                    }
                                return feedObject
                            })
                        )
                    })
                    Promise.all(promises).then(data => {
                        setUserInfos(prevState => ({
                            ...prevState,
                            feebdacks : data
                        }))
                    })
                })

            return () => { unsubscribe; console.log("my arena unmounted") }
        }, [navigation])
    );

    return (
        <Layout>   
            {isFocused && (userInfos  && userInfos.feebdacks) ? 
            <Content>
                <View style={styles.container}>
                    <View style={styles.user_wrapper}>
                        <View style={styles.camera}>
                            <Image style={styles.image} 
                                source={userInfos.avatar ? { uri : userInfos.avatar } : UserDefault}
                            />
                        </View>
                        <View style={{ position : "absolute", left : 30, top: 30}}>
                            <TouchableOpacity onPress={() => navigation.navigate("profile_settings")}>
                                <View style={{ display: "flex", justifyContent: "center", alignContent : "center"}}> 
                                    <Icon style={{ color : "white", alignSelf :"center"}} type="MaterialIcons" name="share"/>
                                    <Text style={{ color : "white"}}>Partager</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ position : "absolute", right : 30, top: 30,}}>
                            <View style={{ display: "flex", justifyContent: "center", alignContent : "center"}}> 
                                <Icon style={{ color : "#FE628C", alignSelf :"center"}} type="Foundation" name="alert"/>
                                <Text style={{ color : "#FE628C"}}>Signaler</Text>
                            </View>
                        </View>
                        <View style={{ marginVertical : 10 }}>
                            <Text style={{ color : '#fff', marginBottom : 10, alignSelf: 'center', fontSize : 30, fontWeight : "bold"}}>
                                {userInfos.name}
                            </Text>
                        </View>
                        <View style={styles.compteur}>
                            <View style={{ alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.fairplay)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14}}>
                                    Fairplay
                                </Text>
                            </View>
                            <View style={{alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.host_quality)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14, textAlign : "center"}}>
                                    Accueil ou{"\n"} réseaux
                                </Text>
                            </View>
                            <View style={{ alignItems :'center'}}>
                                <View style={{ flexDirection :"row", alignItems : "center"}}>
                                    <Text style={{ color : "white", fontSize : 20, fontWeight :"bold" }}>
                                        {makeTwoDigit(userInfos.ponctuality)}
                                    </Text>
                                    <Icon style={{ color : "#f2bd1a", fontSize : 18, marginLeft : 5 }} type="FontAwesome" name="star"/>
                                </View>
                                <Text style={{ color : "white", fontSize : 14}}>
                                    Ponctualité
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.category}>
                        <View style={{ display :"flex",flexDirection :"row", marginBottom : 20}}>
                            <Icon style={{color : "#fff", marginRight : 15}} name="feedback" type="MaterialIcons"/>
                            <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                                Avis :
                            </Text>
                        </View>
                        {userInfos.feebdacks.length > 0 ?
                            <View style={styles.feed_container}>
                                {userInfos.feebdacks.map(feed => (
                                    <View style={styles.feed_item} key={feed.id}>
                                        <TouchableOpacity 
                                            style={{ flexDirection : "row", alignItems : "center"}}
                                            onPress={() => {
                                            userApp.setCustomer({ id : feed.id }); 
                                            navigation.push('customer')
                                        }}>
                                            <Image style={styles.user_feed} source={feed.avatar ? { uri : feed.avatar } : UserDefault}/>
                                            <Text style={{ color : "#fff", marginLeft : 5}}>{feed.name}</Text>
                                        </TouchableOpacity>
                                        <View style={{ marginVertical : 10}}>
                                            <StarRatingRender 
                                                text="Fairplay :"
                                                stars={feed.stars.fairplay}
                                            />
                                            <StarRatingRender 
                                                text="Accueil ou résaux :"
                                                stars={feed.stars.host_quality}
                                            />
                                            <StarRatingRender 
                                                text="Ponctualité :"
                                                stars={feed.stars.fairplay}
                                            />
                                        </View>
                                        <View style={{ borderTopWidth : 0.5, borderColor : "#fff"}}>
                                            <Text selectable={true} style={{ color : "white", marginVertical:15, fontSize :14}}>
                                                {feed.content}
                                            </Text>
                                        </View>
                                    </View>
                                ))}
                            </View>
                            :
                            <Text style={{ color : "white", fontSize : 20}}> 
                                Aucun avis
                            </Text>
                        }
                        {userApp.user.uid !== userApp.customer.id &&
                            <TouchableOpacity style={styles.set_feedback} onPress={() => setModal({visible : !modal.visible})}>
                                <Text style={{ color : "#fff", fontWeight :"bold",  marginRight : 5 }}>
                                    Laisser un avis
                                </Text>
                                <Icon style={{ color : "#fff" }} type="MaterialIcons" name="keyboard-arrow-right"/>    
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={styles.category}>
                        <View style={{ display :"flex",flexDirection :"row", marginBottom : 20}}>
                            <Icon style={{color : "#fff", marginRight : 15}} name="graph" type="SimpleLineIcons"/>
                            <Text style={{color : "#fff", fontSize : 20, fontWeight : 'bold' }}>
                                Activité :
                            </Text>
                        </View>
                        <View style={styles.activity_container}>
                            <View style={[styles.activity, { backgroundColor : "#66d9ff"}]}>
                                <Text style={styles.activity_text}>
                                    {userInfos.arenas}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Arènes crées
                                </Text>
                            </View>
                            <View style={[styles.activity, { backgroundColor : "#5BFFA2"}]}>
                            <Text style={styles.activity_text}>
                                    {userInfos.inscriptions}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Inscriptions
                                </Text>
                            </View>
                            <View style={[styles.activity, { backgroundColor : "#FE628C"}]}>
                                <Text style={styles.activity_text}>
                                    {userInfos.litiges}
                                </Text>
                                <Text style={[styles.activity_text, {fontSize : 14}]}>
                                    Litiges
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <ModalCustom visible={modal.visible} 
                    onPress={() => { setModal({ visible : !modal.visible }) }}
                    onRequestClose={!modal.visible}
                >
                    <View>
                        <SetStarsRender 
                            text={`${userInfos.name} est courtois et fairplay :`}
                            stars={starCount.fairplay}
                            onSelectStar={
                                (rating) => setStarCount(prevState =>({
                                ...prevState,
                                fairplay : rating
                            }))}
                        />
                        <SetStarsRender 
                            text={`Qualité du résaux ou accueil de ${userInfos.name} :`}
                            stars={starCount.host_quality}
                            onSelectStar={
                                (rating) => setStarCount(prevState =>({
                                ...prevState,
                                host_quality : rating
                            }))}
                        />
                        <SetStarsRender 
                            text={`${userInfos.name} est poncutel :`}
                            stars={starCount.ponctuality}
                            onSelectStar={
                                (rating) => setStarCount(prevState =>({
                                ...prevState,
                                ponctuality : rating
                            }))}
                        />
                        <View style={{ paddingTop : 20, marginVertical : 10, borderTopColor : "#fff", borderTopWidth : 0.5}}>
                            <Text style={{ color : "white", marginBottom:20, fontWeight :"bold", fontSize :16}}>
                                Que dire de plus ?    
                            </Text>
                            <Textarea 
                                style={[styles.input, { borderRadius : 15}]} 
                                rowSpan={5} 
                                placeholderTextColor="#fff"
                                onChangeText={(text) => setStarCount(previousValue => ({ 
                                    ...previousValue, content : text }
                                ))}
                            />
                            <TouchableOpacity onPress={() => handleFeedback() }>
                                <View style={[styles.btn_wrapper, { backgroundColor : "#20c6bd"}]}>
                                    <Text style={styles.btn_text}>
                                        Donner mon avis
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ModalCustom>
            </Content>
        :
        <Loading fullscreen />    
        }
        </Layout>
    );
};

const styles = StyleSheet.create({
    container :{
        padding: 30,
        marginBottom : 20
    },
    category : {
        marginTop : 6,
        marginVertical : 30,
    },
    user_wrapper:{
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        paddingHorizontal : 20,
        paddingVertical :10,
        borderRadius : 20,
        marginBottom : 10
    },
    camera : {
        marginTop : 10,
        alignSelf : 'center',
    },
    image : {
        padding : 30,
        height : 130,
        width : 130,
        borderRadius : 500
    },
    compteur : {
        flexDirection : "row",
        justifyContent : "space-evenly"
    },
    games : {
        width : 75,
        height : 75,
        borderRadius : 15,
        marginVertical : 6,
        marginHorizontal : 6,
        padding: 24,
    },
    feed_container : {
        marginBottom : 10
    },
    feed_item : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        borderRadius : 20,
        paddingHorizontal : 20,
        paddingVertical :10,
        marginVertical : 5
    },
    user_feed : {
        width : 50,
        height : 50,
        borderRadius : 200,
        marginVertical : 6,
        marginHorizontal : 6,
        padding: 24,
    },
    rank : {
        borderRadius : 20,
        marginVertical : 5,
        marginHorizontal : 5,
        padding : 30,
        height : 50,
        width : 50,
    },
    user_rank : {
        height : 30,
        width : 30,
        alignSelf : 'center',
        position : "absolute",
        top : 5,
        right : 5
    },
    set_feedback : {
        flexDirection : "row",
        alignItems : "center",
        alignSelf : "flex-end"
    },
    input : {
        color : '#fff',
        borderColor : "#fff",
        borderWidth : 0.5,
        borderRadius : 10
    },
    btn_wrapper : {
        backgroundColor : 'transparent',
        padding : 20,
        borderRadius : 60,
        marginTop : 20
    },
    btn_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
    },
    activity_container : {
        display : "flex",
        flexDirection : "row",
        justifyContent : 'space-around',
        marginBottom : 30,
        alignContent : "center",
    },
    activity:{
        width : "32%",
        borderRadius : 10,
        paddingHorizontal : 10,
        paddingVertical : 20,
        backgroundColor : "red",
        alignItems : "center",
    },
    activity_text : {
        color : "white",
        fontSize : 25,
        fontWeight : "bold"
    }
})