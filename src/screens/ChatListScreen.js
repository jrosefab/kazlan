import React, {useContext, useState, useCallback} from 'react';
import { StyleSheet, View, Text, 
        TouchableOpacity, Image } from 'react-native';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import { ChatContext } from "../contexts/ChatProvider";
import { UserContext } from '../contexts/UserProvider';
import {findStatus, findColorStatus} from '../helpers/switchHelper';
import { Layout,  Loading,  ShouldLog } from '../components';
import { Title, Content, Icon } from 'native-base';
import moment from 'moment';
import 'moment/locale/fr';
import { create } from 'react-test-renderer';
import NoImageAvailable from '../assets/default_image.png';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const ChatListScreen = (props) => {
    const { navigation } = props;
    

    const [ displayType, setDisplayType] = useState("session");
    const [ chatList, setChatList] = useState([]);
    const userApp = useContext(UserContext);
    const chatApp = useContext(ChatContext);
    const isFocused = useIsFocused();

    const getScheduleDate = (data) => (
        moment(data).format("Do MMM YY")
    );

    const truncateString = (str) => {
        return str.length > 40 ? str.substring(0, 41) + "..." : str;
    }

    const getClotured = () => {
        console.log("object")
    }
        
    useFocusEffect(
        useCallback(() => {
        const userRef = firestore().collection('users').doc(userApp.user.uid)
        const chatRef = firestore().collection('chats')
        const unsubscribe = userRef.get()
            .then(doc => {
                const promises = []
                doc.data() && doc.data().chats.forEach( chat => {
                    promises.push(
                        chatRef.doc(chat.id).get()
                        .then((chat) => {
                            var chatObject = {
                                id : chat.id,
                                title : chat.data().title,
                                last_message : chat.data().messages.slice(-1)[0],
                                created_date : chat.data().created_date.toDate(),
                                participants : chat.data().participants.length
                            }
                            return chatObject
                        })
                    )
                })
                Promise.all(promises).then(data => {
                    data.sort((a,b) =>{
                        return new Date(b.created_date) - new Date(a.created_date);
                    });
                    setChatList(data)
                })
            })

            return () => { unsubscribe; console.log("chat list unmounted") }
        }, [navigation, userApp.user.uid])
    );

    return (
        <Layout header={true} title={<Title>Messagerie</Title>}>  
            {!userApp.user.isAuthenticated ? 
                <ShouldLog 
                    login={() => navigation.navigate('log_in', { screen : 'log_in' })}
                    signup={() => navigation.navigate('log_in', { screen : 'sign_up' })}
                />
            :
            <Content style={{ paddingHorizontal : 20 }}>
                { isFocused ? 
                    <>       
                        {chatList.length > 0 ?       
                            chatList.map(chat => (
                                <TouchableOpacity 
                                    style={styles.card_container} 
                                    key={chat.id} 
                                    onPress={() => {
                                    chatApp.setCurrentChat({id : chat.id});
                                    navigation.navigate("arena", { screen : 'arena_chat' });
                                }}>
                                    <View style={styles.card_arena} key={chat.id}>
                                        <Text style={styles.card_text}>{chat.title}</Text>
                                        <View style={styles.indicator_wrapper}>
                                            <Icon style={{ color : "#fff", marginRight : 10, fontSize : 20 }} type="Ionicons" name="md-people"/>    
                                            <Text style={styles.card_text}>
                                                {chat.participants} participants
                                            </Text>
                                        </View>
                                        <View style={{ marginLeft:10, flexDirection : "row", alignItems : "center", marginTop :10}}>
                                            {chat.last_message &&
                                                <Image style={styles.card_avatar} source={{ uri : chat.last_message.user.avatar }} />
                                            }
                                            <Text style={{ color : "#F4FF87", marginLeft : 10,}}>
                                                {chat.last_message ? truncateString(chat.last_message.text) : "Aucun messages..."}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))
                        : chatList.length < 0 &&
                            <View style={{ alignSelf : "center", marginTop : 50}}>
                                <Text style={{ color :"white", fontSize : 20}}>
                                    Aucune conversation pour le moment {"\n"}
                                </Text>
                            </View>
                        }
                    </>
                :
                    <Loading fullscreen />
                }
            </Content>
            }
        </Layout>
    );
};

const styles = StyleSheet.create({
    card_container : {
        backgroundColor : 'rgba(48, 48, 59, 0.74)',
        marginVertical : 10,
        borderRadius : 12,
        padding : 10,
        display : "flex",
        flexDirection : "row",
    },
    card_arena : {
        display : "flex",
        flexDirection : "column",
        paddingVertical : 10
    },
    card_image : {
        borderRadius : 12,
        height : 70,
        width : 70
    },
    card_text : {
        color : "#fff",
        fontWeight : "bold",
        fontSize : 15,
        marginVertical :5,
    },
    card_avatar : {
        height : 30,
        width : 30,
        borderRadius : 100
    },
    indicator_wrapper : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center",
        marginTop : 2,
    },
})