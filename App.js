import React, { Component } from 'react';
import AppNavigator from './src/navigations/AppNavigator';
import { UserProvider } from './src/contexts/UserProvider';
import { ArenaProvider } from './src/contexts/ArenaProvider';
import { ChatProvider } from './src/contexts/ChatProvider';
import stripe from 'tipsi-stripe'
import { LogBox } from 'react-native';  

LogBox.ignoreLogs([
  'Could not find Fiber with id', 
  'DatePickerIOS has been merged',
  'DatePickerAndroid has been merged',
  'Animated: `useNativeDriver`'
])

stripe.setOptions({
  publishableKey: 'pk_test_51HkWMbFVF81H1AMN4sPdkrI53Hwt5UQwDSyfhGj7Mp9JWleXJz7eZyINCBBJlyEY9bMNRB9cd8FsQv9mIvR8Fns300sSC7KEOj',
});

/*const token = await stripe.paymentRequestWithCardForm({
  // Only iOS support this options
  smsAutofillDisabled: true,
  requiredBillingAddressFields: 'full',
  prefilledInformation: {
    billingAddress: {
      name: 'Sayali Sonawane',
      line1: 'Canary Place',
      line2: '3',
      city: 'Macon',
      state: 'Georgia',
      country: 'Estonia',
      postalCode: '31217',
      email: 'sayali.sonawane@mindbowser.com',
    },
  },
});*/

export default class App extends Component {
  render() {
    return (
      <UserProvider>
        <ArenaProvider>
          <ChatProvider>
            <AppNavigator/>
          </ChatProvider>
        </ArenaProvider>
      </UserProvider>
      );
  };
}