const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const stripe = require('stripe')('sk_test_51HkWMbFVF81H1AMNCk72gm9qE8Gjk68SYfiNwI16Gkds4mRHBxhsprGxR7YqTODHH26y98ykHEBk3HCyx5h7suiS00lexUfgDJ');

exports.completePayementWithStripe = functions.https.onRequest((request, response)=>{
    stripe.charges
    .create({
        amount:request.body.amount,
        currency:request.body.currency,
        source:'tok_mastercard'
    }).then(charge => {
        response.send(charge)
    }).catch(error =>{
        console.log(error)
    })
})